<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 * $this IS plxShow
 * @version	2.0.0
 * @date	28/02/2019
 * @author	Thomas Ingles
 **/
if($useCapcha) {
	$this->plxMotor->plxCapcha = new plxCapcha();
}
$pluginName = basename(dirname(__FILE__));//basename(__DIR__); fix php-5.1.4.free ::: https://stackoverflow.com/questions/2749416/is-there-any-difference-between-dir-and-dirname-file-in-php#2749423
$plxPlugin = $this->plxMotor->plxPlugins->aPlugins[$pluginName];//init plug for all form.###
