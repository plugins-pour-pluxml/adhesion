<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 *
 * @version	2.0.0
 * @date	23/10/2018
 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
 **/
?>
<!-- <span id="liberapay" style="position:fixed;bottom:1em;right:1em;" title="<?php $plxPlugin->lang('L_LIBERAPAY');?>."><script src="https://liberapay.com/sudwebdesign/widgets/button.js"></script>
<noscript><a href="https://liberapay.com/sudwebdesign/donate"><img alt="<?php $plxPlugin->lang('L_LIBERAPAY');?>." src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript></span> --><?php
# Control du token du formulaire
plxToken::validateFormToken($_POST);

if(!empty($_POST)) {
	$plxPlugin->setParam('adherents', $_POST['adherents'], 'cdata');
	$plxPlugin->setParam('strcutLocks', $_POST['strcutLocks'], 'numeric');
	$plxPlugin->setParam('mnuDisplay', $_POST['mnuDisplay'], 'numeric');
	$plxPlugin->setParam('mnuName', plxUtils::strCheck(str_replace("'","’",$_POST['mnuName'])), 'cdata');
	$plxPlugin->setParam('mnuPos', $_POST['mnuPos'], 'numeric');
	$plxPlugin->setParam('mnuAdherer', plxUtils::strCheck(str_replace("'","’",$_POST['mnuAdherer'])), 'cdata');
	$plxPlugin->setParam('mnuAdhesion', plxUtils::strCheck(str_replace("'","’",$_POST['mnuAdhesion'])), 'cdata');
	$plxPlugin->setParam('mnuMembers', plxUtils::strCheck(str_replace("'","’",$_POST['mnuMembers'])), 'cdata');
	$plxPlugin->setParam('mnuConnexion', plxUtils::strCheck(str_replace("'","’",$_POST['mnuConnexion'])), 'cdata');
	$plxPlugin->setParam('mnuDeconnexion', plxUtils::strCheck(str_replace("'","’",$_POST['mnuDeconnexion'])), 'cdata');
	$plxPlugin->setParam('desc_adhesion', plxUtils::cdataCheck(trim(str_replace("'","’",$_POST['desc_adhesion']))), 'cdata');
	$plxPlugin->setParam('mnuForgetMyPass', plxUtils::strCheck(str_replace("'","’",$_POST['mnuForgetMyPass'])), 'cdata');
	$plxPlugin->setParam('mnuMyAccount', plxUtils::strCheck(str_replace("'","’",$_POST['mnuMyAccount'])), 'cdata');
	$plxPlugin->setParam('showAnnuaire', plxUtils::strCheck($_POST['showAnnuaire']), 'string');
	$plxPlugin->setParam('publicAnnuaire', plxUtils::strCheck($_POST['publicAnnuaire']), 'string');
	$plxPlugin->setParam('mnuAnnuaire', plxUtils::strCheck(str_replace("'","’",$_POST['mnuAnnuaire'])), 'cdata');
	$plxPlugin->setParam('typeAnnuaire', plxUtils::strCheck($_POST['typeAnnuaire']), 'string');
	$tab = (substr($_POST['tabActivites'],-5) == 'Autre' ? $_POST['tabActivites'] : $_POST['tabActivites'].',Autre' );
	$plxPlugin->setParam('tabActivites', plxUtils::strCheck($tab), 'string');
	if(!plxUtils::checkMail($_POST['email'])) {
		$_POST['email']='';
		plxMsg::Error($plxPlugin->getLang('L_WARNPRM_EMAIL'));
	}
	$plxPlugin->setParam('cle', ($_POST['cle'] > 5 ? $_POST['cle'] : 6), 'numeric');
	$plxPlugin->setParam('annee', $_POST['annee'], 'cdata');
	$plxPlugin->setParam('annee_ss_cotis', $_POST['annee_ss_cotis'], 'cdata');
	$plxPlugin->setParam('nom_asso', plxUtils::strCheck(str_replace("'","’",$_POST['nom_asso'])), 'cdata');
	$plxPlugin->setParam('adresse_asso', plxUtils::cdataCheck(trim(str_replace("'","’",$_POST['adresse_asso']))), 'cdata');
	$plxPlugin->setParam('domaine_asso', $_POST['domaine_asso'], 'cdata');
	$plxPlugin->setParam('email', $_POST['email'], 'cdata');
	$plxPlugin->setParam('subject', plxUtils::strCheck(str_replace("'","’",$_POST['subject'])), 'cdata');
	$plxPlugin->setParam('subject_password', plxUtils::strCheck(str_replace("'","’",$_POST['subject_password'])), 'cdata');
	$plxPlugin->setParam('msg_password', plxUtils::strCheck(str_replace("'","’",$_POST['msg_password'])), 'cdata');
	$plxPlugin->setParam('thankyou', plxUtils::strCheck(str_replace("'","’",$_POST['thankyou'])), 'cdata');
	$plxPlugin->setParam('validation_subject', plxUtils::strCheck(str_replace("'","’",$_POST['validation_subject'])), 'cdata');
	$plxPlugin->setParam('validation_msg', plxUtils::strCheck(str_replace("'","’",$_POST['validation_msg'])), 'cdata');
	$plxPlugin->setParam('devalidation_subject', plxUtils::strCheck(str_replace("'","’",$_POST['devalidation_subject'])), 'cdata');
	$plxPlugin->setParam('devalidation_msg', plxUtils::strCheck(str_replace("'","’",$_POST['devalidation_msg'])), 'cdata');
	$plxPlugin->setParam('rappel_subject', plxUtils::strCheck(str_replace("'","’",$_POST['rappel_subject'])), 'cdata');
	$plxPlugin->setParam('rappel_msg', plxUtils::strCheck(str_replace("'","’",$_POST['rappel_msg'])), 'cdata');
	$plxPlugin->setParam('cotis_subject', plxUtils::strCheck(str_replace("'","’",$_POST['cotis_subject'])), 'cdata');
	$plxPlugin->setParam('cotis_msg', plxUtils::strCheck(str_replace("'","’",$_POST['cotis_msg'])), 'cdata');
	$plxPlugin->setParam('template', $_POST['template'], 'cdata');
	$plxPlugin->setParam('perPage', $_POST['perPage'], 'numeric');
	//Look articles
	$plxPlugin->saveParams();
	header('Location:'.$plxPlugin->plxMotor->racine.$plxPlugin->plxMotor->path_url);
	exit;
}
$adherents = $plxPlugin->getParam('adherents')=='' ? @PLX_CONFIG_PATH : $plxPlugin->getParam('adherents');
$strcutLocks = $plxPlugin->getParam('strcutLocks')=='' ? 0 : $plxPlugin->getParam('strcutLocks');
$mnuDisplay = $plxPlugin->getParam('mnuDisplay')=='' ? 1 : $plxPlugin->getParam('mnuDisplay');
$mnuName = $plxPlugin->getParam('mnuName')=='' ? $plxPlugin->getLang('L_DEFAULT_MENU_NAME') : $plxPlugin->getParam('mnuName');
$mnuPos = $plxPlugin->getParam('mnuPos')=='' ? 2 : $plxPlugin->getParam('mnuPos');
$mnuAdherer = $plxPlugin->getParam('mnuAdherer')=='' ? $plxPlugin->getLang('L_DEFAULT_MENU_ADHERER') : $plxPlugin->getParam('mnuAdherer');
$mnuAdhesion = $plxPlugin->getParam('mnuAdhesion')=='' ? $plxPlugin->getLang('L_DEFAULT_MENU_ADHESION') : $plxPlugin->getParam('mnuAdhesion');
$mnuMembers = $plxPlugin->getParam('mnuMembers')=='' ? $plxPlugin->getLang('L_DEFAULT_MENU_MEMBERS') : $plxPlugin->getParam('mnuMembers');
$mnuConnexion = $plxPlugin->getParam('mnuConnexion')=='' ? $plxPlugin->getLang('L_DEFAULT_MENU_CONNEXION') : $plxPlugin->getParam('mnuConnexion');
$mnuDeconnexion = $plxPlugin->getParam('mnuDeconnexion')=='' ? $plxPlugin->getLang('L_DEFAULT_MENU_DECONNEXION') : $plxPlugin->getParam('mnuDeconnexion');
$desc_adhesion = $plxPlugin->getParam('desc_adhesion')=='' ? $plxPlugin->getLang('L_DEFAULT_DESC') : $plxPlugin->getParam('desc_adhesion');
$mnuForgetMyPass = $plxPlugin->getParam('mnuForgetMyPass')=='' ? $plxPlugin->getLang('L_DEFAULT_MENU_PASS') : $plxPlugin->getParam('mnuForgetMyPass');
$mnuMyAccount = $plxPlugin->getParam('mnuMyAccount')=='' ? $plxPlugin->getLang('L_DEFAULT_MENU_MY_ACCOUNT') : $plxPlugin->getParam('mnuMyAccount');
$showAnnuaire = $plxPlugin->getParam('showAnnuaire')=='' ? 'no' : $plxPlugin->getParam('showAnnuaire');
$publicAnnuaire = $plxPlugin->getParam('publicAnnuaire')=='' ? 'no' : $plxPlugin->getParam('publicAnnuaire');
$mnuAnnuaire = $plxPlugin->getParam('mnuAnnuaire')=='' ? $plxPlugin->getLang('L_DEFAULT_MENU_ANNUAIRE') : $plxPlugin->getParam('mnuAnnuaire');
$typeAnnuaire = $plxPlugin->getParam('typeAnnuaire')=='' ? 'generaliste' : $plxPlugin->getParam('typeAnnuaire');
$tabActivites = $plxPlugin->getParam('tabActivites')=='' ? 'ARC,TEC,IRC,Autre' : $plxPlugin->getParam('tabActivites');
$cle = $plxPlugin->getParam('cle')=='' ? 3 : $plxPlugin->getParam('cle');
$annee = $plxPlugin->getParam('annee')=='' ? 'civile' : $plxPlugin->getParam('annee');
$annee_ss_cotis = $plxPlugin->getParam('annee_ss_cotis')=='' ? 0 : $plxPlugin->getParam('annee_ss_cotis');

$nom_asso = $plxPlugin->getParam('nom_asso')=='' ? $plxPlugin->plxMotor->aConf['title'] : $plxPlugin->getParam('nom_asso');
$adresse_asso = $plxPlugin->getParam('adresse_asso')=='' ? '' : $plxPlugin->getParam('adresse_asso');
$domaine_asso = $plxPlugin->getParam('domaine_asso')=='' ? $plxPlugin->plxMotor->racine : $plxPlugin->getParam('domaine_asso');
$email = $plxPlugin->getParam('email')=='' ? $plxPlugin->plxMotor->aUsers["001"]["email"] : $plxPlugin->getParam('email');
$subject = $plxPlugin->getParam('subject')=='' ? $plxPlugin->getLang('L_DEFAULT_SUBJECT') : $plxPlugin->getParam('subject');
$subject_password = $plxPlugin->getParam('subject_password')=='' ? $plxPlugin->getLang('L_DEFAULT_SUBJECT_PASS') : $plxPlugin->getParam('subject_password');
$msg_password = $plxPlugin->getParam('msg_password')=='' ? $plxPlugin->getLang('L_DEFAULT_MSG_PASS') : $plxPlugin->getParam('msg_password');
$thankyou = $plxPlugin->getParam('thankyou')=='' ? $plxPlugin->getLang('L_DEFAULT_THANKYOU') : $plxPlugin->getParam('thankyou');
$val_sub = $plxPlugin->getParam('validation_subject')=='' ? $plxPlugin->getLang('L_DEFAULT_VALIDATION_SUBJECT') : $plxPlugin->getParam('validation_subject');
$val_msg = $plxPlugin->getParam('validation_msg')=='' ? $plxPlugin->getLang('L_DEFAULT_VAL_MSG') : $plxPlugin->getParam('validation_msg');
$deval_sub = $plxPlugin->getParam('devalidation_subject')=='' ? $plxPlugin->getLang('L_DEFAULT_DEVALIDATION_SUBJECT') : $plxPlugin->getParam('devalidation_subject');
$deval_msg = $plxPlugin->getParam('devalidation_msg')=='' ? $plxPlugin->getLang('L_DEFAULT_DEVAL_MSG') : $plxPlugin->getParam('devalidation_msg');

$rappel_sub = $plxPlugin->getParam('rappel_subject')=='' ? $plxPlugin->getLang('L_DEFAULT_RAPPEL_SUBJECT') : $plxPlugin->getParam('rappel_subject');
$rappel_msg = $plxPlugin->getParam('rappel_msg')=='' ? $plxPlugin->getLang('L_DEFAULT_RAPPEL_MSG') : $plxPlugin->getParam('rappel_msg');
$cotis_sub = $plxPlugin->getParam('cotis_subject')=='' ? $plxPlugin->getLang('L_DEFAULT_COTIS_SUBJECT') : $plxPlugin->getParam('cotis_subject');
$cotis_msg = $plxPlugin->getParam('cotis_msg')=='' ? $plxPlugin->getLang('L_DEFAULT_COTIS_MSG') : $plxPlugin->getParam('cotis_msg');
$template = $plxPlugin->getParam('template')=='' ? 'static.php' : $plxPlugin->getParam('template');
$perPage = $plxPlugin->getParam('perPage')=='' ? 50 : $plxPlugin->getParam('perPage');

# On récupère les templates des pages statiques
$files = plxGlob::getInstance(PLX_ROOT.'themes/'.$plxAdmin->aConf['style']);
if ($array = $files->query('/^static(-[a-z0-9-_]+)?.php$/')) {
	foreach($array as $k=>$v)
		$aTemplates[$v] = $v;
}
$aAnnee = array(
	'civile'=>$plxPlugin->getLang('L_ANNEE_CIVILE'),
	'entiere'=>$plxPlugin->getLang('L_ANNEE_ENTIERE'),
	'illimite'=>$plxPlugin->getLang('L_ANNEE_ILLIMITE')
);
$aAnneesc = array(0=>0,1=>1,2=>2,3=>3,4=>4,5=>5);
$aClefs = array(3=>3,4=>4,5=>5,6=>6,7=>7,8=>8);
$aAnnuaire = array('on'=>L_YES,'no'=>L_NO);
$aTypeAnnuaires = array('generaliste'=>$plxPlugin->getLang('L_ANNUAIRE_GEN'),'professionnel'=>$plxPlugin->getLang('L_ANNUAIRE_PRO'));
?>

<h2><?php echo $plxPlugin->getInfo('title') ?></h2>
<?php
if(function_exists('mail')) {
	echo '<p style="color:green"><strong>'.$plxPlugin->getLang('L_MAIL_AVAILABLE').'</strong></p>
';
} else {
	echo '<p style="color:#ff0000"><strong>'.$plxPlugin->getLang('L_MAIL_NOT_AVAILABLE').'</strong></p>
';
}
?>
<form id="form_adhesion" action="" method="post">
	<fieldset class="config">
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="adherents"><?php $plxPlugin->lang('L_CONFIG_ROOT_PATH') ?>&nbsp;<a class="hint help"><span><?php $plxPlugin->lang('L_CONFIG_ROOT_PATH_HELP') ?></span></a>:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('adherents', $adherents, 'text'); ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_strcutLocks"><?php echo $plxPlugin->lang('L_MENU_STRCUTLOCKS') ?>&nbsp;<a class="hint help"><span><?php $plxPlugin->lang('L_MENU_STRCUTLOCKS_HELP') ?></span></a>:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('strcutLocks',$strcutLocks,'text','2-5') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_mnuDisplay"><?php echo $plxPlugin->lang('L_MENU_DISPLAY') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printSelect('mnuDisplay',array('1'=>L_YES,'0'=>L_NO),$mnuDisplay); ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_mnuName"><?php $plxPlugin->lang('L_MENU_TITLE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('mnuName',$mnuName,'text') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_mnuPos"><?php $plxPlugin->lang('L_MENU_POS') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('mnuPos',$mnuPos,'text','2-5') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_mnuAdherer"><?php $plxPlugin->lang('L_MENU_ADHERER_TITLE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('mnuAdherer',$mnuAdherer,'text') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_mnuAdhesion"><?php $plxPlugin->lang('L_MENU_ADHESION_TITLE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('mnuAdhesion',$mnuAdhesion,'text') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_mnuMembers"><?php $plxPlugin->lang('L_MENU_MEMBERS_TITLE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('mnuMembers',$mnuMembers,'text') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_mnuConnexion"><?php $plxPlugin->lang('L_MENU_CONNEXION_TITLE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('mnuConnexion',$mnuConnexion,'text') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_mnuDeconnexion"><?php $plxPlugin->lang('L_MENU_DECONNEXION_TITLE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('mnuDeconnexion',$mnuDeconnexion,'text') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered field toggle"><label for="id_desc_adhesion"><?php $plxPlugin->lang('L_MENU_DESC') ?>&nbsp;:</label>
			</div>
			<div class="col sml-12 med-8 lrg-7"><a id="toggler_desc" href="javascript:void(0)" onclick="toggleDiv('toggle_desc', 'toggler_desc', '<?php $plxPlugin->lang('L_DISPLAY') ?>','<?php $plxPlugin->lang('L_HIDE') ?>')"><?php $plxPlugin->lang('L_DISPLAY') ?></a>
				<div id="toggle_desc" style="display:none"><?php plxUtils::printArea('desc_adhesion',$desc_adhesion, '', '20', false, 'full-width') ?></div></div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_mnuForgetMyPass"><?php $plxPlugin->lang('L_MENU_FORGET_MY_PASS') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('mnuForgetMyPass',$mnuForgetMyPass,'text') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_mnuMyAccount"><?php $plxPlugin->lang('L_MENU_MY_ACCOUNT') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('mnuMyAccount',$mnuMyAccount,'text') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_showAnnuaire"><?php $plxPlugin->lang('L_SHOW_MENU_ANNUAIRE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printSelect('showAnnuaire',$aAnnuaire,$showAnnuaire) ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_publicAnnuaire"><?php $plxPlugin->lang('L_MENU_ACCES_ANNUAIRE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printSelect('publicAnnuaire',$aAnnuaire,$publicAnnuaire) ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_typeAnnuaire"><?php $plxPlugin->lang('L_TYPE_ANNUAIRE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printSelect('typeAnnuaire',$aTypeAnnuaires,$typeAnnuaire) ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_tabActivites"><?php $plxPlugin->lang('L_TAB_ACTIVITIES') ?>&nbsp;<a class="help hint"><span><?php $plxPlugin->lang('L_TAB_ACTIVITIES_HELP') ?></span></a>:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('tabActivites',$tabActivites,'text') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_mnuAnnuaire"><?php $plxPlugin->lang('L_MENU_ANNUAIRE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('mnuAnnuaire',$mnuAnnuaire,'text') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_cle"><?php $plxPlugin->lang('L_CLE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printSelect('cle', $aClefs, $cle) ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_annee"><?php $plxPlugin->lang('L_ANNEE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printSelect('annee', $aAnnee, $annee) ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_annee_ss_cotis"><?php $plxPlugin->lang('L_ANNEE_SS_COTIS') ?>&nbsp;<a class="hint help"><span><?php $plxPlugin->lang('L_ANNEE_SS_COTIS_HELP') ?></span></a>:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printSelect('annee_ss_cotis', $aAnneesc, $annee_ss_cotis) ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_nom_asso"><?php $plxPlugin->lang('L_NOM_ASSO') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('nom_asso',$nom_asso,'text','50-120') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered field toggle"><label for="id_adresse_asso"><?php $plxPlugin->lang('L_ADRESSE_ASSO') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7"><a id="toggler_ad" href="javascript:void(0)" onclick="toggleDiv('toggle_ad', 'toggler_ad', '<?php $plxPlugin->lang('L_DISPLAY') ?>','<?php $plxPlugin->lang('L_HIDE') ?>')"><?php $plxPlugin->lang('L_DISPLAY') ?></a>
		<div id="toggle_ad" style="display:none"><?php plxUtils::printArea('adresse_asso',$adresse_asso, '', '5', false, 'full-width') ?></div>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_domaine_asso"><?php $plxPlugin->lang('L_DOMAINE_ASSO') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('domaine_asso',$domaine_asso,'text','50-120') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_email"><?php $plxPlugin->lang('L_EMAIL') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('email',$email,'text','50-120') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_subject"><?php $plxPlugin->lang('L_EMAIL_SUBJECT') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('subject',$subject,'text','100-120') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_thankyou"><?php $plxPlugin->lang('L_THANKYOU_MESSAGE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('thankyou',$thankyou,'text','100-680') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_subject_password"><?php $plxPlugin->lang('L_PASSWD_SUBJECT') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('subject_password',$subject_password,'text','100-120') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_msg_password"><?php $plxPlugin->lang('L_PASSWD_MESSAGE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('msg_password',$msg_password,'text','100-180') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_validation_subject"><?php $plxPlugin->lang('L_VALIDATION_SUBJECT') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('validation_subject',$val_sub,'text','100-180') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_validation_msg"><?php $plxPlugin->lang('L_VALIDATION_MESSAGE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('validation_msg',$val_msg,'text','100-180') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_cotis_subject"><?php $plxPlugin->lang('L_COTIS_SUBJECT') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('cotis_subject',$cotis_sub,'text','100-120') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_cotis_msg"><?php $plxPlugin->lang('L_COTIS_MESSAGE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('cotis_msg',$cotis_msg,'text','100-180') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_devalidation_subject"><?php $plxPlugin->lang('L_DEVALIDATION_SUBJECT') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('devalidation_subject',$deval_sub,'text','100-120') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_devalidation_msg"><?php $plxPlugin->lang('L_DEVALIDATION_MESSAGE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('devalidation_msg',$deval_msg,'text','100-180') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_rappel_subject"><?php $plxPlugin->lang('L_RAPPEL_SUBJECT') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('rappel_subject',$rappel_sub,'text','100-120') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_rappel_msg"><?php $plxPlugin->lang('L_RAPPEL_MESSAGE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printInput('rappel_msg',$rappel_msg,'text','100-180') ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_template"><?php $plxPlugin->lang('L_TEMPLATE') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printSelect('template', $aTemplates, $template) ?>
			</div>
		</div>
		<div class="grid">
			<div class="col sml-12 med-4 lrg-5 label-centered"><label for="id_perPage"><?php $plxPlugin->lang('L_LABEL_TABLE_PERPGS') ?>&nbsp;:</label></div>
			<div class="col sml-12 med-8 lrg-7">
				<?php plxUtils::printSelect('perPage', array(5=>5,10=>10,15=>15,20=>20,25=>25,50=>50,100=>100), $perPage) ?>
			</div>
		</div>

		<p id="sendConfig" class="in-action-bar">
			<?php echo plxToken::getTokenPostMethod() ?>
			<input type="submit" name="submit" value="<?php $plxPlugin->lang('L_SAVE') ?> &radic;" />
			<a class="back blue button" href="plugin.php?p=<?php echo get_class($plxPlugin) ?>"><?php echo $plxPlugin->getInfo('title') ?></a>
		</p>

	</fieldset>
</form>
