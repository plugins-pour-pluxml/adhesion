<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 * $this IS plxShow
 * @version 2.1.1
 * @date    15/04/2019
 * @author  Stephane F, Cyril MAGUIRE, Thomas Ingles
 **/
$useCapcha = TRUE;
include('form.init.inc.php');#init plug & capcha
$linkToken = $plxPlugin->getTokenLink();
if($linkToken == '') {
    header('Location:'.$this->plxMotor->urlRewrite());
    exit();
}
$plxPlugin->getAdherents('/^[0-9]{5}.(.[a-z-]+){2}.[0-9]{10}.xml$/');

foreach ($plxPlugin->plxRecord_adherents->result as $key => $account) {
    if ($account['rand1'] == $linkToken) {
        $compte = $plxPlugin->plxRecord_adherents->result[$key];
        break;
    }
}
if (!isset($compte)) {
    //On supprime les index de session
    unset($_SESSION['lockArticles']);
    unset($_SESSION['account']);
    unset($_SESSION['domainAd']);
    header('Location:'.$this->plxMotor->urlRewrite());
    exit();
}

//Définition de variables
$pro = array();
$error = array();
$success=false;
$wall_e = '';
if(!empty($_POST) && !empty($_POST['wall-e'])) {
    $wall_e = $_POST['wall-e'];
}
if(!empty($_POST) && isset($_POST['password'],$_POST['verif']) && !empty($_POST['password']) && empty($wall_e)) {
    $compte['password'] = plxUtils::strCheck($_POST['password']);
    if (false === $plxPlugin->PW->isPasswordStrong($compte['password'], $plxPlugin->getPwLen()) ) {
        $error[] = $plxPlugin->getLang('L_PASSWORD_NOT_STRONG');
    }
    if (hash('whirlpool',$compte['salt']) != $_POST['verif']) {
        $error[] = $plxPlugin->getLang('L_ERR_DURING_RECORD');
    } 
    if(empty($error)) {
        $compte['rand1'] = $plxPlugin->generate_string(14);
        if ($plxPlugin->editMyAccount($compte,$compte['id'])) {//On édite le compte de l'adhérent
            $_SESSION['erase']['m'] = '<p id="password_success" class="success">'.$plxPlugin->getLang('L_EDIT_OK').'</p>';
            $_SESSION['erase']['d'] = 's';//display what
            header('Location:'.$this->plxMotor->urlRewrite());
            exit;
        } else {
            $error = array($plxPlugin->getLang('L_INTERNAL_ERR'));
        }
    }
}
?>
<p id="all_required"><?php echo sprintf($plxPlugin->getLang('L_FORM_ALL_REQUIRED'),'<exp class="mandatory">*</exp>');?></p>
<div id="form_adherer">
<?php if(!empty($error)): ?>
        <div class="contact_error">
<?php if(!empty($error)): ?>
                <h3><?php $plxPlugin->lang('L_FORM_FIELDS_MISSING') ?></h3>
                <ul>
<?php foreach ($error as $e) {
                    echo PHP_EOL.'                      <li>'.$e.'</li>';
                }
?>
                </ul>
<?php endif; ?>
        </div>
    <?php endif;
    unset($_POST);?>
    <form action="#form" method="post">
        <fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_SECURITY');?></h2></legend>
        <p>
            <label for="password"><?php $plxPlugin->lang('L_EDIT_PASSWORD') ?>&nbsp;:
                <input id="password" name="password" type="password" size="30" pattern=".{12,}" value="" maxlength="90" required /><exp class="mandatory">*</exp>
                <br/>
                <small><?php echo sprintf($plxPlugin->getLang('L_SECURITY_RULES'),$plxPlugin->getPwLen()) ?></small>
            </label>
            <input type="hidden" name="verif" value="<?php echo hash('whirlpool',$compte['salt']);?>">
        </p>
        <p class="wall-e">
            <label for="walle2"><?php $plxPlugin->lang('L_FORM_WALLE') ?>
                <input id="walle2" name="wall-e" type="text" size="50" value="<?php echo plxUtils::strCheck($wall_e) ?>" maxlength="50" />
            </label>
        </p>
        <p class="text-right">
            <input type="submit" name="submit" value="<?php $plxPlugin->lang('L_FORM_BTN_SEND') ?>" />
        </p>
        </fieldset>
    </form>
</div>