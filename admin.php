<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 *
 * @version	2.0.0
 * @date	23/10/2018
 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
 **/
$pluginName = get_class($plxPlugin);
if(!empty($_GET) AND !empty($_GET['config'])){
 # Controle de l'accès à la page en fonction du profil de l'utilisateur connecté
 $plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MANAGER);
 include(PLX_PLUGINS.$pluginName.'/config.php');
 return;
}
# Hook Plugins
if(eval($plxAdmin->plxPlugins->callHook('AdminAdhesionBegin'))) return;
//ADMIN CLASSIQ (ADHERENTS)
$prfl = ($_SESSION['profil'] > PROFIL_ADMIN);
$cnfHref = ($prfl?'':'parametres_').'plugin.php?p='.$pluginName.($prfl?'&amp;config=ok':'');
//parametres_plugin.php?p=adhesion
# Control du token du formulaire
plxToken::validateFormToken($_POST);
$a = $plxPlugin->getAdherents('/^[0-9]{5}.(.[a-z-]+){2}.[0-9]{10}.xml$/');
//$aActivites = array('arc'=>'ARC','tec'=>'TEC','irc'=>'IRC','autre'=>'Autre');
$aA = explode(',',$plxPlugin->getParam('tabActivites'));
$aK = array_map('strtolower', $aA);
$aActivites = array_combine($aK, $aA);
$settings = ($plxPlugin->getParam('showAnnuaire') != 'on'?'':PHP_EOL.'<li><a href="'.$plxPlugin->plxMotor->urlRewrite('?annuaire.html').'" title="'.$plxPlugin->getLang('L_ANNUAIRE').'"><img alt="'.$plxPlugin->getLang('L_ANNUAIRE').'" src="'.PLX_PLUGINS.$pluginName.'/annu.png" /></a></li>').($_SESSION['profil'] > PROFIL_MANAGER?'':PHP_EOL.'<li><a href="'.$cnfHref.'" title="'.L_MENU_CONFIG.'"><img alt="'.L_MENU_CONFIG.'" src="'.PLX_PLUGINS.$pluginName.'/settings.png" /></a></li>');//L_PLUGINS_CONFIG  PROFIL_ADMIN
$enteteTableau = '
		<th>'.$plxPlugin->getLang('L_ADMIN_LIST_NAME').'<br/>'
			.$plxPlugin->getLang('L_ADMIN_LIST_FIRST_NAME').'<br/>&nbsp;</th>
		<th>'.$plxPlugin->getLang('L_ADMIN_LIST_ADRESSE').'<br/>'
			.$plxPlugin->getLang('L_ADMIN_LIST_ADRESSE').'<br/>'
			.$plxPlugin->getLang('L_ADMIN_LIST_ZIP_CODE').'&nbsp;'
			.$plxPlugin->getLang('L_ADMIN_LIST_CITY').'</th>
		<th>'.$plxPlugin->getLang('L_ADMIN_LIST_TEL').'<br/>'
			.$plxPlugin->getLang('L_ADMIN_LIST_MAIL').'<br/>&nbsp;</th>
		<th>'.$plxPlugin->getLang('L_ADMIN_LIST_CHOICE').'<br/>&nbsp;<br/>&nbsp;</th>
';
if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
$enteteTableau = '
		<th>'.$plxPlugin->getLang('L_ADMIN_LIST_NAME').'<br/>'
			.$plxPlugin->getLang('L_ADMIN_LIST_FIRST_NAME').'<br/>&nbsp;</th>
		<th>'.$plxPlugin->getLang('L_ADMIN_LIST_ACTIVITY').'<br/>'
			.$plxPlugin->getLang('L_ADMIN_LIST_STRUCTURE').'<br/>'
			.$plxPlugin->getLang('L_ADMIN_LIST_DPT').'</th>
		<th>'.$plxPlugin->getLang('L_ADMIN_LIST_ADRESSE').'<br/>'
			.$plxPlugin->getLang('L_ADMIN_LIST_ADRESSE').'<br/>'
			.$plxPlugin->getLang('L_ADMIN_LIST_ZIP_CODE').'&nbsp;'
			.$plxPlugin->getLang('L_ADMIN_LIST_CITY').'</th>
		<th>'.$plxPlugin->getLang('L_ADMIN_LIST_TEL').'<br/>'
			.$plxPlugin->getLang('L_ADMIN_LIST_TEL_OFFICE').'<br/>'
			.$plxPlugin->getLang('L_ADMIN_LIST_MAIL').'</th>
		<th>'.$plxPlugin->getLang('L_ADMIN_LIST_CHOICE').'<br/>&nbsp;<br/>&nbsp;</th>
';
}
if(isset($_GET['sendrappel']) && !empty($_GET['sendrappel'])) {//On envoie le rappel a cotisation
	$mail = str_replace('true&mail=','',base64_decode($_GET['sendrappel']));
	$num = (isset($_GET['num'])?2:1);
	if( $plxPlugin->sendRappel(plxUtils::strCheck($mail), $num) ) {
		$_SESSION['info'] = $plxPlugin->getLang('L_RAPPEL_'.$num).' '.$plxPlugin->getLang('L_RAPPEL_SENT');
	}else{
		$_SESSION['error'] = $plxPlugin->getLang('L_ERR_RAPPEL_SENT');
	}
	header('Location: plugin.php?p='.$pluginName);
	exit;
}
if(isset($_GET['forgetmypass']) && !empty($_GET['forgetmypass'])) {//Mot de passe oublié, on renvoie la clé si l'email correspond
	$mail = str_replace('true&mail=','',base64_decode($_GET['forgetmypass']));
	if($plxPlugin->retrieveMyPass(plxUtils::strCheck($mail) )) {
		$_SESSION['info'] = $plxPlugin->getLang('L_PASS_SENT');
	}else{
		$_SESSION['error'] = $plxPlugin->getLang('L_ERR_PASS_SENT');
	}
	header('Location: plugin.php?p='.$pluginName);
	exit;
}

if(!empty($_POST)) {# On édite les adhérent(e)s
//	echo'<pre>';print_r($_POST);echo '</pre>';exit();
	if (isset($_POST['adherentNum']) && $_POST['nom_'.$_POST['adherentNum'][0]] == $plxPlugin->getLang('L_FORM_NAME')){//remove if name == name, en fr nom == nom
		unset($_POST['nom_'.$_POST['adherentNum'][0]]);
		unset($_POST['prenom_'.$_POST['adherentNum'][0]]);
		unset($_POST['adresse1_'.$_POST['adherentNum'][0]]);
		unset($_POST['adresse2_'.$_POST['adherentNum'][0]]);
		unset($_POST['cp_'.$_POST['adherentNum'][0]]);
		unset($_POST['ville_'.$_POST['adherentNum'][0]]);
		unset($_POST['tel_'.$_POST['adherentNum'][0]]);
		unset($_POST['mail_'.$_POST['adherentNum'][0]]);
		unset($_POST['choix_'.$_POST['adherentNum'][0]]);
		unset($_POST['mailing_'.$_POST['adherentNum'][0]]);
		unset($_POST['validation_'.$_POST['adherentNum'][0]]);
		if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
			unset($_POST['activite_'.$_POST['adherentNum'][0]]);
			unset($_POST['activite_autre_'.$_POST['adherentNum'][0]]);
			unset($_POST['etablissement_'.$_POST['adherentNum'][0]]);
			unset($_POST['service_'.$_POST['adherentNum'][0]]);
			unset($_POST['tel_office_'.$_POST['adherentNum'][0]]);
		}
		if ($plxPlugin->getParam('showAnnuaire') == 'on') {
			unset($_POST['coordonnees_'.$_POST['adherentNum'][0]]);
		}
		unset($_POST['adherentNum']);//fix Undefined index: adherentNum if type annuaire pro (laissait les posts activie,  ... ,coordonnées)
	}
	$idAdh = isset($_POST['idAdherent'])?$_POST['idAdherent']:'0';
	$plxPlugin->editAdherentsList($_POST,$idAdh);//exit;
	header('Location: plugin.php?p='.$pluginName);//kiss
	exit;

}
# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminAdhesionTop'));
?>
<h4 id="title_config" class="hide"><?php $plxPlugin->lang('L_TITLE_CONFIG'); ?></h4>
<script type="text/javascript">//surcharge du titre dans l'admin
try{//pluxml 5.4+
 var title = document.getElementById('title_config').innerHTML;
 document.getElementsByClassName('inline-form')[0].firstChild.nextSibling.innerHTML = 'Adhesion - '+title;
}catch(e){console.log(e);}
</script>
	<div id="export" class="in-action-bar">
		<span class="adh-hide"><?php $plxPlugin->lang('L_ADMIN_EXPORT_LIST') ?></span>
		<ul style="margin: -0.33rem 0px;">
			<li><?php echo $settings ?></li>
			<li><a href="plugin.php?p=<?php echo $pluginName ?>&amp;print=ods" title="<?php $plxPlugin->lang('L_ADMIN_EXPORT_FORMAT')?> ods (LibreOffice)"><img src="<?php echo PLX_PLUGINS.$pluginName.'/opentbs/ods.png' ?>" alt="Format ods" /></a></li>
			<li><a href="plugin.php?p=<?php echo $pluginName ?>&amp;print=xlsx" title="<?php $plxPlugin->lang('L_ADMIN_EXPORT_FORMAT')?> xlsx (Excel 2010)"><img src="<?php echo PLX_PLUGINS.$pluginName.'/opentbs/xlsx.png' ?>" alt="Format xlsx" /></a></li>
			<li><a href="plugin.php?p=<?php echo $pluginName ?>&amp;print=xls" title="<?php $plxPlugin->lang('L_ADMIN_EXPORT_FORMAT')?> xls (Excel 2003)"><img src="<?php echo PLX_PLUGINS.$pluginName.'/opentbs/xls.png' ?> " alt="Format xls" /></a></li>
		</ul>
	</div>
<div id="title">
	<div id="tabs" class="css-tabs history">
		<a href="#plus" id="tabplus" title="<?php $plxPlugin->lang('L_ADMIN_LIST_MEMBER_NEW')?>">✚</a>
		<a href="#attente" id="tabattente"><?php $plxPlugin->lang('L_ADMIN_LIST_MEMBERS_TO_VALIDATE')?></a>
		<a href="#validees" id="tabvalidees"><?php $plxPlugin->lang('L_ADMIN_LIST_MEMBERS_VALIDATED')?></a>
<?php eval($plxAdmin->plxPlugins->callHook('AdminAdhesionUsersTabs')) # Hook Plugins ?>
	</div>
</div>

<div id="listes">
<?php eval($plxAdmin->plxPlugins->callHook('AdminAdhesionUsersListTop')) # Hook Plugins ?>

<!-- NOUVEL ADHERENT -->
<div class="onglet" id="plus">

<?php eval($plxAdmin->plxPlugins->callHook('AdminAdhesionUsersTopNew')) # Hook Plugins ?>

<form action="" method="post" id="form2">
<div class="scrollable-table">
	<table class="table full-width" id="table-new">
	<caption id="title_plus"><?php $plxPlugin->lang('L_ADMIN_LIST_NEW')?></caption>
	<thead>
		<tr class="new">
			<th class="checkbox"><!-- <input type="checkbox" onclick="checkAll(this.form, 'idAdherent[]')" /> --></th>
			<th class="title"><?php $plxPlugin->lang('L_ADMIN_LIST_ID'); ?></th>
			<?php echo $enteteTableau; ?>
		</tr>
	</thead>
	<tbody>
		<tr>
<?php
		$new_adherentid = $plxPlugin->nextIdAdherent();
		//Tableau pour saisir nouvel adhérent sans passer par la partie publique
			echo '<td><input id="checkbox_'.$new_adherentid.'" type="checkbox" name="idAdherent[]" value="'.$new_adherentid.'" style="display:none" /><input type="hidden" name="adherentNum[]" value="'.$new_adherentid.'" /><input type="hidden" name="new" value="'.$new_adherentid.'" /><input class="button_submit" type="submit" name="update" value="'.L_OK.'" onclick="document.getElementById(\'checkbox_'.$new_adherentid.'\').checked=true;" /></td>';
			echo '<td>'.$new_adherentid.'</td><td>';
			plxUtils::printInput('nom_'.$new_adherentid, $plxPlugin->getLang('L_FORM_NAME'), 'text', '15-50',false,'" pattern="[^0-9]+" title="'.$plxPlugin->getLang('L_ONLY_LETTERS').'" onfocus="if (this.value == \''.$plxPlugin->getLang('L_FORM_NAME').'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \''.$plxPlugin->getLang('L_FORM_NAME').'\';}" onsubmit="if (this.value == \''.$plxPlugin->getLang('L_FORM_NAME').'\') {this.value = \'\';}');echo '<br/>';
			plxUtils::printInput('prenom_'.$new_adherentid, $plxPlugin->getLang('L_FORM_FIRST_NAME'), 'text', '15-50',false,'" pattern="[^0-9]+" title="'.$plxPlugin->getLang('L_ONLY_LETTERS').'" onfocus="if (this.value == \''.$plxPlugin->getLang('L_FORM_FIRST_NAME').'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \''.$plxPlugin->getLang('L_FORM_FIRST_NAME').'\';}" onsubmit="if (this.value == \''.$plxPlugin->getLang('L_FORM_FIRST_NAME').'\') {this.value = \'\';}');
			echo '</td><td>';
			if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
				plxUtils::printSelect('activite_'.$new_adherentid, $aActivites, '','','activite_new" onchange="activite_new(this);');
				plxUtils::printInput('activite_autre_'.$new_adherentid, '', 'text', '15-50','','autre" placeholder="'.$aActivites['autre'].'" onfocus="activite_evnt(this,\'focus\');" onblur="activite_evnt(this,\'blur\');');echo '<br/>';
				plxUtils::printInput('etablissement_'.$new_adherentid, $plxPlugin->getLang('L_FORM_SOCIETY'), 'text', '15-150',false,'" onfocus="if (this.value == \''.$plxPlugin->getLang('L_FORM_SOCIETY').'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \''.$plxPlugin->getLang('L_FORM_SOCIETY').'\';}" onsubmit="if (this.value == \''.$plxPlugin->getLang('L_FORM_SOCIETY').'\') {this.value = \'\';}');echo '<br/>';
				plxUtils::printInput('service_'.$new_adherentid, $plxPlugin->getLang('L_FORM_SERVICE'), 'text', '15-150',false,'" onfocus="if (this.value == \''.$plxPlugin->getLang('L_FORM_SERVICE').'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \''.$plxPlugin->getLang('L_FORM_SERVICE').'\';}" onsubmit="if (this.value == \''.$plxPlugin->getLang('L_FORM_SERVICE').'\') {this.value = \'\';}');
				echo '</td><td>';
			}
			plxUtils::printInput('adresse1_'.$new_adherentid, $plxPlugin->getLang('L_FORM_ADDRESS'), 'text', '25-250',false,'" onfocus="if (this.value == \''.$plxPlugin->getLang('L_FORM_ADDRESS').'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \''.$plxPlugin->getLang('L_FORM_ADDRESS').'\';}" onsubmit="if (this.value == \''.$plxPlugin->getLang('L_FORM_ADDRESS').'\') {this.value = \'\';}');echo '<br/>';
			plxUtils::printInput('adresse2_'.$new_adherentid, '', 'text', '25-250');
			echo '<br/>';
			plxUtils::printInput('cp_'.$new_adherentid, $plxPlugin->getLang('L_FORM_ZIP_CODE'), 'text', '5-6',false,'" onfocus="if (this.value == \''.$plxPlugin->getLang('L_FORM_ZIP_CODE').'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \''.$plxPlugin->getLang('L_FORM_ZIP_CODE').'\';}" onsubmit="if (this.value == \''.$plxPlugin->getLang('L_FORM_ZIP_CODE').'\') {this.value = \'\';}').'&nbsp;';
			plxUtils::printInput('ville_'.$new_adherentid, $plxPlugin->getLang('L_FORM_CITY'), 'text', '15-150',false,'" onfocus="if (this.value == \''.$plxPlugin->getLang('L_FORM_CITY').'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \''.$plxPlugin->getLang('L_FORM_CITY').'\';}" onsubmit="if (this.value == \''.$plxPlugin->getLang('L_FORM_CITY').'\') {this.value = \'\';}');
			echo '</td><td>';
			plxUtils::printInput('tel_'.$new_adherentid, $plxPlugin->getLang('L_FORM_TEL'), 'text', '15-15',false,'" onfocus="if (this.value == \''.$plxPlugin->getLang('L_FORM_TEL').'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \''.$plxPlugin->getLang('L_FORM_TEL').'\';}" onsubmit="if (this.value == \''.$plxPlugin->getLang('L_FORM_TEL').'\') {this.value = \'\';}');
			echo '<br/>';
			if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
				plxUtils::printInput('tel_office_'.$new_adherentid, '', 'text', '15-15');
				echo '<br/>';
			}
			plxUtils::printInput('mail_'.$new_adherentid, $plxPlugin->getLang('L_NOTI_MAIL'), 'email', '25-150',false,'email" onfocus="if (this.value == \''.$plxPlugin->getLang('L_NOTI_MAIL').'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \''.$plxPlugin->getLang('L_NOTI_MAIL').'\';}" onsubmit="if (this.value == \''.$plxPlugin->getLang('L_NOTI_MAIL').'\') {this.value = \'\';}');
			echo '</td><td>';
			plxUtils::printSelect('choix_'.$new_adherentid, array('adhesion'=>$plxPlugin->getLang('L_ADHESION_OK'),'renouveler'=>$plxPlugin->getLang('L_ADHESION_RENEW')), '');echo '<br/>';
			if ($plxPlugin->getParam('showAnnuaire') == 'on') {
				plxUtils::printSelect('coordonnees_'.$new_adherentid, array('public'=>$plxPlugin->getLang('L_COORD_PUBLIC'),'rec'=>$plxPlugin->getLang('L_COORD_REC'),'refus'=>$plxPlugin->getLang('L_COORD_NO')), '');echo '<br/>';
			}
			plxUtils::printSelect('mailing_'.$new_adherentid, array('maillist'=>$plxPlugin->getLang('L_NEWS_OK'),'blacklist'=>$plxPlugin->getLang('L_NEWS_NO')), '');
			echo '</td>';

					# Hook Plugins
					eval($plxAdmin->plxPlugins->callHook('AdminAdhesionNew'));

?>
		</tr>
	</tbody>
	</table>
</div>
<?php echo plxToken::getTokenPostMethod(); ?>
</form>

</div>

<!-- ADHERENTS ENTRANTS SORTANTS -->
<div class="onglet" id="attente">

	<?php eval($plxAdmin->plxPlugins->callHook('AdminAdhesionUsersTop')) # Hook Plugins ?>

<form action="" method="post" id="form0" onsubmit="return adherentsVerif('0');">
	<p class="in-action-bar-nojs">
		<?php plxUtils::printSelect('selection[]', array( 'false' => $plxPlugin->getLang('L_ADMIN_SELECTION'), 'update' => $plxPlugin->getLang('L_ADMIN_LIST_UPDATE'), 'validation' => $plxPlugin->getLang('L_ADMIN_LIST_VALIDATION'), '-' => '-----','delete' => $plxPlugin->getLang('L_ADMIN_DELETE')), '', false, '', 'id_selection0') ?>
		<input class="button_submit" type="submit" id="submit0" name="submit0" value="<?php echo L_OK ?>" />
	</p>
<div class="scrollable-table">
	<table class="table full-width" id="table0">
	<caption id="title_attente"><?php $plxPlugin->lang('L_ADMIN_LIST_MEMBERS_TO_VALIDATE')?></caption>
	<thead>
		<tr class="adh wait">
			<th class="checkbox" data-sortable="false"><input type="checkbox" onclick="checkAll(this.form, 'idAdherent[]');document.getElementById('new_adherent').checked = false;" /></th>
			<th class="title"><?php $plxPlugin->lang('L_ADMIN_LIST_ID'); ?><br/><span class="petit"><?php $plxPlugin->lang('L_ADMIN_FIRST_ASK'); ?></span></th>
			<?php echo $enteteTableau; ?>
		</tr>
	</thead>
	<tbody>
		<?php
		$num = 0;
		$empty = '';
		if($a) { # On a des adhérents
			$num=0;# Initialisation de l'ordre
			while($plxPlugin->plxRecord_adherents->loop()) {# Pour chaque adhérent
				$ad = $plxPlugin->plxRecord_adherents;
				$vd = $ad->f('validation');//fix Fatal error: Can't use method return value in write context
				$ac = $ad->f('activite');
				$adId = $adisplay = $ad->f('id');
				if (!$vd) {// '0' => '' empty($ad->f('validation')) == Fatal error: Can't use method return value in write context in adhesion/admin.php
					$NewOrOld = $ad->f('firstDate') ? 'OLD' : 'NEW';#æncienne | Nouvelle demande
					$ordre = ++$num;
					if ($ac && !array_key_exists(strtolower($ac),$aActivites)) {
						$activite = 'autre';
						$activite_autre = $ac;
					} else {
						$activite = $ac;
						$activite_autre = '';
					}
					$firstDate = intval(plxUtils::strCheck($ad->f('firstDate')));
					echo "\n".'<!-- ADHERENT -->'."\n";
					echo '<tr class="line-'.($num%2).'">';

					# Hook Plugins
					eval($plxAdmin->plxPlugins->callHook('AdminAdhesionUserWait'));

					echo '<td><input type="checkbox" id="checkbox_'.$adId.'" name="idAdherent[]" value="'.$adId.'" /><input type="hidden" name="update" value="true" /><input type="hidden" id="validation_'.$adId.'" name="validation_'.$adId.'" value="0" /></td>';
					echo '<td>'.$adisplay; echo ($ad->f('firstDate') != ''? '<br/>
					<input type="hidden" id="firstDate_'.$adId.'" name="firstDate_'.$adId.'" value="'.$firstDate.'" />
					 <a title="'.$plxPlugin->getLang('L_CHANGE_DATE_BTN').' '.$plxPlugin->getLang('L_CHANGE_FIRST_DATE').'" href="javascript:void(0)" onclick="dialogBoxDate(\''.$adId.'\',true,\'firstDate\');return false;" id="btnChangeDate'.$adId.'">
					  <span id="date_text_'.$adId.'" class="petit">'.date($plxPlugin->getLang('L_DATE_FORMAT'),$firstDate).'</span>
					 </a>
					' : '');echo ($ad->f('date')!='' && $ad->f('validation') == '0') ? '<br/><span class="petit">'.$plxPlugin->getLang('L_ADMIN_DATE_DEL').'<br/>'.date($plxPlugin->getLang('L_DATE_FORMAT'),intval(plxUtils::strCheck($ad->f('date')))).'</span>' : '<br/><span class="petit">'.$plxPlugin->getLang('L_FORM_'.$NewOrOld).'</span>';
					echo '</td><td>';
					echo '<p class="datatable-search hidden">'.$ad->f('nom').' '.$ad->f('prenom').'</p>';//data
					plxUtils::printInput('nom_'.$adId, strtoupper(plxUtils::strCheck($ad->f('nom'))), 'text', '15-50','','" pattern="[^0-9]+" title="'.$plxPlugin->getLang('L_ONLY_LETTERS'),$plxPlugin->getLang('L_ONLY_LETTERS'));echo '<br/>';
					plxUtils::printInput('prenom_'.$adId, ucfirst(plxUtils::strCheck($ad->f('prenom'))), 'text', '15-50','','" pattern="[^0-9]+" title="'.$plxPlugin->getLang('L_ONLY_LETTERS'),$plxPlugin->getLang('L_ONLY_LETTERS'));
					echo '</td><td>';
					if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
					echo '<p class="datatable-search hidden">'.$activite.' '.$activite_autre.' '.$ad->f('etablissement').' '.$ad->f('service').'</p>';//data
					plxUtils::printSelect('activite_'.$adId, $aActivites, $activite,'','activite_new" onchange="activite_new(this);');
					plxUtils::printInput('activite_autre_'.$adId, $activite_autre, 'text', '15-50','','autre" placeholder="'.$aActivites['autre'].'" onfocus="activite_evnt(this,\'focus\');" onblur="activite_evnt(this,\'blur\');');echo '<br/>';
					plxUtils::printInput('etablissement_'.$adId, plxUtils::strCheck($ad->f('etablissement')), 'text', '15-150');echo '<br/>';
					plxUtils::printInput('service_'.$adId, plxUtils::strCheck($ad->f('service')), 'text', '15-150');
					echo '</td><td>';
					}
					echo '<p class="datatable-search hidden">'.$ad->f('adresse1').' '.$ad->f('adresse2').' '.$ad->f('cp').' '.$ad->f('ville').'</p>';//data
					plxUtils::printInput('adresse1_'.$adId, plxUtils::strCheck($ad->f('adresse1')), 'text', '25-250');echo '<br/>';
					plxUtils::printInput('adresse2_'.$adId, plxUtils::strCheck($ad->f('adresse2')), 'text', '25-250');
					echo '<br/>';
					plxUtils::printInput('cp_'.$adId, plxUtils::strCheck($ad->f('cp')), 'text', '5-6');
					plxUtils::printInput('ville_'.$adId, plxUtils::strCheck($ad->f('ville')), 'text', '15-150');
					echo '</td><td>';
					echo '<p class="datatable-search hidden">'.$ad->f('tel').'</p>';//data
					plxUtils::printInput('tel_'.$adId, plxUtils::strCheck($ad->f('tel')), 'text', '15-15');
					echo '<br/>';
					if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
					echo '<p class="datatable-search hidden">'.$ad->f('tel_office').'</p>';//data
					plxUtils::printInput('tel_office_'.$adId, plxUtils::strCheck($ad->f('tel_office')), 'text', '15-15');
					echo '<br/>';
					}
					echo '<p class="datatable-search hidden">'.$ad->f('mail').'</p>';//data
					plxUtils::printInput('mail_'.$adId, plxUtils::strCheck($ad->f('mail')), 'email', '25-150',false,'email');
					echo '</td><td>';
					echo '<p class="datatable-search hidden">'.$ad->f('choix').'</p>';//data
					plxUtils::printSelect('choix_'.$adId, array('adhesion'=>$plxPlugin->getLang('L_ADHESION_OK'),'renouveler'=>$plxPlugin->getLang('L_ADHESION_RENEW')), plxUtils::strCheck($ad->f('choix')));echo '<br/>';
					if ($plxPlugin->getParam('showAnnuaire') == 'on') {
					echo '<p class="datatable-search hidden">'.$ad->f('coordonnees').'</p>';//data
						plxUtils::printSelect('coordonnees_'.$adId, array('public'=>$plxPlugin->getLang('L_COORD_PUBLIC'),'rec'=>$plxPlugin->getLang('L_COORD_REC'),'refus'=>$plxPlugin->getLang('L_COORD_NO')), plxUtils::strCheck($ad->f('coordonnees')));echo '<br/>';
					}
					echo '<p class="datatable-search hidden">'.$ad->f('mailing').'</p>';//data
					plxUtils::printSelect('mailing_'.$adId, array('maillist'=>$plxPlugin->getLang('L_NEWS_OK'),'blacklist'=>$plxPlugin->getLang('L_NEWS_NO')), plxUtils::strCheck($ad->f('mailing')));
					echo '</td></tr>';
				}
			}//FI WHILE
			if ($num == 0) {
				$empty = '<p style="text-align:center;"><strong>'.$plxPlugin->getLang('L_ADMIN_VALIDATION_PENDING').'</strong></p>';
			}
			# On récupère le dernier identifiant
			$lastId = array_keys((array)$plxPlugin->plxRecord_adherents);
			rsort($lastId);
		}
		else {
			$empty = '<p style="text-align:center;"><strong>'.$plxPlugin->getLang('L_ADMIN_VALIDATION_PENDING').'</strong></p>';
			$lastId[1] = 0;
		}
?>

	</tbody>
	</table>
	<?php echo $empty ?>
<?php echo plxToken::getTokenPostMethod(); ?>
</div>
</form>
</div>

<!-- ADHERENTS VALIDES -->
<div class="onglet" id="validees">

<?php eval($plxAdmin->plxPlugins->callHook('AdminAdhesionUsersTopValid')) # Hook Plugins ?>

<form action="" method="post" id="form1" onsubmit="return adherentsVerif('1');">
	<p class="in-action-bar-nojs">
		<?php
			$aSelListValid = array('false' => $plxPlugin->getLang('L_ADMIN_SELECTION'), 'update' => $plxPlugin->getLang('L_ADMIN_LIST_UPDATE'));
			if($plxPlugin->getParam('annee') != 'illimite') $aSelListValid['cotise'] = $plxPlugin->getLang('L_ADMIN_LIST_COTISE');
			$aSelListValid['devalidation'] = $plxPlugin->getLang('L_ADMIN_LIST_DEVALIDATION');
			$aSelListValid['regenerepass'] = $plxPlugin->getLang('L_ADMIN_LIST_REGENEREPASS');
			$aSelListValid['-'] = '-----';
			$aSelListValid['delete'] = $plxPlugin->getLang('L_ADMIN_DELETE');
			plxUtils::printSelect('selection[]', $aSelListValid, '', false, '', 'id_selection1'); ?>
		<input class="button_submit" type="submit" id="submit1" name="submit1" value="<?php echo L_OK ?>" />
	</p>
<div class="scrollable-table">
	<table class="table full-width" id="table1">
	<caption id="title_validees"><?php $plxPlugin->lang('L_ADMIN_LIST_MEMBERS_VALIDATED')?></caption>
	<thead>
		<tr class="adh valid">
			<th class="checkbox" data-sortable="false"><input type="checkbox" onclick="checkAll(this.form, 'idAdherent[]')" /></th>
			<th class="title"><?php $plxPlugin->lang('L_ADMIN_LIST_ID'); ?><br/><span class="petit"><?php $plxPlugin->lang('L_ADMIN_DATE_VAL'); ?></span><br/><span class="petit"><?php $plxPlugin->lang('L_ADMIN_ACTION') ?></span></th>
<?php if($plxPlugin->getParam('annee') != 'illimite'): ?>
			<th>&nbsp;<br/><?php $plxPlugin->lang('L_COTIS'); ?><br/>&nbsp;</th>
<?php endif; ?>
			<?php echo $enteteTableau; ?>
<!--
			<th><?php $plxPlugin->lang('L_ADMIN_ACTION') ?>&nbsp;</th>
-->
		</tr>
	</thead>
	<tbody>
<?php
		$empty = '';
		if($a) {# On a des adhérents
			$logins = array();
			foreach($plxPlugin->getPasswords() as $compte){#Pour afficher logins & mots de passes. #togglerlogin
				$logins[$compte['id']] = array(str_replace(array('-','_'),'',plxUtils::title2url(strtolower($compte['nom'].$compte['prenom'] ))), $compte['cle'].'-'.substr($compte['mail'],0,-$compte['rand1']).$compte['rand2']);
			}
			unset($compte);

			# Initialisation de l'ordre
			$num=0;
			while($plxPlugin->plxRecord_adherents->loop()) {# Pour chaque adhérent
				$ad = $plxPlugin->plxRecord_adherents;
				$vd = $ad->f('validation');//fix  Fatal error: Can't use method return value in write context
				$ac = $ad->f('activite');
				$adId = $adisplay = $ad->f('id');
				if ($vd) {// == '1' Fatal error: Can't use method return value in write context in adhesion/admin.php
					if ($ac && !array_key_exists(strtolower($ac),$aActivites)) {
						$activite = 'autre';
						$activite_autre = $ac;
					} else {
						$activite = $ac;
						$activite_autre = '';
					}
					$ordre = ++$num;
					$firstDate = intval(plxUtils::strCheck($ad->f('firstDate')));
					$validationDate = intval(plxUtils::strCheck($ad->f('date')));

					# Hook Plugins
					eval($plxAdmin->plxPlugins->callHook('AdminAdhesionValidTop'));

					echo "\n".'<!-- ADHERENT -->'."\n";
					echo '<tr class="line-'.($num%2).'">';

					# Hook Plugins
					eval($plxAdmin->plxPlugins->callHook('AdminAdhesionValid'));

					echo '<td><input type="checkbox" id="checkbox_'.$adId.'" name="idAdherent[]" value="'.$adId.'" /><input type="hidden" name="update" value="true" /><input type="hidden" id="validation_'.$adId.'" name="validation_'.$adId.'" value="1" />
					<input type="hidden" id="firstDate_'.$adId.'" name="firstDate_'.$adId.'" value="'.$firstDate.'" />
					<input type="hidden" id="date_'.$adId.'" name="date_'.$adId.'" value="'.$validationDate.'" />
					<br/>
						<a title="'.$plxPlugin->getLang('L_CHANGE_DATE_BTN').' '.$plxPlugin->getLang('L_CHANGE_FIRST_DATE').'" href="javascript:void(0)" onclick="dialogBoxDate(\''.$adId.'\',false,\'firstDate\');return false;" id="btnChangeDate'.$adId.'"><img src="'.PLX_PLUGINS.$pluginName.'/calendar.png"></a>
					</td>';
					echo '<td>'.$adisplay.'<br/>
						<a title="'.$plxPlugin->getLang('L_CHANGE_DATE_BTN').' '.$plxPlugin->getLang('L_CHANGE_DATE').'" href="javascript:void(0)" onclick="dialogBoxDate(\''.$adId.'\',true,\'date\');return false;" id="btnChangeDate'.$adId.'">
							<span id="date_text_'.$adId.'" class="petit">'.date($plxPlugin->getLang('L_DATE_FORMAT'),$validationDate).'</span>
						</a>
					<br/><a class="enveloppe" href="plugin.php?p=adhesion&amp;forgetmypass='.base64_encode('true&mail='.$ad->f('mail')).'" title="'.$plxPlugin->getLang('L_ADMIN_SEND_PASS').'"><span>&#9993;</span></a>&nbsp;<a title="('.L_VIEW.'/'.L_HIDE.') '.$plxPlugin->getLang('L_ID').' &amp; '.$plxPlugin->getLang('L_FORM_PASSWORD').'" id="togglerlogin'.$adId.'" href="javascript:void(0)" onclick="toggleDiv(\'login'.$adId.'\',\'togglerlogin'.$adId.'\',\'✚\',\'—\')" class="togglerlogin">✚</a><div id="login'.$adId.'" style="display:none">'.$logins[$adId][0].'</div></td>';
					if($plxPlugin->getParam('annee') != 'illimite') echo '<td>'.$plxPlugin->cotisationAJour($validationDate,true,$ad->f('mail')).'</td>';
					echo '<td><p class="datatable-search hidden">'.$ad->f('nom').' '.$ad->f('prenom').'</p>';//data
					plxUtils::printInput('nom_'.$adId, strtoupper(plxUtils::strCheck($ad->f('nom'))), 'text', '15-50','','" pattern="[^0-9]+" title="'.$plxPlugin->getLang('L_ONLY_LETTERS'),$plxPlugin->getLang('L_ONLY_LETTERS'));echo '<br/>';
					plxUtils::printInput('prenom_'.$adId, ucfirst(plxUtils::strCheck($ad->f('prenom'))), 'text', '15-50','','" pattern="[^0-9]+" title="'.$plxPlugin->getLang('L_ONLY_LETTERS'),$plxPlugin->getLang('L_ONLY_LETTERS'));
					echo '</td><td>';
					if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
						echo '<p class="datatable-search hidden">'.$activite.' '.$activite_autre.' '.$ad->f('etablissement').' '.$ad->f('service').'</p>';//data
						plxUtils::printSelect('activite_'.$adId, $aActivites, $activite,'','activite_new" onchange="activite_new(this);');
						plxUtils::printInput('activite_autre_'.$adId, $activite_autre, 'text', '15-50','','autre" placeholder="'.$aActivites['autre'].'" onfocus="activite_evnt(this,\'focus\');" onblur="activite_evnt(this,\'blur\');');echo '<br/>';
						plxUtils::printInput('etablissement_'.$adId, plxUtils::strCheck($ad->f('etablissement')), 'text', '15-150');echo '<br/>';
						plxUtils::printInput('service_'.$adId, plxUtils::strCheck($ad->f('service')), 'text', '15-150');
						echo '</td><td>';
					}
					echo '<p class="datatable-search hidden">'.$ad->f('adresse1').' '.$ad->f('adresse2').' '.$ad->f('cp').' '.$ad->f('ville').'</p>';//data
					plxUtils::printInput('adresse1_'.$adId, plxUtils::strCheck($ad->f('adresse1')), 'text', '25-250');echo '<br/>';
					plxUtils::printInput('adresse2_'.$adId, plxUtils::strCheck($ad->f('adresse2')), 'text', '25-250');
					echo '<br/>';
					plxUtils::printInput('cp_'.$adId, plxUtils::strCheck($ad->f('cp')), 'text', '5-6');
					plxUtils::printInput('ville_'.$adId, plxUtils::strCheck($ad->f('ville')), 'text', '15-150');
					echo '</td><td>';
					plxUtils::printInput('tel_'.$adId, plxUtils::strCheck($ad->f('tel')), 'text', '15-15');
					echo '<br/>';
					if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
						echo '<p class="datatable-search hidden">'.$ad->f('tel').'</p>';//data
						plxUtils::printInput('tel_office_'.$adId, plxUtils::strCheck($ad->f('tel_office')), 'text', '15-15');
						echo '<br/>';
					}
					echo '<p class="datatable-search hidden">'.$ad->f('mail').'</p>';//data
					plxUtils::printInput('mail_'.$adId, plxUtils::strCheck($ad->f('mail')), 'email', '25-150',false,'email');
					echo '</td><td>';
					echo '<p class="datatable-search hidden">'.$ad->f('choix').'</p>';//data
					plxUtils::printSelect('choix_'.$adId, array('adhesion'=>$plxPlugin->getLang('L_ADMIN_FIRST_SEL').date($plxPlugin->getLang('L_DATE_FORMAT'),intval(plxUtils::strCheck($ad->f('firstDate')))),'renouveler'=>$plxPlugin->getLang('L_ADHESION_RENEW'),'stop'=>$plxPlugin->getLang('L_ADHESION_STOP')), plxUtils::strCheck($ad->f('choix')),false,'choices" onchange="choice(this);');echo '<br/>';
					if ($plxPlugin->getParam('showAnnuaire') == 'on') {
						echo '<p class="datatable-search hidden">'.$ad->f('coordonnees').'</p>';//data
						plxUtils::printSelect('coordonnees_'.$adId, array('public'=>$plxPlugin->getLang('L_COORD_PUBLIC'),'rec'=>$plxPlugin->getLang('L_COORD_REC'),'refus'=>$plxPlugin->getLang('L_COORD_NO')), plxUtils::strCheck($ad->f('coordonnees')));echo '<br/>';
					}
					echo '<p class="datatable-search hidden">'.$ad->f('mailing').'</p>';//data
					plxUtils::printSelect('mailing_'.$adId, array('maillist'=>$plxPlugin->getLang('L_NEWS_OK'),'blacklist'=>$plxPlugin->getLang('L_NEWS_NO')), plxUtils::strCheck($ad->f('mailing')));
					echo '</td><!-- <td style="text-align:left"></td> --></tr>';

					# Hook Plugins
					eval($plxAdmin->plxPlugins->callHook('AdminAdhesionValidFoot'));

				}
			}
			if ($num == 0) {
				$empty = '<p style="text-align:center;"><strong>'.$plxPlugin->getLang('L_ADMIN_NO_VALIDATION').'</strong></p>';
			}
			# On récupère le dernier identifiant
			$a = array_keys($plxPlugin->adherentsList);
			rsort($a);
		}
		else {
			$empty = '<p style="text-align:center;"><strong>'.$plxPlugin->getLang('L_ADMIN_NO_VALIDATION').'</strong></p>';
			$a[1] = 0;
		}

		# Hook Plugins
		eval($plxAdmin->plxPlugins->callHook('AdminAdhesionValidTbody'));

?>
	</tbody>
	</table>
	<?php echo $empty ?>
<?php echo plxToken::getTokenPostMethod() ?>
</div>
</form>

</div>

<?php eval($plxAdmin->plxPlugins->callHook('AdminAdhesionUsersListFoot')) # Hook Plugins ?>

</div><!-- fin #liste -->

	<!-- Change Date Dialog Box -->
	<div id="dlgChangeDate" class="dialogue" style="z-index:100000">
		<div class="dialogue-content" style="position:relative">
			<span class="dialogue-close">&times;</span>
			<p><?php $plxPlugin->lang('L_DATE') ?> <b id="id_newdate_title"></b><br/><?php $plxPlugin->lang('L_FORM_IDENTITY') ?>&nbsp;:&nbsp;<i id="id_identity_title"></i></p>
			<div class="col sml-12 lrg-6" style="padding:0">
				<p><?php $plxPlugin->lang('L_DATE') ?>&nbsp;:&nbsp;</p>
				<input id="id_newdate" type="date" name="newdate" value="" class="datepickermm" data-value="<?php echo date('Y/m/d') ?>" />
			</div>
			<div class="col sml-12 lrg-6" style="padding:0">
				<p><?php $plxPlugin->lang('L_TIME') ?>&nbsp;:&nbsp;</p>
				<input id="id_newtime" type="time" name="newtime" value="" class="timepickermm" data-value="<?php echo date('H:i') ?>" />
			</div>
			<input id="id_epodate" type="hidden" name="epodate" />
			<input id="id_epod" type="hidden" name="epod" />
			<input id="id_epot" type="hidden" name="epot" />
			<p class="text-center col sml-12"><a href="javascript:void(0);" id="btn_ChangeDate" class="button green" title="<?php $plxPlugin->lang('L_CHANGE_DATE_BTN') ?>"><?php $plxPlugin->lang('L_NEW_DATE') ?></a></p>
			<p>&nbsp;</p>
		</div>
	</div>

<?php eval($plxAdmin->plxPlugins->callHook('AdminAdhesionUsersFoot')); # Hook Plugins ?>

<script type="text/javascript">
<?php if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') :
//OBSO? IN Hook('AdminAdhesionUsersFoot') IN vanilla + CSS http://codeblog.cz/vanilla/essentials.html#jquery-function ?>
<?php endif; ?>

</script>
<script type="text/javascript" src="<?php echo PLX_PLUGINS.$pluginName ?>/js/jquery-cookie/jquery.cookie.js"></script><!-- mem de tabs.js -->
<script type="text/javascript" src="<?php echo PLX_PLUGINS.$pluginName ?>/js/tab.js"></script>
<script type="text/javascript" src="<?php echo PLX_PLUGINS.$pluginName ?>/js/jquery-dateformat.min.js"></script><!-- set new date text gh : phstc/jquery-dateFormat -->
<!-- default -->
<link rel="stylesheet" href="<?php echo PLX_PLUGINS.$pluginName ?>/js/pickadate.js-3.5.6/lib/compressed/themes/default.css" id="theme_base">
<link rel="stylesheet" href="<?php echo PLX_PLUGINS.$pluginName ?>/js/pickadate.js-3.5.6/lib/compressed/themes/default.date.css" id="theme_date">
<link rel="stylesheet" href="<?php echo PLX_PLUGINS.$pluginName ?>/js/pickadate.js-3.5.6/lib/compressed/themes/default.time.css" id="theme_time">

<!-- classic 
<link rel="stylesheet" href="<?php echo PLX_PLUGINS.$pluginName ?>/js/pickadate.js-3.5.6/lib/compressed/themes/classic.css" id="theme_base">
<link rel="stylesheet" href="<?php echo PLX_PLUGINS.$pluginName ?>/js/pickadate.js-3.5.6/lib/compressed/themes/classic.date.css" id="theme_date">
<link rel="stylesheet" href="<?php echo PLX_PLUGINS.$pluginName ?>/js/pickadate.js-3.5.6/lib/compressed/themes/classic.time.css" id="theme_time">
-->
<script type="text/javascript" src="<?php echo PLX_PLUGINS.$pluginName ?>/js/pickadate.js-3.5.6/lib/compressed/picker.js"></script>
<script type="text/javascript" src="<?php echo PLX_PLUGINS.$pluginName ?>/js/pickadate.js-3.5.6/lib/compressed/picker.date.js"></script>
<script type="text/javascript" src="<?php echo PLX_PLUGINS.$pluginName ?>/js/pickadate.js-3.5.6/lib/compressed/picker.time.js"></script>
<script type="text/javascript" src="<?php echo PLX_PLUGINS.$pluginName ?>/js/pickadate.js-3.5.6/lib/compressed/legacy.js"></script>
<script type="text/javascript" src="<?php echo PLX_PLUGINS.$pluginName ?>/js/pickadate.js-3.5.6/lib/compressed/translations/<?php $plxPlugin->lang('L_LANG_ISO') ?>.js"></script>
<script type="text/javascript">
/* dialogBox() pluxml funk de visual.js */
function dialogueBox(dlg) {
	this.dlg = document.getElementById(dlg);
	this.span = document.querySelector('#'+dlg+' .dialogue-close');
	var self = this;
	this.open = function() {
		self.dlg.style.display = "block";
	}
	this.close = function() {
		self.dlg.style.display = "none";
	}
	this.addEvent = function (element, evnt, funct){
		if (element.attachEvent)//addEventListener is not supported in <= IE8
			return element.attachEvent('on'+evnt, funct);
		else
		return element.addEventListener(evnt, funct, false);
	}
	this.addEvent(this.span, 'click', this.close);
	this.open();
}

Date.prototype.getUnixTime = function() { return this.getTime()/1000|0 };
function isInt(value) {
 return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
}
//ChangeDate #bep
var btn_ChangeDate = document.getElementById('btn_ChangeDate');

var gapochDB = document.getElementById('id_epodate');//epoch time : full unix time stamp ;)
var gapodDB = document.getElementById('id_epod');//without time
var gapotDB = document.getElementById('id_epot');//Only time
var gadateDB = document.getElementById('id_newdate');
var gatimeDB = document.getElementById('id_newtime');
var $pickd = $('#id_newdate').pickadate({
    //editable: true,
    format: '<?php $plxPlugin->lang('L_FORMAT_SUBMIT') ?>',
    formatSubmit: 'yyyy-mm-dd',
    selectYears: true,//10 by default
    selectMonths: true,
    firstDay: 1,
//    closeOnSelect: false,
    closeOnClear: false,
//    min: -15,// An integer (positive/negative) sets it relative to today.
//    max: true,// `true` sets it to today. `false` removes any limits.
    onStart: function() {
        console.log( 'Hello there :)' )
    },
    onRender: function() {
        console.log( 'Whoa.. rendered anew' )
    },
    onOpen: function() {
        console.log( 'Opened up' )
    },
    onClose: function() {
        console.log( 'Closed now' )
    },
    onStop: function() {
        console.log( 'See ya.' )
    },
    onSet: function( event ) {
        if(event.select && isInt(event.select/1000)){
        //  var tm = gatimeDB.value.split(':');
        //  gapotDB.value = (tm[0] * 60) + tm[1];// time
            gapochDB.value = ((event.select / 1000 ) + 1 * gapotDB.value);// + time .toNumber()
            gapodDB.value = 1 *(event.select / 1000);// no time
            btn_ChangeDate.style.visibility = '';//un hide validate link
            console.log( 'Just set stuff date  in: ' + gapochDB.value, event )
        }
//        console.log( 'Just set stuff date: ', event )
    }
});
var pickd = $pickd.pickadate( 'picker' );

var $pickt = $('#id_newtime').pickatime({
    //editable: true,
    format: 'HH:i',
    formatSubmit: 'H.i',
    onStart: function() {
        console.log( 'Hello there :)' )
    },
    onRender: function() {
        console.log( 'Whoa.. rendered anew' )
    },
    onOpen: function() {
        console.log( 'Opened up' )
    },
    onClose: function() {
        console.log( 'Closed now' )
    },
    onStop: function() {
        console.log( 'See ya.' )
    },
    onSet: function( event ) {
    if(isInt(event.select)){
            gapotDB.value = event.select * 60;// time
            gapochDB.value = (1 * gapodDB.value + 1 * gapotDB.value);// date + time in epoch
//          gapodDB.value = event.select / 1000;// no time
/*    
            var tm = gatimeDB.value.split(':');
            gapochDB.value = (event.select + (tm[0] * 60) + tm[1]) / 1000;// + time
*/    
            btn_ChangeDate.style.visibility = '';//un hide validate link
            console.log( 'Just set stuff time: ' + gapochDB.value, event );
        }
    }
});
var pickt = $pickt.pickatime( 'picker' );

/*
var $input_object__node = $( '#demo__api-object--node' ).pickadate(),
    picker_object__node = $input_object__node.pickadate( 'picker' )
$( '#button__api-object--node' ).on( 'click', function( event ) {
    console.log( picker_object__node.$node )
    event.stopPropagation()
})
*/

//console.log( 'Just : ',pickd,pickt);
function dialogBoxDate(adh,show,who) {
	//console.log( 'Justinfunk : ',pickd,pickt);
	var dlg = 'dlgChangeDate';
	var who = who?who:'date';
	this.adh = adh;
	this.vld = document.getElementById('validation_'+adh).value;// 0 / 1
	this.show = show;//first|last date
	this.ida = '#' + adh + ' ' + document.getElementById('id_nom_'+adh).value + ' ' + document.getElementById('id_prenom_'+adh).value;
	this.tit = (who!='date'? '<?php $plxPlugin->lang('L_CHANGE_FIRST_DATE') ?>': '<?php $plxPlugin->lang('L_CHANGE_DATE') ?>');
	//this.btn = document.getElementById('btn_ChangeDate').value = (who!='date'? '<?php $plxPlugin->lang('L_CHANGE_FIRST_DATE') ?>': '<?php $plxPlugin->lang('L_CHANGE_DATE') ?>');
	document.getElementById('id_identity_title').innerText = this.ida;
	document.getElementById('id_newdate_title').innerText = this.tit;
	this.span = document.querySelector('#'+dlg+' .dialogue-close');
	this.btn = document.getElementById('btn_ChangeDate');//.querySelector('#'+dlg+' .dialogue-close');
	var self = this;
	this.adate = document.getElementById('date_text_'+adh);
	this.apoch = document.getElementById(who+'_'+adh);
 
	//this.apoch = document.getElementById('date_'+adh);
	//this.vpoch = document.getElementById('firstDate_'+adh);


/**/
	this.apochDB =gapochDB;// document.getElementById('id_epodate');//epoch time : full unix time stamp ;)
	this.apodDB  =gapodDB;// document.getElementById('id_epod');//without time
	this.apotDB  =gapotDB;// document.getElementById('id_epot');//Only time
	this.adateDB =gadateDB;// document.getElementById('id_newdate');
	this.atimeDB =gatimeDB;// document.getElementById('id_newtime');

	var dateVal ="/Date("+this.apoch.value+"000)/";
	var date = new Date( parseFloat( dateVal.substr(6 )));
	var dateIn = date.toISOString();//date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDay(); ,date.getDate()
	//console.log(date,dateIn,dateIn.substr(11, 5));//2019-01-12T01:47:33.000Z
	this.apochDB.value = this.apoch.value;console.log(this.apochDB.value);

//1547257653


	var hours = date.getHours();// Hours part from the timestamp
	var minutes = 1 * (date.getMinutes() >= 30 ? 1800 : 0);// Minutes part from the timestamp step of picker
	var seconds = 1 * date.getSeconds();// Seconds part from the timestamp
	//console.log('apotDB00000: '+hours+', '+minutes+', '+seconds+'... '+(hours * 60 * 60)+' '+(minutes * 1)+' ::: '+this.apotDB.value+' ');
	this.apotDB.value = (1 * hours * 60 * 60) + (minutes * 1)/* + seconds*/; console.log('apotDB:'+this.apotDB.value);
	this.apodDB.value = this.apoch.value - this.apotDB.value; console.log('apodDB:'+this.apodDB.value);

	this.adateDB.value = dateIn.substr(0, 10);//this.adate.innerText.replace('/','-');console.log(this.adateDB.value);
	this.atimeDB.value = dateIn.substr(11, 5);//date.getHours.toISOString() + ":" + date.getMinutes.toISOString();console.log(date.getHours() + ":" + date.getMinutes(),this.atimeDB.value);
	this.adateDB.setAttribute('data-value', adateDB.value.replace(/-/g,'/')/**/);
	this.atimeDB.setAttribute('data-value', atimeDB.value);

	pickd.set('select', this.apodDB.value * 1000);
	pickt.set('select', this.apotDB.value / 60);
	pickd.set('format', '<?php $plxPlugin->lang('L_FORMAT_SUBMIT') ?>');
	pickt.set('format', 'HH:i');

	this.set = function() {//BEP
		self.apoch.value = self.apochDB.value;
//		console.log('#set self.apochDB.innerText:'+self.apochDB.value);
//		console.log('#set self.apoch.value:'+self.apoch.value);
//		console.log('#set self.adate.innerText:'+self.adate.innerText);
		if(self.show){//on affiche la nvl date
			document.getElementById('date_text_'+self.adh).innerText = self.apoch.value;
			var dateValue ="/Date("+self.apoch.value+"000)/";
//~			 $.format.date(new Date(parseFloat( dateVal.substr(6 ))), 'yyyy/MM/dd HH:mm:ss');//stackoverflow.com/a/22594718
			self.adate.innerText = $.format.date(new Date(parseFloat( dateValue.substr(6 ))), '<?php $plxPlugin->lang('L_FORMAT_TO_TXT') ?>');//jquery !gh phstc/jquery-dateFormat
			self.adate.classList.add('edited');
//~			 self.adate.innerText = DateFormat.format(new Date(parseFloat( dateValue.substr(6 ))), '<?php $plxPlugin->lang('L_FORMAT_TO_TXT') ?>');// Vanilla TypeError: DateFormat.format is not a function
		}

		self.span.click();//close

		document.getElementById('checkbox_'+self.adh).checked = true;
		document.getElementById('id_selection'+self.vld).options[1].selected = 'selected';//update
	}//this.set

	this.addEvent = function (element, evnt, funct){
		if (element.attachEvent)
			return element.attachEvent('on'+evnt, funct);
		else
		return element.addEventListener(evnt, funct, false);
	}//this.addEvent

	this.addEvent(this.btn, 'click', this.set);
	this.btn.style.visibility = 'hidden';

	dialogueBox(dlg);//Open The time picker dialog
}
function remautocheck(id,adh){//1 autocheck + select update on click
	var frm = document.getElementById('form'+id);
	console.log('remautocheck form: ',frm);
	var tags = ['input','select'];
	for (var t = 0;t < tags.length;t++){
		var elements = frm.getElementsByTagName(tags[t]);
		for (var i = 0;i < elements.length;i++){
//			console.log('autocheck preg: ',elements[i].name.search(/_[0-9]/));
			var ade = elements[i].name.split('_');
//			if(elements[i].name.search(/_[0-9]/) >= 1){
			if(ade[1] == adh){
				console.log('remautocheck: '+elements[i].name);
				//this.addEvent(elements[i], 'input', this.chk);//change click
				elements[i].removeEventListener('input', this.chk, false);//change click
			}
		}
	}
}
function autocheck(){//autocheck + select update on click
	this.chk = function() {//TEP
		var adh = this.name.split('_');
		var adh = adh[1];//id number
		document.getElementById('checkbox_'+adh).checked = true;
		if(document.getElementById('validation_'+adh)){//		console.log('autocheck.chk: '+adh);
			var vld = document.getElementById('validation_'+adh).value;// 0 / 1
			document.getElementById('id_selection'+vld).options[1].selected = 'selected';//update
			document.getElementById('submit'+vld).classList.add('orange');
			remautocheck(vld,adh);
		}
	}
	this.addEvent = function (element, evnt, funct){
		if (element.attachEvent)//addEventListener is not supported in <= IE8
			return element.attachEvent('on'+evnt, funct);
		else
		return element.addEventListener(evnt, funct, false);
	}
	var tags = ['input','select'];
	for (var t = 0;t < tags.length;t++){
		var elements = document.getElementsByTagName(tags[t]);
		for (var i = 0;i < elements.length;i++){
//			console.log('autocheck preg: ',elements[i].name.search(/_[0-9]/));
			if(elements[i].name.search(/_[0-9]/) >= 1){
//				console.log('autocheck: '+elements[i].name);
				this.addEvent(elements[i], 'input', this.chk);//change click
			}
		}
	}
	var self = this;
}
autocheck();
</script>
<!-- <span id="liberapay" style="position:fixed;bottom:1em;right:1em;" title="<?php $plxPlugin->lang('L_LIBERAPAY');?>.">
<a href="https://liberapay.com/sudwebdesign/donate"><img alt="<?php $plxPlugin->lang('L_LIBERAPAY');?>." src="<?php echo PLX_PLUGINS.$pluginName.'/liberapay-fr.svg' ?>"></a>
</span> -->
<?php
//ENDOFADMIN:
