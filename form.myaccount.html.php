<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 * $this IS plxShow
 * @version	2.1.1
 * @date	15/04/2019
 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
 **/
if(
(!isset($_SESSION['lockArticles']['articles']) && !isset($_SESSION['lockArticles']['categorie']) && !isset($_SESSION['account']))
OR($_SESSION['lockArticles']['articles'] != 'on' && $_SESSION['lockArticles']['categorie'] != 'on' && $_SESSION['account'] != '')
) {//Si l'utilisateur n'est pas connecté, on le redirige vers l'accueil
	header('Location:'.$this->plxMotor->urlRewrite());
	exit;
}
$useCapcha = TRUE;
include('form.init.inc.php');#init plug & capcha
$plxPlugin->getAdherents('/^[0-9]{5}.(.[a-z-]+){2}.[0-9]{10}.xml$/');

if(!isset($_SESSION['account'])) {
	header('Location:'.$this->plxMotor->urlRewrite());
	exit();
}
$verif = substr($_SESSION['account'],5,-3);
$compte = array(NULL);
foreach ($plxPlugin->plxRecord_adherents->result as $key => $account) {
	if (md5($account['mail']) == $verif) {
		$compte = $plxPlugin->plxRecord_adherents->result[$key];
		break;
	}
}

if($compte == array(NULL)) {
	//On supprime les index de session
	unset($_SESSION['lockArticles']);
	unset($_SESSION['account']);
	unset($_SESSION['domainAd']);
	header('Location:'.$this->plxMotor->urlRewrite());
	exit;
}
//Définition de variables
$pro = array();
$error = array();
$success=false;
$wall_e = '';
if(!empty($_POST) && !empty($_POST['wall-e'])) {
	$wall_e = $_POST['wall-e'];
}
if(!empty($_POST) && isset($_POST['password']) && !empty($_POST['password']) && empty($wall_e)) {
	$compte['password'] = plxUtils::strCheck($_POST['password']);
	if (false === $plxPlugin->PW->isPasswordStrong($compte['password'], $plxPlugin->getPwLen()) ) {
		$error[] = $plxPlugin->getLang('L_PASSWORD_NOT_STRONG');
	}
	if(empty($error)) {
		if (isset($_SESSION['pw'])) {
			unset($_SESSION['pw']);
		}
		if ($plxPlugin->editMyAccount($compte,$compte['id'])) {//On édite le compte de l'adhérent
			$_SESSION['erase']['m'] = '<p id="password_success" class="success">'.$plxPlugin->getLang('L_EDIT_OK').'</p>';
			$_SESSION['erase']['d'] = 's';//display what
		} else {
			$error = array($plxPlugin->getLang('L_INTERNAL_ERR'));
		}
		header('Location:'.$this->plxMotor->path_url);//.$this->plxMotor->racine.$this->plxMotor->get
		exit;

	}
}
if(!empty($_POST) && !isset($_POST['password']) && empty($wall_e)) {

	$compte['nom']=strtoupper(trim(plxUtils::strCheck($_POST['nom'])));
	$compte['prenom']=strtolower(trim(plxUtils::strCheck($_POST['prenom'])));
	$compte['coordonnees']=plxUtils::strCheck($_POST['coordonnees']);
	if(trim($compte['coordonnees']) == '')
		$error[] = $plxPlugin->getLang('L_ERR_COORDONNEES');
	if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
		$compte['activite'] = plxUtils::strCheck($_POST['activite']);
		$compte['activite_autre']=trim(plxUtils::strCheck($_POST['activite_autre']));
		$compte['etablissement']=trim(plxUtils::strCheck($_POST['etablissement']));
		$compte['service']=trim(plxUtils::strCheck($_POST['service']));
		$compte['tel_office']=str_replace(array('.','-',' ','_','+','(',')',',',':',';','/'),'',plxUtils::strCheck($_POST['tel_office']));
		if($compte['activite'] =='')
			$error[] = $plxPlugin->getLang('L_ERR_ACTIVITE');
		if($compte['activite'] =='autre' && trim($compte['activite_autre']) == '')
			$error[] = $plxPlugin->getLang('L_ERR_AUTRE_ACTIVITE');
		if(trim($compte['etablissement']) == '')
			$error[] = $plxPlugin->getLang('L_ERR_ETABLISSEMENT');
		if(trim($compte['service']) == '')
			$error[] = $plxPlugin->getLang('L_ERR_SERVICE');
		if(trim($compte['tel_office']) != '' && !preg_match('!^[0-9]{3,13}[0-9]?$!',$compte['tel_office']))
			$error[] = $plxPlugin->getLang('L_ERR_TEL_OFFICE');
	}

	$compte['adresse1']=trim(plxUtils::strCheck($_POST['adresse1']));
	$compte['adresse2']=trim(plxUtils::strCheck($_POST['adresse2']));
	$compte['cp']=$_POST['cp'];
	$compte['ville']=trim(plxUtils::strCheck($_POST['ville']));
	$compte['tel']=str_replace(array('.','-',' ','_','+','(',')',',',':',';','/'),'',$_POST['tel']);
	$compte['mail']=trim(str_replace('&#64;', '@', plxUtils::strCheck($_POST['mail'])));
	$compte['choix']=empty($_POST['choix']) ? 'adhesion': plxUtils::strCheck($_POST['choix']);
	$compte['mailing']=plxUtils::strCheck($_POST['mailing']);
	if(trim($compte['nom'])=='')
		$error[] = $plxPlugin->getLang('L_ERR_NAME');
	if(trim($compte['prenom'])=='')
		$error[] = $plxPlugin->getLang('L_ERR_FIRST_NAME');
	if(trim($compte['adresse1']) == '')
		$error[] = $plxPlugin->getLang('L_ERR_ADRESSE');
	if(trim($compte['cp']) == '' || strlen($compte['cp']) != 5 || !is_numeric($compte['cp']))
		$error[] = $plxPlugin->getLang('L_ERR_CP');
	if(trim($compte['ville']) == '')
		$error[] = $plxPlugin->getLang('L_ERR_VILLE');
	if(trim($compte['tel']) == '' || !preg_match('!^[0-9]{9,13}[0-9]$!',$compte['tel']))
		$error[] = $plxPlugin->getLang('L_ERR_TEL');
	if(trim($compte['mailing']) == '')
		$error[] = $plxPlugin->getLang('L_ERR_MAILING');
	if(!plxUtils::checkMail($compte['mail']))
		$error[] = $plxPlugin->getLang('L_ERR_MAIL');
	if (isset($this->plxMotor->plxPlugins->aPlugins['plxCapchaImage']))//capchaImage (module ok)
		$_SESSION['capcha']=sha1(@$_SESSION['capcha']);
	if($this->plxMotor->aConf['capcha'] AND $_SESSION['capcha'] != sha1($_POST['rep']))
		$error[] = $plxPlugin->getLang('L_ERR_ANTISPAM');
	if(empty($error)) {
		if ($plxPlugin->editMyAccount($compte,$compte['id'])) {//On édite le compte de l'adhérent
			if($compte['choix'] == 'stop') {//Si l'utilisateur ne souhaite plus être membre de l'asso, on envoie une notification à un admin
				$content = $plxPlugin->notification($compte['nom'],$compte['prenom'],$compte['adresse1'],$compte['adresse2'],$compte['cp'],$compte['ville'],$compte['tel'],$compte['mail'],$compte['choix'],$compte['mailing']);
				if($plxPlugin->sendEmail($plxPlugin->getParam('nom_asso'),$plxPlugin->getParam('email'),$plxPlugin->getParam('email'),$plxPlugin->getParam('devalidation_subject'),$content,'html')){
						$_SESSION['erase']['m'] = '<p id="password_success">'.$plxPlugin->getLang('L_EDIT_OK').'<br/>'.$plxPlugin->getLang('L_FORM_ERASE_OK').'</p>';
						$_SESSION['erase']['d'] = 's';//display what
						unset($_SESSION['account']);
						unset($_SESSION['isConnected']);
						unset($_SESSION['lockArticles']);
						header('Location:'.$this->plxMotor->urlRewrite());
						exit;
				}
			}
			$_SESSION['erase']['m'] = '<p id="password_success" class="success">'.$plxPlugin->getLang('L_EDIT_OK').'</p>';
			$_SESSION['erase']['d'] = 's';//display what
		} else {
			$error = array($plxPlugin->getLang('L_INTERNAL_ERR'));
		}
		header('Location:'.$this->plxMotor->path_url);//.$this->plxMotor->racine.$this->plxMotor->get
		exit;

	}
}
?>
<p id="all_required"><?php echo sprintf($plxPlugin->getLang('L_FORM_ALL_REQUIRED'),'<exp class="mandatory">*</exp>');?></p>
<div id="form_adherer">
<?php if(!empty($error)): ?>
		<div class="contact_error">
<?php if(!empty($error)): ?>
				<h3><?php $plxPlugin->lang('L_FORM_FIELDS_MISSING') ?></h3>
				<ul>
<?php foreach ($error as $e) {
					echo PHP_EOL.'						<li>'.$e.'</li>';
				}
?>
				</ul>
<?php endif; ?>
		</div>
	<?php endif;
	unset($_POST);?>
	<form action="#form" method="post">
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_SECURITY');?></h2></legend>
		<p>
			<label for="password"><?php $plxPlugin->lang('L_EDIT_PASSWORD') ?>&nbsp;:
				<input id="password" name="password" type="password" size="30" pattern=".{12,}" value="" maxlength="90" required /><exp class="mandatory">*</exp>
				<br/>
				<small><?php echo sprintf($plxPlugin->getLang('L_SECURITY_RULES'),$plxPlugin->getPwLen()) ?></small>
			</label>
		</p>
		<p class="wall-e">
			<label for="walle2"><?php $plxPlugin->lang('L_FORM_WALLE') ?>
				<input id="walle2" name="wall-e" type="text" size="50" value="<?php echo plxUtils::strCheck($wall_e) ?>" maxlength="50" />
			</label>
		</p>

		<p class="text-right">
			<input type="submit" name="submit" value="<?php $plxPlugin->lang('L_FORM_BTN_SEND') ?>" />
		</p>
		</fieldset>
	</form>
	<form action="#form" method="post" name="monadhesion">
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_IDENTITY');?></h2></legend>
		<p>
			<label for="name"><?php $plxPlugin->lang('L_FORM_NAME') ?>&nbsp;:
				<input id="name" name="nom" type="text" size="30" pattern="[^0-9]+" value="<?php echo plxUtils::strCheck($compte['nom']) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
			</label>
		</p>
		<p>
			<label for="firstname"><?php $plxPlugin->lang('L_FORM_FIRST_NAME') ?>&nbsp;:
				<input id="firstname" name="prenom" type="text" size="30" pattern="[^0-9]+" value="<?php echo plxUtils::strCheck($compte['prenom']) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
			</label>
		</p>
		</fieldset>
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') :
 $aA = explode(',',$plxPlugin->getParam('tabActivites'));
 $aK = array_map('strtolower', $aA);
 $aActivites = array_combine($aK, $aA);
 $activite = strtolower($compte['activite']);
 $tot=count($aActivites);
 $ia=0;
?>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_ACTIVITY');?></h2></legend>
<?php foreach($aActivites AS $iActi => $acti):
		echo ++$ia==$tot?'<br/>':'';
		if (!array_key_exists($activite,$aActivites)) {
			$activite = 'autre';
			$activite_autre = isset($compte['activite_autre'])?$compte['activite_autre']:$compte['activite'];
		} else {
			$activite_autre = '';
		}
		$compte['activite'] = isset($compte['activite_autre'])?$compte['activite_autre']:$compte['activite'];//if posted
?>
		<p class="act">
			<input id="<?php echo $iActi ?>" name="activite" type="radio" value="<?php echo $iActi ?>" <?php echo plxUtils::strCheck($activite) == $iActi? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="<?php echo $iActi ?>"><?php echo $acti ?></label>
		</p>
<?php endforeach;?>
		<p>
			<label class="mask" for="activite_autre"><?php echo $plxPlugin->lang('L_FORM_DETAIL');?>&nbsp;:
				<input class="mask" id="activite_autre" name="activite_autre" type="text" value="<?php echo plxUtils::strCheck($activite) == 'autre' ? plxUtils::strCheck($compte['activite']) : ''; ?>" />
			</label>
		</p>
		</fieldset>
<?php endif; ?>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_AGENDA');?></h2></legend>
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') : ?>
		<p>
			<label for="etablissement"><?php $plxPlugin->lang('L_FORM_SOCIETY') ?>&nbsp;:
				<input id="etablissement" name="etablissement" type="text" size="50" value="<?php echo plxUtils::strCheck($compte['etablissement']) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
			</label>
		</p>
		<p>
			<label for="service"><?php $plxPlugin->lang('L_FORM_SERVICE') ?>&nbsp;:
				<input id="service" name="service" type="text" size="50" value="<?php echo plxUtils::strCheck($compte['service']) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
			</label>
		</p>
<?php endif; ?>
		<p>
			<label for="adresse1"><?php $plxPlugin->lang('L_FORM_ADDRESS') ?>&nbsp;:
				<input class="xl" id="adresse1" name="adresse1" type="text" size="50" value="<?php echo plxUtils::strCheck($compte['adresse1']) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
			</label>
			<input class="xl" id="adresse2" name="adresse2" type="text" size="50" value="<?php echo plxUtils::strCheck($compte['adresse2']) ?>" maxlength="50" />
		</p>
		<p>
			<label for="cp"><?php $plxPlugin->lang('L_FORM_ZIP_CODE') ?>&nbsp;:
				<input id="cp" name="cp" type="text" size="5" value="<?php echo plxUtils::strCheck($compte['cp']) ?>" maxlength="5" required /><exp class="mandatory">*</exp>
			</label>
		</p>
		<p>
			<label for="ville"><?php $plxPlugin->lang('L_FORM_CITY') ?>&nbsp;:
				<input id="ville" name="ville" type="text" size="50" value="<?php echo plxUtils::strCheck($compte['ville']) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
			</label>
		</p>
		<p>
			<label for="tel"><?php $plxPlugin->lang('L_FORM_TEL') ?>&nbsp;:
				<input id="tel" name="tel" type="text" size="50" value="<?php echo plxUtils::strCheck($compte['tel']) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
			</label>
		</p>
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') : ?>
		<p>
			<label for="tel_office"><?php $plxPlugin->lang('L_FORM_TEL_OFFICE') ?>&nbsp;:
				<input id="tel_office" name="tel_office" type="text" size="50" value="<?php echo plxUtils::strCheck($compte['tel_office']) ?>" maxlength="50" />
			</label>
		</p>
<?php endif; ?>
		<p>
			<label for="courriel"><?php $plxPlugin->lang('L_FORM_MAIL') ?>&nbsp;:
				<input id="courriel" name="mail" type="email" size="50" value="<?php echo ($compte['mail'] != '')? str_replace('@','&#64;',$compte['mail']):''; ?>" required /><exp class="mandatory">*</exp>
			</label>
		</p>
		</fieldset>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_CHOICE');?></h2></legend>
<?php if($plxPlugin->getParam('annee') != 'illimite'): ?>
		<p>
			<input id="renouveler" name="choix" type="radio" value="renouveler" <?php echo plxUtils::strCheck($compte['choix']) != 'stop'? 'checked="checked"' : ''; ?> />
			<label for="renouveler"><?php $plxPlugin->lang('L_FORM_RULES_RE') ?> <?php echo $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
<?php endif;#$plxPlugin->getParam('annee') != 'illimite') ?>
		<p>
			<input id="stop" name="choix" type="radio" value="stop" <?php echo plxUtils::strCheck($compte['choix']) == 'stop'? 'checked="checked"' : ''; ?> />
			<label for="stop"><?php $plxPlugin->lang('L_FORM_RULES_NO') ?> <?php echo $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
<?php if($plxPlugin->getParam('showAnnuaire') == 'on') : ?>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_SHARING');?></h2></legend>
<?php if($plxPlugin->getParam('publicAnnuaire') == 'on') : ?>
		<p>
			<input id="public" name="coordonnees" type="radio" value="public" <?php echo plxUtils::strCheck($compte['coordonnees']) == 'public' ? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="public"><?php $plxPlugin->lang('L_FORM_RULES_SHARE_PUBLIC') ?> <?php echo $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
<?php endif;#($plxPlugin->getParam('publicAnnuaire') == 'on') ?>
		<p>
			<input id="rec" name="coordonnees" type="radio" value="rec" <?php echo plxUtils::strCheck($compte['coordonnees']) == 'rec' ? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="rec"><?php $plxPlugin->lang('L_FORM_RULES_SHARE_MEMBER') ?> <?php echo $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		<p>
			<input id="refus" name="coordonnees" type="radio" value="refus" <?php echo plxUtils::strCheck($compte['coordonnees']) == 'refus' ? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="refus"><?php $plxPlugin->lang('L_FORM_RULES_SHARE_NO') ?> <?php echo $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		</fieldset>
<?php else:#fi ($plxPlugin->getParam('showAnnuaire') == 'on') ?>
			<input id="annuoff" name="coordonnees" type="hidden" value="<?php echo isset($compte['coordonnees'])?plxUtils::strCheck($compte['coordonnees']):'refus'; ?>" />
<?php endif; ?>

		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_MAILING');?></h2></legend>
		<p>
			<input id="maillist" name="mailing" type="radio" value="maillist" <?php echo plxUtils::strCheck($compte['mailing']) == 'maillist' ? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="maillist"><?php $plxPlugin->lang('L_FORM_RULES_NEWS') ?> <?php echo $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		<p>
			<input id="blacklist" name="mailing" type="radio" value="blacklist" <?php echo plxUtils::strCheck($compte['mailing']) == 'blacklist' ? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="blacklist"><?php $plxPlugin->lang('L_FORM_RULES_NEWS_NO') ?></label>
		</p>
		<p class="wall-e">
			<label for="walle"><?php $plxPlugin->lang('L_FORM_WALLE') ?></label>
			<input id="walle" name="wall-e" type="text" size="50" value="<?php echo plxUtils::strCheck($wall_e) ?>" maxlength="50" />
		</p>
		</fieldset>

<?php eval($this->plxMotor->plxPlugins->callHook('myAccountForm'.get_class($plxPlugin)));# Hook Plugins ?>

		<fieldset>

<?php if($plxPlugin->getParam('annee') != 'illimite') echo '		<p class="cotisation info">'.$plxPlugin->getLang('L_COTIS'). ' ' .$plxPlugin->cotisationAJour($compte['date']).'</p>'; ?>

<?php if($this->plxMotor->aConf['capcha'])://$this->lang('ANTISPAM_WARNING')?>
			<p>
				<label for="id_rep"><strong><?php $plxPlugin->lang('L_FORM_ANTISPAM') ?>&nbsp;:</strong></label>
			</p>
			<?php $this->capchaQ(); ?>
			<input id="id_rep" name="rep" type="text" size="2" maxlength="1" autocomplete="off" style="width: auto; display: inline;" required /><exp class="mandatory">*</exp>
<?php endif; ?>
		<p class="text-right">
			<input type="submit" name="submit" value="<?php $plxPlugin->lang('L_FORM_BTN_SEND') ?>" />
		</p>
		</fieldset>
	</form>
	<script type="text/javascript">
		/* <![CDATA[ */
		document.getElementById('stop').onclick = function() {
			if(this.checked);
				if (!confirm("<?php echo $plxPlugin->getLang('L_FORM_CONFIRM'); ?>"))
					this.checked = false;
		}
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') :// ici on cache ou affiche l'input autre :insp: http://stackoverflow.com/a/8997289 ?>
		var rad = document.monadhesion.activite;
		var othr = document.getElementsByClassName('mask');
		for(var i = 0; i < rad.length; i++) {
			rad[i].onclick = function() {
				if(this.value == 'autre'){//on affiche
					for(var m = 0;m < othr.length; m++)
						othr[m].style.display='';
					document.getElementById('activite_autre').required = true;
					document.getElementById('activite_autre').focus();
				}else{//on cache
					for(var m = 0;m < othr.length; m++)
						othr[m].style.display='none';
					document.getElementById('activite_autre').required = false;
					document.getElementById('activite_autre').value = '';
				}
			};
			var NoOth = (rad[i].value == 'autre' && rad[i].checked)?false:true;
		}
		if(NoOth)
			for(var m = 0;m < othr.length; m++)
				othr[m].style.display='none';
<?php endif; ?>
		/* ]]> */
	</script>
</div>