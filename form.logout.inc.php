<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 * $this IS adhesion : used with finclude : Members Space
 * @version	2.0.0
 * @date	20/02/2019
 * @author	Thomas Ingles
 **/
if(!$this->form_login_adherent)
	$this->form_login_adherent = $this->plxMotor->mode;
else if (!isset($_SESSION['domainAd']))
	return;//Only one form!
$pluginName = get_class($this);//adhesion
//$ad = $this->adherentsList[$id];
if ($this->plxRecord_adherents != null) {
	$compte = $this->plxRecord_adherents->result[$this->adherentsList[$_SESSION['adhesion']]];
} else {
	$compte = array('date' => null);
}
?>
<div class="<?php echo $this->plxMotor->mode.($class?' '.$class:''); ?>" id="espace-adherents">
	<h3 class="widget-title icon-parents"><?php echo $this->getParam('mnuMembers');#echo '#test ::: mode: '. $this->plxMotor->mode.', get: '. $this->plxMotor->get.', cible: '. $this->plxMotor->cible.', path_url: '.$this->plxMotor->path_url.', form_login_adherent.: '.$this->form_login_adherent.', time: '.time() ?></h3>
<?php eval($this->plxMotor->plxPlugins->callHook('logout'.$pluginName)); # Hook plugins ?>
	<ul class="<?php echo $pluginName ?>-list unstyled-list">
		<li>
			<form action="" method="post" id="logout">
				<fieldset>
					<input type="hidden" name="logout">
					<input type="submit" value="<?php echo $this->getParam('mnuDeconnexion') ?>" id="logout-sub"/>
				</fieldset>
<?php eval($this->plxMotor->plxPlugins->callHook('logoutForm'.$pluginName)); # Hook plugins ?>
			</form>
		</li>
<?php if($this->getParam('showAnnuaire') != 'no') : $annuName = $this->getParam('mnuAnnuaire'); ?>
		<li id="annuaire"><a href="<?php echo $this->plxMotor->urlRewrite('?annuaire.html') ?>" title="<?php echo $annuName ?>"><?php echo $annuName ?></a></li>
<?php endif; ?>
<?php eval($this->plxMotor->plxPlugins->callHook('logoutList'.$pluginName)); # Hook plugins ?>
		<li id="myaccount"><a href="<?php echo $this->plxMotor->urlRewrite('?myaccount.html');?>"><?php $this->lang('L_MY_ACCOUNT')?></a></li>
	</ul>
<?php eval($this->plxMotor->plxPlugins->callHook('logoutEnd'.$pluginName)); # Hook plugins ?>
<?php if($this->plxMotor->mode == 'login-page' AND $this->getParam('annee') != 'illimite') echo '<p class="cotisation">'.$this->getLang('L_COTIS'). ' ' .$this->cotisationAJour($compte['date']).'</p>'; ?>
</div>