jQuery(function($) {
	try{//pluxml 5.4 +
		var title_obj = document.getElementsByClassName('inline-form')[0].firstChild.nextSibling;//pluxml 5.4+
	}catch(e){var title_obj = document.getElementById('title_config'); console.log(e);}//pluxml 5.4-
	var title_pge = title_obj.innerHTML;

	//$('textarea.expanding').autogrow({minHeight : 100});

	//Décommenter les 2 lignes en dessous pour utiliser le système d'onglets de jQuery Tools
	//$("div.panes > div.contenu").css({'display' : 'none'});
	//$("ul.css-tabs").tabs("div.panes > div.contenu", {history: true});

	//Gestion des onglets d'après http://www.grafikart.com
	var anchor = window.location.hash;
	$('.css-tabs').each(function(){
		var current = null;
		var id = $(this).attr('id');
		//Si l'on veut conserver l'historique d'ouverture des onglets
		//il faut ajouter la classe history au menu
		if ($(this).hasClass('history') === true) {
			var history = true;
		}
		if (anchor != '' && $(this).find('a[href="'+anchor+'"]').length > 0 ) {
			current = anchor;
		} else if($.cookie('tab'+id) && $(this).find('a[href="'+$.cookie('tab'+id)+'"]').length > 0 ){
			current = $.cookie('tab'+id);
		} else {
			current = $(this).find('a:nth-child(2)').attr('href');//a:first : choose number of default opened tab
		}
		$(this).find('a[href="'+current+'"]').addClass('current');
		//le titre change
		title_obj.innerHTML = title_pge + ' : ' + document.getElementById('title_' + current.toString().substr(1)).innerHTML;
		$(current).siblings().hide();
		$(this).find('a').click(function(){
			var link = $(this).attr('href');
			if (link != current) {
				$('a.current').removeClass('current');
				$(this).addClass('current');
				$(link).show().siblings().hide();
				current = link;
				if (history) {
					$.cookie('tab'+id,current);
				}
				//le titre change
				title_obj.innerHTML = title_pge + ' : ' + document.getElementById('title_' + current.toString().substr(1)).innerHTML;
			}
			if ($(this).attr('href').match(new RegExp('\#')) !== null){
				return false;
			}
		});
	});
});