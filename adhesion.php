<?php if(!defined('PLX_ROOT')) exit;

include __DIR__.DIRECTORY_SEPARATOR.'class.PasswordTools.php';

/**
 * PluXml Plugin adhesion
 * #todo ds menu + 1 lien ds adherer.html (ou se connecter)
 * @version	2.2.0
 * @date	21/04/2019
 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
 **/
class adhesion extends plxPlugin {
	public $plxMotor;
	public $v = '2.2.0';
	public $plxGlob_adherents;#Obj des données concernant les fichiers adhérents
	public $plxRecord_adherents;#Obj des données concernant les adhérents
	public $form_login_adherent;#Str mode du moteur & un formulaire seulement : form.login.inc.php

	public $oldAdherentsList = array();#Tableau des données des adhérents, extraites de la liste générée par l'ancienne version du plugin
	public $adherentsList = array();#Tableau des index des adhérents
	public $listsDiff = array();#Tableau des listes de diffusion
	public $listDiff = array();#La liste de diffusion des adhérents
	public $msg = FALSE;
	public $ok = FALSE;
#Paramètres des listes de diffusion Gutuma
	public $isGutumaActivated=false;
	public $GutumaListsDir;
	public $PW;
	private $id;
	private $name;
	private $private;
	private $addresses;
	private $size;
#Paramètres de connexion
	private $ban = array();
	private $session_domain;
	private $pwLen = 12;
	private $tokenLenght = 64;

	/**
	 * Constructeur de la classe
	 *
	 * @param	default_lang	langue par défaut
	 * @return	stdio
	 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
	 **/
	public function __construct($default_lang) {

		$this->PW = new PasswordTools();

		# appel du constructeur de la classe plxPlugin (obligatoire)
		parent::__construct($default_lang);

		# droits pour accèder à la page config.php du plugin
		$this->setConfigProfil(PROFIL_ADMIN);
		$this->setAdminProfil(PROFIL_ADMIN, PROFIL_MANAGER);

#ADMIN_MENU_TITLE + utilisé coté public avec plxAdminBar
		$this->setAdminMenu($this->getLang('L_ADMIN_MENU_NAME'), 1, $this->getLang('L_ADMIN_TITLE_MENU'));#Position du Menu : remplacer 0 par tout autre chiffre

		$ia = (defined('PLX_ADMIN'));#is admin ::: Déclaration des hooks pour la zone d'administration

		# déclaration des hooks
		$this->addHook('plxMotorConstruct', 'plxMotorConstruct');#Méthode qui Récupére les données des adhérents : getAdherents()
		if($ia) {#admin
			$this->addHook('AdminPrepend', 'AdminPrepend');#Permet l'export de la liste des adhérents (opentbs)
			$this->addHook('AdminTopEndHead', 'AdminTopEndHead');#Affiche le js (jquery) et css du plugin dans la partie administration
			$this->addHook('AdminTopBottom', 'AdminTopBottom');#Affiche un message s'il y a un message à afficher
			$this->addHook('AdminAdhesionUsersFoot', 'AdminAdhesionUsersFoot');#hook admin.php Affiche le javascript dataTable
#			$this->addHook('AdminAdhesionUsersTop', 'AdminAdhesionUsersTop');#hook admin.php : OLD search : désactivé (dataTableIncHead() have searchs)
#			$this->addHook('AdminAdhesionUsersTopValid', 'AdminAdhesionUsersTopValid');#hook admin.php : OLD search : désactivé
		}
		if(plxUtils::checkMail($this->getParam('email'))) {
			$this->addHook('plxMotorConstructLoadPlugins', 'plxMotorConstructLoadPlugins');#plxGlob_adherents qui mémorise le contenu du dossier adherents
			$this->addHook('plxMotorPreChauffageBegin', 'plxMotorPreChauffageBegin');#Met à jour la liste des adhérents de leur inscription, dès qu'une page publique est affichée & Page statique du formulaire d'adhésion
			$this->addHook('plxShowConstruct', 'plxShowConstruct');#Charge les infos des page statiques : $this->plxMotor->mode
			$this->addHook('plxShowStaticListEnd', 'plxShowStaticListEnd');#Gère l'affichage du menu des pages statiques du plugin
			$this->addHook('plxShowPageTitle', 'plxShowPageTitle');#Méthode qui renseigne le titre de la page dans la balise html <title> ONLY mode == 'adhesion' * Todo : adherer & annuaire(si:public)
			$this->addHook('ThemeEndHead', 'ThemeEndHead');#Ajoute le fichier css dans le fichier header.php du thème
			$this->addHook('ThemeEndBody', 'ThemeEndBody');#Affiche des messages succes/erreur
			$this->addHook('SitemapStatics', 'SitemapStatics');#Référence la page d'adhesion dans le sitemap * Todo : adherer & annuaire(si:public)
## déclaration des hooks pour sécuriser les articles
			if($ia) {#admin
#index
				$this->addHook('AdminIndexTop', 'AdminIndexTop');#Enclanche la bufferisation de sortie pour afficher les cadenas (voir AdminIndexFoot())
				$this->addHook('AdminIndexFoot', 'AdminIndexFoot');#Affiche le cadenas au niveau de la page articles de l'administration si un article a un mot de passe
#article
				$this->addHook('AdminArticlePostData', 'AdminArticlePostData');#Ajoute le champ 'mot de passe' dans l'édition de l'article (Nouvelle cat article.php)
				$this->addHook('AdminArticleParseData', 'AdminArticleParseData');#Affiche l'icone si l'article est privé (cat ou lui même)
				$this->addHook('AdminArticleInitData', 'AdminArticleInitData');#Affiche l'icone si l'article est privé (cat ou lui même)
				$this->addHook('AdminArticleTop', 'AdminArticleTop');#Demarre la bufferisation de sortie sur la page admin/article.php
				$this->addHook('AdminArticleContent', 'AdminArticleContent');#Affiche l'icone si l'article est privé (cat ou lui même)
				$this->addHook('AdminArticleFoot', 'AdminArticleFoot');#Affiche une icone aux catégories dédiées aux adhérents (sidebar)
				$this->addHook('AdminArticleSidebar','AdminArticleSidebar');#Ajoute le champ 'mot de passe' dans l'édition de l'article
#plxAdmin
				$this->addHook('plxAdminEditArticleXml','plxAdminEditArticleXml');#Ajoute le champ 'mot de passe' dans l'édition de l'article
				$this->addHook('plxAdminEditCategoriesUpdate','plxAdminEditCategoriesUpdate');#Ajoute le champs 'mot de passe' dans les options des catégories
				$this->addHook('plxAdminEditCategoriesNew','plxAdminEditCategoriesNew');#Ajoute le champs 'mot de passe' dans les options des catégories, dans article.php
				$this->addHook('plxAdminEditCategoriesXml','plxAdminEditCategoriesXml');#Ajoute le champ 'mot de passe' dans l'édition de l'article
				$this->addHook('plxAdminEditCategorie','plxAdminEditCategorie');#Ajoute le champs 'mot de passe' dans les options des catégories
#cats
				$this->addHook('AdminCategory','AdminCategory');#Ajoute le champs 'mot de passe' dans les options des catégories
				$this->addHook('AdminCategoriesTop','AdminCategoriesTop');#Permet de démarrer la bufferisation de sortie sur la page admin/categories.php
				$this->addHook('AdminCategoriesFoot','AdminCategoriesFoot');#Affiche l'image du cadenas si la page est protégée par un mot de passe
			}
#Moteur
			$this->addHook('plxMotorDemarrageBegin', 'plxMotorDemarrageBegin');#Permet la (dé)conexion en empechant de poster un commentaire (en mode article)
			$this->addHook('plxMotorPreChauffageEnd', 'plxMotorPreChauffageEnd');#Méthode qui redefinit les modes de plxMotor : art,homes,cat,tag&arch
			$this->addHook('plxMotorDemarrageEnd', 'plxMotorDemarrageEnd');#Affiche le formulaire de saisie du mot de passe
			$this->addHook('plxMotorParseArticle','plxMotorParseArticle');#Ajoute le champ 'mot de passe' dans l'édition de l'article
			$this->addHook('plxMotorGetCategories','plxMotorGetCategories');#Affiche le champs 'mot de passe' dans les options des catégories
#feed
			$this->addHook('plxFeedPreChauffageEnd','plxFeedPreChauffageEnd');#Permet de masquer s'il y a un mot de passe
			$this->addHook('plxFeedDemarrageBegin','plxFeedDemarrageBegin');#Permet d'afficher un article dans le flux RSS s'il n'est pas protégé par un mot de passe
#plug
#			$this->addHook('showIconIfLock','showIconIfLock');#UTILISÉ ? OUI
			$this->addHook('loginLogout','loginLogout');#Hook qui affiche le formulaire de cnx ou l'espace adhérent

## déclaration des hooks pour sécuriser les pages statiques
			$this->addHook('plxMotorGetStatiques', 'plxMotorGetStatiques');#Récupère la notification de mot de passe stockée dans le fichier xml statiques.xml
			if($ia) {#admin
				$this->addHook('AdminStatic', 'AdminStatic');#Ajoute le champ de saisie du mot de passe dans la page d'édition de la page statique
				$this->addHook('plxAdminEditStatique', 'plxAdminEditStatique');#Récupère la notification de mot de passe saisit lors de l'édition de la page statique
				$this->addHook('plxAdminEditStatiquesXml', 'plxAdminEditStatiquesXml');#Ajoute la notification de mot de passe dans la chaine xml à sauvegarder dans statiques.xml
				$this->addHook('AdminStaticsTop', 'AdminStaticsTop');#Permet de démarrer la bufferisation de sortie sur la page admin/statiques.php
				$this->addHook('AdminStaticsFoot', 'AdminStaticsFoot');#Affiche l'image du cadenas si la page est protégée par un mot de passe
			}

			$this->addHook('plxShowConstruct', 'plxShowConstructStat');#Protege les pages statiques si mode static_password : si page protégée url = static_password
			$this->addHook('plxMotorPreChauffageEnd', 'plxMotorPreChauffageEndStat');#Affiche le formulaire d'identification si un mot de passe est présent pour la page statique
			$this->addHook('plxMotorDemarrageEnd', 'plxMotorDemarrageEndStat');#Valide la connexion d'un adhérent
			$this->addHook('plxShowPageTitle', 'plxShowPageTitleStat');#Renseigne le titre de la page dans la balise html <title>
			$this->addHook('ThemeEndHead', 'ThemeEndHeadStat');#Ajoute le fichier style-lock.css dans le fichier header.php du thème
		}

		# On récupère l'ensemble des adhérents
		$this->plxGlob_adherents = plxGlob::getInstance(PLX_ROOT.$this->getParam('adherents').'adhesions',false,true,'arts');
		ksort($this->plxGlob_adherents->aFiles);
		$this->adherentsList = array_flip(array_keys($this->plxGlob_adherents->aFiles));
	}

	public function getPwLen()
	{
		return $this->pwLen;
	}

	//On neutralise les méthodes magiques
	public function __get($value='')
	{
		return true;
	}

	public function __set($var, $value='')
	{
		return true;
	}

	public function gutumaPlugin() {

		$this->isGutumaActivated = isset($this->plxMotor->plxPlugins->aPlugins['gutuma'])?$this->plxMotor->plxPlugins->aPlugins['gutuma']:null;
		if(is_object($this->isGutumaActivated)) {
			$this->isGutumaActivated = true;
			$htaccess = "Allow from none\n";
			$htaccess .= "Deny from all\n";
			$htaccess .= "<Files *.php>\n";
			$htaccess .= "order allow,deny\n";
			$htaccess .= "deny from all\n";
			$htaccess .= "</Files>\n";
			$htaccess .= "Options -Indexes\n";
			# Emplacement des listes de diffusion de Gutuma
			if ($this->plxMotor->plxPlugins->aPlugins['gutuma']->listsDir != null)
				$this->GutumaListsDir = $this->plxMotor->plxPlugins->aPlugins['gutuma']->listsDir;
			else
				$this->GutumaListsDir = PLX_ROOT.'data/gutuma';
			# Récupération des listes des anciennes versions de Gutuma
			if (is_dir(PLX_PLUGINS.'gutuma/news/lists')) {
				@rename(PLX_PLUGINS.'gutuma/news/lists', $this->GutumaListsDir);
				touch($this->GutumaListsDir.'/.htaccess');
				file_put_contents($this->GutumaListsDir.'/.htaccess', $htaccess);
			}
			# Récupération de la config des anciennes versions de Gutuma
			if (is_file(PLX_PLUGINS.'gutuma/news/inc/config.php')) {
				@mkdir($this->GutumaListsDir.'/inc');
				@rename(PLX_PLUGINS.'gutuma/news/inc/config.php', $this->GutumaListsDir.'/inc/config.php');
				touch($this->GutumaListsDir.'/inc/.htaccess');
				file_put_contents($this->GutumaListsDir.'/inc/.htaccess', $htaccess);
			}
			# On récupère les paramètres de la liste de diffusion
			$list = $this->getAllGutumaLists(TRUE);
			$k = 0;
			foreach ($list as $key => $value) {
				if ($value['name'] == 'adherents') {
					$k = $key;
				}
			}
			if (isset($list[$k])) {
				$this->id = $list[$k]['id'];
				$this->name = $list[$k]['name'];
				$this->private = $list[$k]['private'];
				$this->addresses = $list[$k]['addresses'];
				$this->listDiff = $list[$k]['addresses'];
			}

			if ($this->id != ''){
				$this->ok = TRUE;
			}

		}
	}

	///////////////////////////////////////////////////////////
	//
	// Méthodes permettant la mise en place du plugin
	//
	//////////////////////////////////////////////////////////

	/**
	 * plxGlob_adherents qui mémorise le contenu du dossier adherents
	 * @param	dir				repertoire à lire
	 * @param	rep				boolean pour ne prendre que les répertoires sans les fichiers
	 * @param	onlyfilename	boolean pour ne récupérer que le nom des fichiers sans le chemin
	 * @param	type			type de fichier lus (arts ou '')
	 * SI ($type=='arts') $index = str_replace('_','',substr($file, 0,strpos($file,'.'))); [initCache($type='')]
	 **/
	public function plxMotorConstructLoadPlugins() {
		echo '<?php ';?>
		$this->plxGlob_adherents = plxGlob::getInstance(PLX_ROOT.$this->plxPlugins->aPlugins['<?php echo __CLASS__?>']->getParam('adherents').'adhesions',false,true,'arts');
		$this->adherentsList = array_flip(array_keys($this->plxGlob_adherents->aFiles));
		# Hook plugins
		eval($this->plxPlugins->callHook('plxMotorConstructLoadPlugins<?php echo __CLASS__?>'));
?><?php
	}

	/**
	 * Méthode qui Récupére les données des adhérents : getAdherents()
	 *
	 * @return	stdio
	 * @author	Cyril MAGUIRE, Thomas Ingles
	 **/
	public function plxMotorConstruct() {
		echo '<?php ';?>
		$this->plxPlugins->aPlugins['<?php echo __CLASS__?>']->getAdherents('/^[0-9]{5}.(.[a-z-]+){2}.[0-9]{10}.xml$/');
		$this->plxPlugins->aPlugins['<?php echo __CLASS__?>']->timezone = $this->aConf['timezone'];
		# Hook plugins
		eval($this->plxPlugins->callHook('plxMotorConstruct<?php echo __CLASS__?>'));
?><?php
	}

	/**
	 * Méthode qui place le htaccess dans le dossier de config
	 *
	 * @return	stdio
	 * @author	Thomas Ingles
	 **/
	public function onUpdate() {
		if (is_dir(PLX_ROOT.$this->getParam('adherents').'adhesions')) {
			$file = PLX_ROOT.PLX_CONFIG_PATH.'.htaccess';
			$newfile = PLX_ROOT.$this->getParam('adherents').'/adhesions/.htaccess';
			if (!file_exists($newfile))
				if (!copy($file, $newfile)) @$_SESSION['error'] .= sprintf($this->getLang('L_ERROR_FILE_COPY'),$file,$newfile).PHP_EOL;
		}
		$upParms = false;
		if (!$this->getParam('mnuMembers')) {#2.1.1
			$upParms = true;
			$this->setParam('mnuMembers',$this->getLang('L_DEFAULT_MENU_MEMBERS'),'cdata');
			$this->setParam('mnuConnexion',$this->getLang('L_DEFAULT_MENU_CONNEXION'),'cdata');
			$this->setParam('mnuDeconnexion',$this->getLang('L_DEFAULT_MENU_DECONNEXION'),'cdata');
			@define('L_SAVE_SUCCESSFUL','#MAJ #Plugin #adhesion #réussit');#Fix notice in saveParams
		}#fi 2.1.1
		if ($upParms) @$this->saveParams();#if in public (by internaute or ...) error class plxmsg unexits
	}

	/**
	 * Méthode qui préconfigure le plugin
	 *
	 * @return	stdio
	 * @author	Cyril MAGUIRE
	 **/
	public function onActivate() {
		$plxAdmin = plxAdmin::getInstance();
		#Paramètres par défaut
		if(!is_file($this->plug['parameters.xml'])) {
			$this->setParam('adherents', 'data/configuration/', 'cdata');
			$this->setParam('mnuName', 'Devenir membre', 'string');
			$this->setParam('domaine_asso', plxUtils::strCheck($plxAdmin->aConf['racine']), 'string');
			$this->saveParams();
		}
		if (!is_dir(PLX_ROOT.$this->getParam('adherents').'adhesions')) {
			@mkdir(PLX_ROOT.$this->getParam('adherents').'adhesions');
			$file = PLX_ROOT.PLX_CONFIG_PATH.'.htaccess';
			$newfile = PLX_ROOT.$this->getParam('adherents').'adhesions/.htaccess';
			if (!file_exists($newfile))
				if (!copy($file, $newfile)) @$_SESSION['error'] .= sprintf($this->getLang('L_ERROR_FILE_COPY'),$file,$newfile).PHP_EOL;
		}
		# Si le fichier unique des adhérents existe (ancienne version), on le découpe
		if (is_file(PLX_ROOT.$this->getParam('adherents').'plugin.adhesion.adherents.xml')) {
			$this->genNewFilesFormOldData(PLX_ROOT.$this->getParam('adherents').'plugin.adhesion.adherents.xml');
		}
		if (isset($plxAdmin->plxPlugins->aPlugins["gutuma"])) {
			$listeDeDiffusion = plxUtils::strCheck($plxAdmin->aConf['title']);
			if ($listeDeDiffusion == '') {
				$listeDeDiffusion = 'Newsletters';
			}
			# On crée la liste de diffusion si elle n'existe pas
			if ($this->name != $listeDeDiffusion) {
				$this->listDiff = $this->createGutumaList($listeDeDiffusion);
			}
		}
		//Si les plugins lockArticles et plxMyPrivateStatic sont activés, on les désactive
		if (isset($plxAdmin->plxPlugins->aPlugins["lockArticles"])) {
			$content['selection'] ='deactivate';
			$content['plugName'] = array('lockArticles'=>'on');
			$content['action']['lockArticles'] = 'on';
			$content['chkAction'] = array(0 => 'lockArticles');
			$plxAdmin->plxPlugins->saveConfig($content);
		}
		if (isset($plxAdmin->plxPlugins->aPlugins["plxMyPrivateStatic"])) {
			$content['selection'] ='deactivate';
			$content['plugName'] = array('plxMyPrivateStatic'=>'on');
			$content['action']['plxMyPrivateStatic'] = 'on';
			$content['chkAction'] = array(0 => 'plxMyPrivateStatic');
			$plxAdmin->plxPlugins->saveConfig($content);
		}
		//Si le plugin openStreetMap est activé, on le désactive et on le réactive pour qu'il soit chargé après
		if (isset($plxAdmin->plxPlugins->aPlugins["openStreetMaps"])) {
			$content['selection'] ='deactivate';
			$content['plugName'] = array('openStreetMaps'=>'on');
			$content['action']['openStreetMaps'] = 'on';
			$content['chkAction'] = array(0 => 'openStreetMaps');
			$plxAdmin->plxPlugins->saveConfig($content);
			$content['selection'] ='activate';
			$content['plugName'] = array('adhesion'=>'on','openStreetMaps'=>'on');
			$content['action']['adhesion'] = 'on';
			$content['action']['openStreetMaps'] = 'on';
			$content['chkAction'] = array(0 => 'adhesion',1 => 'openStreetMaps');
			$plxAdmin->plxPlugins->saveConfig($content);
			$this->osmHasToBeActivated = false;
			
		}
	}

	/**
	 * Méthode qui récupère les infos enregistrées dans le fichier data/configuration/plugin.adhesion.adherents.xml
	 *
	 * @param $filename ressource le chemin vers le fichier des adhérents
	 * @return array
	 *
	 * @author Cyril MAGUIRE
	 */
	private function getAdherentsFromOldFile($filename) {

		if(!is_file($filename)) return;

		# Mise en place du parseur XML
		$data = implode('',file($filename));
		$parser = xml_parser_create(PLX_CHARSET);
		xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
		xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,0);
		xml_parse_into_struct($parser,$data,$values,$iTags);
		xml_parser_free($parser);
		if(isset($iTags['adherent']) AND isset($iTags['nom'])) {
			$nb = sizeof($iTags['nom']);
			$size=ceil(sizeof($iTags['adherent'])/$nb);
			for($i=0;$i<$nb;$i++) {
				$attributes = $values[$iTags['adherent'][$i*$size]]['attributes'];
				$number = $attributes['number'];
				# Recuperation du nom
				$this->oldAdherentsList[$number]['nom']=plxUtils::getValue($values[$iTags['nom'][$i]]['value']);
				# Recuperation du prenom
				$this->oldAdherentsList[$number]['prenom']=plxUtils::getValue($values[$iTags['prenom'][$i]]['value']);
				# Recuperation de l'adresse 1
				$this->oldAdherentsList[$number]['adresse1']=plxUtils::getValue($values[$iTags['adresse1'][$i]]['value']);
				# Recuperation de l'adresse 2
				$this->oldAdherentsList[$number]['adresse2']=plxUtils::getValue($values[$iTags['adresse2'][$i]]['value']);
				# Recuperation du code postal
				$this->oldAdherentsList[$number]['cp']=plxUtils::getValue($values[$iTags['cp'][$i]]['value']);
				# Recuperation de la ville
				$this->oldAdherentsList[$number]['ville']=plxUtils::getValue($values[$iTags['ville'][$i]]['value']);
				# Recuperation du téléphone
				$this->oldAdherentsList[$number]['tel']=plxUtils::getValue($values[$iTags['tel'][$i]]['value']);
				# Recuperation du mail
				$this->oldAdherentsList[$number]['mail']=plxUtils::getValue($values[$iTags['mail'][$i]]['value']);
				# Recuperation du choix quant à l'adhésion
				$this->oldAdherentsList[$number]['choix']=plxUtils::getValue($values[$iTags['choix'][$i]]['value']);
				# Recuperation du choix pour le mailing
				$this->oldAdherentsList[$number]['mailing']=plxUtils::getValue($values[$iTags['mailing'][$i]]['value']);
				# Recuperation du statut de l'adhérent
				$this->oldAdherentsList[$number]['validation']=plxUtils::getValue($values[$iTags['validation'][$i]]['value']);
				# Recuperation de la date de première adhésion
				$this->oldAdherentsList[$number]['firstDate']=plxUtils::getValue($values[$iTags['firstDate'][$i]]['value']);
				# Recuperation de la date de validation de l'adhésion
				$this->oldAdherentsList[$number]['date']=plxUtils::getValue($values[$iTags['date'][$i]]['value']);
				# Recuperation de la chaine salt de l'adhérent
				$this->oldAdherentsList[$number]['salt']=plxUtils::getValue($values[$iTags['salt'][$i]]['value']);
				# Recuperation du mot de passe cripté de l'adhérent
				$this->oldAdherentsList[$number]['password']=plxUtils::getValue($values[$iTags['password'][$i]]['value']);
				# Recuperation de la clé
				$this->oldAdherentsList[$number]['cle']=plxUtils::getValue($values[$iTags['cle'][$i]]['value']);
				# Recuperation des chaines aléatoires
				$this->oldAdherentsList[$number]['rand1']=plxUtils::getValue($values[$iTags['rand1'][$i]]['value']);
				$this->oldAdherentsList[$number]['rand2']=plxUtils::getValue($values[$iTags['rand2'][$i]]['value']);
				if ($this->getParam('typeAnnuaire') == 'professionnel') {
					# Recuperation de l'activité
					$this->oldAdherentsList[$number]['activite']=plxUtils::getValue($values[$iTags['activite'][$i]]['value']);
					# Recuperation de l'établissement
					$this->oldAdherentsList[$number]['etablissement']=plxUtils::getValue($values[$iTags['etablissement'][$i]]['value']);
					# Recuperation du service
					$this->oldAdherentsList[$number]['service']=plxUtils::getValue($values[$iTags['service'][$i]]['value']);
					# Recuperation du poste
					$this->oldAdherentsList[$number]['tel_office']=plxUtils::getValue($values[$iTags['tel_office'][$i]]['value']);
				}
				if ($this->getParam('showAnnuaire') == 'on') {
					# Recuperation du choix sur le partage des coordonnées
					$this->oldAdherentsList[$number]['coordonnees']=plxUtils::getValue($values[$iTags['coordonnees'][$i]]['value']);
				}
			}
		}
		//tri du tableau par ordre alphabétique des noms
		$tmp = array();
		foreach($this->oldAdherentsList as $id=>$v){
			$tmp[$v["nom"]] = array(
				'id'=>$id,
				'details'=>$v
			);
		}
		ksort($tmp);
		$this->oldAdherentsList = array();
		foreach ($tmp as $nom => $value) {
			$this->oldAdherentsList[$value['id']] = $value['details'];
		}
		return $this->oldAdherentsList;
	}

	private function genNewFilesFormOldData($filename) {

		$this->getAdherentsFromOldFile($filename);

		foreach ($this->oldAdherentsList as $id => $adherent) {
			$fileName = $id.'.'.plxUtils::title2filename($adherent['nom'].'.'.$adherent['prenom']).'.'.(empty($adherent['firstDate'])? time() : $adherent['firstDate']).'.xml';
			# On génére le fichier XML
			$xml = "<?xml version=\"1.0\" encoding=\"".PLX_CHARSET."\"?>\n";
			$xml .= "<document>\n";
				$xml .= "\t<adherent number=\"".$id."\">\n\t\t";
				$xml .= "<nom><![CDATA[".plxUtils::cdataCheck($adherent['nom'])."]]></nom>\n\t\t";
				$xml .= "<prenom><![CDATA[".plxUtils::cdataCheck($adherent['prenom'])."]]></prenom>\n\t\t";
				$xml .= "<adresse1><![CDATA[".plxUtils::cdataCheck($adherent['adresse1'])."]]></adresse1>\n\t\t";
				$xml .= "<adresse2><![CDATA[".plxUtils::cdataCheck($adherent['adresse2'])."]]></adresse2>\n\t\t";
				$xml .= "<cp><![CDATA[".plxUtils::cdataCheck($adherent['cp'])."]]></cp>\n\t\t";
				$xml .= "<ville><![CDATA[".plxUtils::cdataCheck($adherent['ville'])."]]></ville>\n\t\t";
				$xml .= "<tel><![CDATA[".plxUtils::cdataCheck($adherent['tel'])."]]></tel>\n\t\t";
				$xml .= "<mail><![CDATA[".plxUtils::cdataCheck($adherent['mail'])."]]></mail>\n\t\t";
				$xml .= "<choix><![CDATA[".plxUtils::cdataCheck($adherent['choix'])."]]></choix>\n\t\t";
				$xml .= "<mailing><![CDATA[".plxUtils::cdataCheck($adherent['mailing'])."]]></mailing>\n\t\t";
				$xml .= "<salt><![CDATA[".plxUtils::cdataCheck($adherent['salt'])."]]></salt>\n\t\t";
				$xml .= "<password><![CDATA[".plxUtils::cdataCheck($adherent['password'])."]]></password>\n\t\t";
				$xml .= "<rand1><![CDATA[".plxUtils::cdataCheck($adherent['rand1'])."]]></rand1>\n\t\t";
				$xml .= "<rand2><![CDATA[".plxUtils::cdataCheck($adherent['rand2'])."]]></rand2>\n\t\t";
				$xml .= "<cle><![CDATA[".plxUtils::cdataCheck($adherent['cle'])."]]></cle>\n\t\t";
				$xml .=	"<validation>".plxUtils::cdataCheck($adherent['validation'])."</validation>\n\t\t";
				$xml .=	"<firstDate>".plxUtils::cdataCheck($adherent['firstDate'])."</firstDate>\n\t\t";
				$xml .=	"<date>".plxUtils::cdataCheck($adherent['date'])."</date>\n\t\t";
				if ($this->getParam('typeAnnuaire') == 'professionnel') {
				$xml .= "<activite><![CDATA[".plxUtils::cdataCheck($adherent['activite'])."]]></activite>\n\t\t";
				$xml .= "<etablissement><![CDATA[".plxUtils::cdataCheck($adherent['etablissement'])."]]></etablissement>\n\t\t";
				$xml .= "<service><![CDATA[".plxUtils::cdataCheck($adherent['service'])."]]></service>\n\t\t";
				$xml .= "<tel_office><![CDATA[".plxUtils::cdataCheck($adherent['tel_office'])."]]></tel_office>\n\t";
				}
				if ($this->getParam('showAnnuaire') == 'on') {
				$xml .= "\t<coordonnees><![CDATA[".plxUtils::cdataCheck($adherent['coordonnees'])."]]></coordonnees>\n\t";
				}
				$xml .= "</adherent>\n";
			$xml .= "</document>";
			# On écrit le fichier
			if(!plxUtils::write($xml, PLX_ROOT.$this->getParam('adherents').get_class($this).'s/'.$fileName)) {
				$_SESSION['error'] = $this->getLang('L_WARNPRM_ADHERENTS').'<br/>' ;
				break;
			}
		}
	}
	/**
	 * Méthode qui Charge les infos d'une des pages statiques : plxMotor->mode
	 *
	 * @return	stdio
	 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
	 **/
	public function plxShowConstruct() {
		echo '<?php '; ?>
			$nomMnu = '';

			switch($this->plxMotor->mode){
				case 'login-page':
					$nomMnu = '<?php echo $this->getParam('mnuConnexion') ?>';
					break;
				case 'adhesion':
					$nomMnu = '<?php echo $this->getParam('mnuAdhesion') ?>';
					break;
				case 'adherer':
					$nomMnu = '<?php echo $this->getParam('mnuAdherer') ?>';
					break;
				case 'forgetmypass':
					$nomMnu = '<?php echo $this->getParam('mnuForgetMyPass') ?>';
					break;
				case 'changemypass':
					$nomMnu = '<?php echo $this->getParam('mnuForgetMyPass') ?>';
					break;
				case 'annuaire':
					$nomMnu = '<?php echo $this->getParam('mnuAnnuaire') ?>';
					break;
				case 'myaccount':
					$nomMnu = '<?php echo $this->getParam('mnuMyAccount') ?>';
					break;
//				default:return;#MEMO,plxShowConstruct NEVER RETURN, lock other static plugs & more
			}

			# Hook Plugins
			eval($this->plxMotor->plxPlugins->callHook('plxShowConstruct<?php echo __CLASS__ ?>'));
			if(!empty($nomMnu)){
#				$nomMnu = '<?php echo isset($_SESSION['account'])?$this->getParam('mnuMembers'):$this->getParam('mnuName') ?> / ' . $nomMnu;# + first name menu on page title (h2)
				$array = array();
				$array[$this->plxMotor->cible] = array(
					'name'		=> $nomMnu,
					'menu'		=> '',//oui
					'url'		=> $this->plxMotor->mode.'.html',
					'readable'	=> 1,
					'active'	=> 1,
					'group'		=> ''//grp name
				);
				$this->plxMotor->aStats = array_merge($this->plxMotor->aStats, $array);
//				return;#MEMO,plxShowConstruct NEVER RETURN, lock other static plugs & more
			}
?><?php
	}

	/**
	 * Fonction qui teste si la cotisation est a jour (l'adhérent a 1 an pour régler la cotis (suivante))
	 * @param time : timestamp 
	 * @return	string OR bool
	 * @author	Thomas Ingles
	 **/
	public function cotisationAJour($time,$echo=true,$mail='') {
		if ($this->getParam('annee') == 'illimite') return $echo?$this->getLang('L_COTIS_ZERO'):true;#O K OU
		if ($this->getParam('annee') == 'civile') {
			$datetimeOld = date('Y',$time);
			$datetimeNew = strtotime('01 January '.($datetimeOld+1).' 00:00:01' );
		}
		else {//if ($this->getParam('annee') == 'entiere') {
			$datetimeOld = $time;
			$datetimeNew = $datetimeOld+365*24*60*60;//(60*60*24*365) = 31536000 secondes soit 1 an
		}

		if ( $datetimeNew < time() ){
			$text = $this->getLang('L_COTIS_NO') . '<br/><i class="red">' . intval((time() - $datetimeNew)/24/60/60) . ' ' . $this->getLang('L_DAYS') . '</i>';
			#liens pour envoyer les rappels a cotiser (si admin)
			if(defined('PLX_ADMIN') AND !isset($_GET['print'])) $text .= '<br/><a class="red enveloppe" href="plugin.php?p=adhesion&amp;sendrappel='.base64_encode('true&mail='.$mail).'" title="'.$this->getLang('L_ADMIN_SEND_RAPPEL').'"><span>&#9993;</span></a>&nbsp;<a class="red enveloppe" href="plugin.php?p=adhesion&amp;sendrappel='.base64_encode('true&mail='.$mail).'&amp;num=2" title="'.$this->getLang('L_ADMIN_SEND_RAPPEL2').'"><span>&#9993;</span></a>';
			$return = false;
		}else{
			$text = $this->getLang('L_COTIS_OK') . '<br/><i class="green">' . intval(($datetimeNew - time())/24/60/60) . ' ' . $this->getLang('L_DAYS') . '</i>';
			$return = true;
		}
		return $echo?$text:$return;
	}

	/**
	 * Méthode de traitement du hook plxMotorPreChauffageBegin
	 * 1) On met à jour la liste des adhérents, en fonction de la date de leur inscription, dès qu'une page publique est affichée
	 * 2) On utilise une page statique pour afficher le formulaire d'adhésion
	 *
	 * @return	stdio
	 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
	 **/
	public function plxMotorPreChauffageBegin() {
		$this->plxMotor = plxMotor::getInstance();//$_SESSION
		$this->gutumaPlugin();
		if($this->getParam('annee') != 'illimite'){
			$cron_time = @filemtime(PLX_ROOT.$this->getParam('adherents').'adhesions'.DIRECTORY_SEPARATOR.'.cron');#returns FALSE if file does not exist
			if(date('z') != date('z',$cron_time)){#touch cron.txt, si modifié ce jour on passe
				$content = array();//echo strtotime("20 October 2011");exit();
				if(isset($this->plxRecord_adherents->result)){//chron by user
					foreach ($this->plxRecord_adherents->result as $i => $value) {
						if ($value['validation'] == '1') {
							$nba = 1 + $this->getParam('annee_ss_cotis');#1 + [0 > 5]
							if ($this->getParam('annee') == 'civile') {
								$datetimeOld = date('Y',$value['date']);
								//(60*60*24*365) = 31536000 secondes soit 1 an
								$datetimeNew = strtotime('01 January '.($datetimeOld + $nba).' 00:00:01' );
							}
							else{// ($this->getParam('annee') == 'entiere') {
								$datetimeOld = $value['date'];
								$datetimeNew = $datetimeOld+((365*24*60*60) * $nba);//(60*60*24*365) = 31536000 secondes soit 1 an x nba
							}
							if ( $datetimeNew < time() ){
								foreach ($value as $key => $v) {
									$content[$key.'_'.$value['id']] = $v;
								}
								$content['validation_'.$value['id']] = '0';
								$content['idAdherent'] = array($value['id']);
								$content['selection'] = array('0'=>'devalidation');//Notice: Undefined index: selection IN updateAdherentsList()
								$this->editAdherentslist($content,$value['id'],TRUE);
							}
						}
					}
				}
				file_put_contents(PLX_ROOT.$this->getParam('adherents').'adhesions'.DIRECTORY_SEPARATOR.'.cron', '');#touch .cron ici
			}
		}

		$template = $this->getParam('template')==''?'static.php':$this->getParam('template');
		$cible = (defined('PLX_MYMULTILINGUE')?'../':'').'../../'.PLX_PLUGINS.__CLASS__.'/form';
		$string = '
		$pagesAccount = explode(\' \', \'login-page adhesion adherer myaccount forgetmypass changemypass annuaire\');
		if($this->get) {
		foreach($pagesAccount as $page){
				if(preg_match(\'/^\'.$page.\'\/?/\',$this->get)) {
					$this->mode = $page;
					$this->cible = \''.$cible.'\';
					$this->template = \''.$template.'\';
					return TRUE;
				}
			}
		}'.PHP_EOL;
		# Hook plugins
		if(eval($this->plxMotor->plxPlugins->callHook('plxMotorPreChauffageBegin'.__CLASS__))) return;
		echo '<?php '.$string.' ?>';
	}

	/**
	 * Méthode qui Gère l'affichage du menu des pages statiques du plugin
	 *
	 * @return	stdio
	 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
	 **/
	public function plxShowStaticListEnd() {
		echo '<?php '; ?>
		$class = 'noactive';//($this->plxMotor->mode=='adhesion' || $this->plxMotor->mode=='adherer' || $this->plxMotor->mode=='annuaire')?'active':'noactive';
		$class1 = $this->plxMotor->mode=='adhesion'?'active':'noactive';#Formulaire d'inscription
		$class2 = $this->plxMotor->mode=='adherer'?'active':'noactive';#Pourquoi adherer?
		$class3 = $this->plxMotor->mode=='annuaire'?'active':'noactive';
		$class4 = $this->plxMotor->mode=='login-page'?'active':'noactive';#Formulaire d'entrée
		$hookmenuadhesion = $menuadhesion = $annuaire = '';

		# Hook Plugins
		if(eval($this->plxMotor->plxPlugins->callHook('plxShowStaticListEnd<?php echo __CLASS__ ?>'))) return;

<?php
		if($this->getParam('mnuDisplay')) {# ajout du menu d'accès  && $this->getParam('publicAnnuaire') == 'on'
			if($this->getParam('showAnnuaire') != 'no') :
?>
				$annuaire = '
  <li id="static-adhesion" class="static menu '.$class3.'">
   <a href="<?php echo $this->plxMotor->urlRewrite('?annuaire.html') ?>" title="<?php echo $this->getParam('mnuAnnuaire') ?>"><?php echo $this->getParam('mnuAnnuaire') ?></a>
  </li>';
<?php
			endif;//showAnnuaire
			if(!isset($_SESSION['account'])) {#si l'utilisateur n'est pas connecté :à la page d'adhesion
				if($this->getParam('publicAnnuaire') == 'no') {#on désactive l'annuaire si privé ?>
					$annuaire = '';
<?php
				}//fi publicAnnuaire
?>
				$menuadhesion = '
<li id="static-adhesion" class="static menu '.$class.' menu-item page_item menu-item-type-post_type menu-item-object-page menu-item-has-children">
 <a href="#"><span class="menu-item static group '.$class.'"><?php echo $this->getParam('mnuName') ?></span></a>
 <ul id="static-adhesion-guest" class="sub-menu">
  <li class="static menu '.$class2.'">
   <a href="'.$this->plxMotor->urlRewrite('?adherer.html').'" title="<?php echo $this->getParam('mnuAdherer') ?>"><?php echo $this->getParam('mnuAdherer') ?></a>
  </li>
  <li class="static menu '.$class1.'">
   <a href="'.$this->plxMotor->urlRewrite('?adhesion.html').'" title="<?php echo $this->getParam('mnuAdhesion') ?>"><?php echo $this->getParam('mnuAdhesion') ?></a>
  </li>'.$annuaire.$hookmenuadhesion.'
  <li class="static menu '.$class4.'">
   <a href="'.$this->plxMotor->urlRewrite('?login-page.html').'" title="<?php echo $this->getParam('mnuConnexion') ?>"><?php echo $this->getParam('mnuConnexion') ?></a>
  </li>
 </ul>
</li>'.PHP_EOL;
<?php
			}// fi guest menu
		 	if (isset($_SESSION['account'])){#L'utilisateur est connecté
?>
				$menuadhesion = '
<li id="static-adhesion" class="static menu '.$class.' menu-item page_item menu-item-type-post_type menu-item-object-page menu-item-has-children">
 <a href="#"><span class="menu-item static group '.$class.'"><?php echo $this->getParam('mnuMembers') ?></span></a>
 <ul id="static-adhesion-account" class="sub-menu">'.$annuaire.$hookmenuadhesion.'
  <!-- MENU OSM -->
  <li class="static menu '.$class2.'">
   <a href="'.$this->plxMotor->urlRewrite('?myaccount.html').'" title="<?php echo $this->getParam('mnuMyAccount') ?>"><?php echo $this->getParam('mnuMyAccount') ?></a>
  </li>
  <li class="static menu '.$class1.'">
   <form action="" method="post" id="logout"><input type="hidden" name="logout"><input type="submit" value="<?php echo $this->getParam('mnuDeconnexion') ?>" id="menu-logout-sub"/></form>
  </li>
 </ul>
</li>'.PHP_EOL;
<?php
			}// fi menu utilisateur
?>
			array_splice($menus, <?php echo ($this->getParam('mnuPos')-1) ?>, 0, $menuadhesion);
<?php
		}//fi mnuDisplay
echo ' ?>';
	}

	/**
	 * Méthode qui inclus dataTable (balise css & js) (ThemeEndHead et AdminTopEndHead)
	 *
	 * @return	stdio
	 * @author	Thomas Ingles
	 **/
	public function dataTableIncHead($w=false) {//cdn idée
		$v = PLX_PLUGINS.__CLASS__.'/js/Vanilla-DataTables/vanilla-dataTables.min.';//https://github.com/Mobius1/Vanilla-DataTables/pull/65 & jscompress.com
		if($w)
			$v = $w;
?>
	<link rel="stylesheet" type="text/css" href="<?php echo $v ?>css?v=1.6.14" media="screen" />
	<script type="text/javascript" src="<?php echo $v ?>js?v=1.6.14"></script>
<?php
	}

	/**
	 * Méthode qui ajoute le fichier css dans le fichier header.php du thème
	 *
	 * @return	stdio
	 * @author	Stephane F, Cyril MAGUIRE, Thomas I.
	 **/
	public function ThemeEndHead() {
		$css = "\t".'<link rel="stylesheet" type="text/css" href="'.PLX_PLUGINS.__CLASS__.'/style.css?v='.$this->v.'" media="screen" />'.PHP_EOL;
		switch($this->plxMotor->mode) {
			case "annuaire":
				$this->dataTableIncHead();
//tags,home,tags_password,static_password,article_password,categorie_password,categories_password,adhesion,myaccount,forgetmypass//adherer
			default:
				echo $css;
				if(!isset($_SESSION['lockArticles']));// return;//strpos($this->plxMotor->mode,'_password') === false; not annuary
?>	<style type="text/css">#wall-e{position:absolute;left:-99999px;}</style><?php
		}
	}

	/**
	 * Méthode qui renseigne le titre de la page dans la balise html <title>
	 *
	 * @return	stdio
	 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
	 **/
	public function plxShowPageTitle() {
		echo '<?php ';?>
			# Hook Plugins
			if(eval($this->plxMotor->plxPlugins->callHook('plxShowPageTitle<?php echo __CLASS__ ?>'))) return;

			switch($this->plxMotor->mode) {
				case 'login-page':
					echo plxUtils::strCheck($this->plxMotor->aConf['title'].' - <?php echo $this->getParam('mnuConnexion') ?>');
					return TRUE;
				case 'adhesion':
					echo plxUtils::strCheck($this->plxMotor->aConf['title'].' - <?php echo $this->getParam('mnuName') ?>');
					return TRUE;
				case 'adherer':
					echo plxUtils::strCheck($this->plxMotor->aConf['title'].' - <?php echo $this->getParam('mnuAdherer') ?>');
					return TRUE;
				case 'annuaire':
					echo plxUtils::strCheck($this->plxMotor->aConf['title'].' - <?php echo $this->getParam('mnuAnnuaire') ?>');
					return TRUE;
				case 'myaccount':
					echo plxUtils::strCheck($this->plxMotor->aConf['title'].' - <?php echo $this->getParam('mnuMyAccount') ?>');
					return TRUE;
				case 'forgetmypass':
					echo plxUtils::strCheck($this->plxMotor->aConf['title'].' - <?php echo $this->getParam('mnuForgetMyPass') ?>');
					return TRUE;
				case 'changemypass':
					echo plxUtils::strCheck($this->plxMotor->aConf['title'].' - <?php echo $this->getParam('mnuChangeMyPass') ?>');
					return TRUE;
#				default:
#					echo plxUtils::strCheck($this->plxMotor->aConf['title'].' - '.$this->plxMotor->mode);
#					return TRUE;
			}
?><?php
	}

	/**
	 * Méthode qui référence la page d'adhesion dans le sitemap
	 * Note : annuaire(si:public) : todo : adherer?
	 * @return	stdio
	 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
	 **/
	public function SitemapStatics() {
		foreach(array(get_class($this), ($this->getParam('publicAnnuaire')!='no'?'annuaire':false)) AS $statUrl):
			if (!$statUrl) continue;
?>
	<url>
		<loc><?php echo $this->plxMotor->urlRewrite('?'.$statUrl.'.html') ?></loc>
		<changefreq>monthly</changefreq>
		<priority>0.8</priority>
	</url>
<?php
endforeach;
	}

	/**
	 * Méthode permettant l'export de la liste des adhérents
	 *
	 * @return void
	 * @author Cyril MAGUIRE
	 */
	public function AdminPrepend() {
		$this->plxMotor = plxAdmin::getInstance();
		$this->gutumaPlugin();
		# Impression de la liste des adhérents
		if (isset($_GET['print'])) {
			$this->getAdherents('/^[0-9]{5}.(.[a-z-]+){2}.[0-9]{10}.xml$/');
			# Inclusion des librairies de TBS
			if (version_compare(PHP_VERSION,'5')<0) {
				include_once 'opentbs/tbs_class.php'; // TinyButStrong template engine
			} else {
				include_once 'opentbs/tbs_class_php5.php'; // TinyButStrong template engine
			}
			if (($_GET['print'] == 'xls') ) {
				include_once 'opentbs/tbs_plugin_excel.php'; // Excel plugin
			} else {
				include_once 'opentbs/tbs_plugin_opentbs.php'; // OpenTBS plugin
			}
			include_once 'print.php';
			exit;
		}
	}

	/**
	 * Méthode qui affiche le js et css du plugin de ses parties administratives
	 * [parametres_]plugin.php ok
	 * @author Cyril MAGUIRE, Stephane F
	 */
	public function AdminTopEndHead() {
		$bn = basename($_SERVER['SCRIPT_NAME']);
		$ok = ($bn=='plugin.php'||$bn=='parametres_plugin.php')&&(isset($_GET['p']) && $_GET['p']==__CLASS__);
		if($ok) {
?>
	<link rel="stylesheet" type="text/css" href="<?php echo PLX_PLUGINS.__CLASS__ ?>/style-admin.css?v=<?php echo $this->v ?>" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo PLX_PLUGINS.__CLASS__ ?>/style.css?v=<?php echo $this->v ?>" media="screen" />
	<script type="text/javascript">
/* <![CDATA[ */
		if (typeof jQuery == 'undefined') {
			document.write('<script type="text\/javascript" src="<?php echo PLX_PLUGINS.__CLASS__ ?>\/js\/jquery.min.js?v=3.2.1"><\/script>');
		}
/* ]]> */
	</script>
<?php
			$this->dataTableIncHead();//cdn ou jquery:::PLX_PLUGINS.__CLASS__.'/js/jquery-DataTables/datatables.min.' ::: AdminAdhesionUsersFoot()
		}
	}

	/**
	 * Méthode de traitement du hook AdhesionUserTop ::: OLD
	 *
	 * @return	stdio
	 * @author	Stephane F
	 **/
	public function AdminAdhesionUsersTop() {
?>
		<p class="hide" style="display:none;text-align:right">
			<?php $this->lang('L_LABEL_FIND') ?>&nbsp;:&nbsp;
			<input type="text" id="txtFilter" name="txtFilter" />&nbsp;
			<img style="display:none" id="imgDeleteFilter" src="<?php echo PLX_PLUGINS.__CLASS__ ?>/cancel.gif" alt="<?php $this->lang('L_LABEL_DELETE_FILTER') ?>" title="<?php $this->lang('L_LABEL_DELETE_FILTER') ?>" />
		</p>
<?php
	}
	/**
	 * Méthode de traitement du hook AdhesionUserTopValid ::: OLD
	 *
	 * @return	stdio
	 * @author	Stephane F
	 **/
	public function AdminAdhesionUsersTopValid() {
?>
		<p class="hide" style="display:none;text-align:right">
			<?php $this->lang('L_LABEL_FIND') ?>&nbsp;:&nbsp;
			<input type="text" id="txtFilterV" name="txtFilterV" />&nbsp;
			<img style="display:none" id="imgDeleteFilterV" src="<?php echo PLX_PLUGINS.__CLASS__ ?>/cancel.gif" alt="<?php $this->lang('L_LABEL_DELETE_FILTER') ?>" title="<?php $this->lang('L_LABEL_DELETE_FILTER') ?>" />
		</p>
<?php
	}

	/**
	 * hook admin.php Affiche le javascript dataTable
	 * DataTable js + Chercher Adhérent(e)s
	 * @return	stdio
	 * @author	Thomas Ingles
	 **/
	public function AdminAdhesionUsersFoot() {
?>
<script type="text/javascript">
/* <![CDATA[ */
for(var dt=0; dt<2; dt++){
	var dataTable = new DataTable("#table"+dt, {
		searchable: true,
		fixedHeight: false,
		perPageSelect: [5, 10, 15, 20, 25, 50, 100],
		perPage: <?php echo ($this->getParam('perPage') == '' ? 50 : $this->getParam('perPage')); ?>,
		// Customise the display text
		labels: {
			placeholder: "<?php $this->lang('L_LABEL_JSDTABLE_PLACEHOLDE') ?>", // The search input placeholder
			perPage: "<?php $this->lang('L_LABEL_JSDTABLE_PERPGS') ?>", // per-page dropdown label
			noRows: "<?php $this->lang('L_LABEL_JSDTABLE_NODATA') ?>", // Message shown when there are no search results
			info: "<?php $this->lang('L_LABEL_JSDTABLE_INFO') ?>", //Showing {start} to {end} of {rows} entries
		}
	});
}
dataTable.on('datatable.init',function(){autres();});
dataTable.on('datatable.sort',function(column, direction){autres();});
//~ dataTable.on('datatable.perpage',function(perpage){});
//~ clrSrch = document.createElement("span");
//~ clrSrch.innerHTML = '<img src="<?php echo PLX_PLUGINS.__CLASS__ ?>/cancel.gif" alt="<?php $this->lang('L_LABEL_DELETE_FILTER') ?>" title="<?php $this->lang('L_LABEL_DELETE_FILTER') ?>" />';
//~ dataTable.on('datatable.search',function(query, matched){);
function autres(){//datatable.init & datatable.sort
	var other_activity = window.document.getElementsByClassName('autre');
	for(var a = 0;a < other_activity.length; a++){
		var i = other_activity[a].id.replace(/_autre/,'');
		var sel_activity = window.document.getElementById(i);
		if(sel_activity.value!='autre')other_activity[a].style.display='none';//fix old chromium
	}
}
function activite_new(e){//onchange
	var i = e.id.replace(/activite/,'activite_autre');
	var other = window.document.getElementById(i);
	if(e.value!='autre'){other.style.display='none;';}
	else{other.style.display='';other.focus();/*activite_evnt(e,'focus');*/}
}
function activite_evnt(e,o){//on(focus|blur)
//console.log('activite_evnt',e,o);
	if(o=='blur'){e.value = e.value?e.value:'<?php $this->lang('L_ADMIN_NOT_DONE') ?>';}
	else{e.value = e.value!='<?php $this->lang('L_ADMIN_NOT_DONE') ?>'?e.value:'';}
}
function choice(e){//onchange
	if (e.value == 'stop'){
		if(!confirm('<?php $this->lang('L_ADMIN_CONFIRM') ?>'))
			e.value = 'adhesion';
		else
			e.className = e.className+' stop';
	}else{
		e.className = e.className.replace(/ stop/,'');
	}
}
function adherentsVerif(i){//admin.php
	var s = window.document.getElementById('id_selection'+i).value;//false, (de)validation, update, delete
	if(s=='false') return false;
	var a = window.document.getElementsByName('idAdherent[]');
	var ok = false;
	var e = 0;
	while(e<a.length){
		if(a[e].form.id == 'form'+i){
			if(a[e].checked){
				ok = true;
				break;
			}
		}
		e = e + 1;
	}
	if (!ok) return false;

	if(s=='devalidation')
		return confirm('<?php $this->lang('L_ADMIN_LIST_DEVALIDATION')?>');
	if(s=='regenerepass')
		return confirm('<?php $this->lang('L_ADMIN_LIST_REGENEREPASS')?>');
	if(s=='delete')
		return confirm('<?php $this->lang('L_ADMIN_DELETE')?>');
}
var nojs = document.getElementsByClassName('in-action-bar-nojs');
for(var a = 0; a < nojs.length; a++){
 nojs[a].classList.add('in-action-bar');
// nojs[a].classList.remove('in-action-bar-nojs');//dont finish ? it's work ;)
}
/* ]]> */
</script>
<?php
	}

	/**
	 * Méthode qui affiche un message s'il y a un message à afficher
	 *
	 * @return	stdio
	 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
	 **/
	public function AdminTopBottom() {
		echo '<?php '; ?>
		$nomgrf = "<?php echo __CLASS__ ?>";
		$greffe = $plxAdmin->plxPlugins->aPlugins[$nomgrf];
		$nomgrf = '<a href="parametres_plugin.php?p='.$nomgrf.'#id_%s">Plugin '.$nomgrf.'%s</a>&nbsp;:&nbsp;';
		if($greffe->isGutumaActivated) {
			if(!$greffe->ok) {
				echo '<p class="notice warning">'.sprintf($nomgrf,'gutuma','').'<?php $this->lang('L_WARNPRM_NO_GULIST') ?></p>';
				plxMsg::Display();
			}
		}
		$tstPrm = explode(' ','email nom_asso adresse_asso adherents');
		foreach($tstPrm AS $prm){
			if($greffe->getParam($prm)=="") {
				echo '<p class="warning">'.sprintf($nomgrf,$prm,' '.L_PLUGINS_CONFIG).$greffe->getLang('L_WARNPRM_'.strtoupper($prm)).'</p>';
				plxMsg::Display();
			}
		}
		$tstPrm = explode(' ','subject desc_adhesion validation_subject devalidation_subject rappel_subject cotis_subject thankyou subject_password');
		foreach($tstPrm AS $prm){
			if($greffe->getParam($prm)=="" OR $greffe->getParam($prm)==str_replace("'","’",$greffe->getLang('L_DEFAULT_'.strtoupper($prm)))) {
				$hprm = str_replace('desc_adhesion','mnuAdhesion',$prm);#desc_adhesion hidden hack
				echo '<p class="notice warning">'.sprintf($nomgrf,$hprm,' config').$greffe->getLang('L_WARNPRM_'.strtoupper($prm)).'</p>';
				plxMsg::Display();
			}
		}

		if(trim(strip_tags(htmlspecialchars_decode($greffe->getParam('desc_adhesion'))))==str_replace("'","’",$greffe->getLang('L_DEFAULT_DESC'))) {
			echo '<p class="notice warning">'.sprintf($nomgrf,'mnuAdhesion',' config').$greffe->getLang('L_WARNPRM_DESC_ADHESION').'</p>';#desc_adhesion hidden hack
			plxMsg::Display();
		}

		if (!is_dir(PLX_ROOT.$greffe->getParam('adherents').'adhesions')) {
			echo '<p class="warning">'.sprintf($nomgrf,'adherents',' config').$greffe->getLang('L_WARNPRM_NO_DIR_ADHES').'</p>';
			plxMsg::Display();
		}
		unset($greffe,$nomgrf,$prm,$tstPrm);
?><?php
	}

	/**
	 * Méthode qui inverse la position des lettres composant un email afin d'éviter les spams
	 *
	 * @param email string l'email à obfusquer
	 * @return string
	 *
	 * @author Cyril MAGUIRE
	 */
	public function badEmail($email) {
		//$email = str_replace(array('@','.'),array('[AT]','[DOT]'),$email);
		$longueur = strlen(trim($email));
		for ($i=1; $i < $longueur+1 ; $i++) {
			$tmp[$i] = $email[$longueur-$i];
		}
		$email = implode('', $tmp);
		return '<span onClick="joindre(this.textContent);" class="baddirection">'.str_replace(array('@','.'),array('_[TA]_','_[TOD]_'),$email).'</span>';
	}

	/**
	 * Méthode d'envoi de mail
	 *
	 * @param	name		string 			Nom de l'expéditeur
	 * @param	from		string 			Email de l'expéditeur
	 * @param	to			array/string	Adresse(s) du(des) destinataires(s)
	 * @param	subject		string			Objet du mail
	 * @param	body		string			Contenu du mail
	 * @param	contentType	string			Format du mail : txt ou html
	 * @param	cc			array			Les destinataires en copie
	 * @param	bcc			string			Les destinataires en copie
	 * @return				boolean			renvoie FAUX en cas d'erreur d'envoi
	 * @author	Cyril MAGUIRE, Thomas Ingles
	 */
	public function sendEmail($name, $from, $to, $subject, $body, $contentType="html", $cc=FALSE, $bcc=FALSE) {
//return TRUE;//preseve free in dev Warning: mail(): connect() failed: Connection refused  && Warning: mail(): mail server down
		$eBody  = '<html><head><title>'.$subject.'</title></head><body style="margin:10px;">'.$body.'</body></html>';
		if(is_array($to)) {
			$to = implode(', ', $to);
		}
		if(is_array($cc)) {
			$cc = implode(', ', $cc);
		}
		if(is_array($bcc)) {
			$bcc = implode(', ', $bcc);
		}

		$headers  = "From: ".$name." <".$this->getParam('email').">\r\n";
		$headers .= "Reply-To: no-reply@".$this->getParam('domaine_asso')."\r\n";
		$headers .= 'MIME-Version: 1.0'."\r\n";
		// Content-Type
		if($contentType == 'html') {
			$headers .= 'Content-type: text/html; charset="'.PLX_CHARSET.'"'."\r\n";
		} else {
			$headers .= 'Content-type: text/plain; charset="'.PLX_CHARSET.'"'."\r\n";
		}

		$headers .= 'Content-transfer-encoding: 8bit'."\r\n";

		if($cc != "") {
			$headers .= 'Cc: '.$cc."\r\n";
		}
		if($bcc != "") {
			$headers .= 'Bcc: '.$bcc."\r\n";
		}
		
		if($this->freeMail($to, $subject, $eBody, $headers)==false){#pp.free.fr Fix : Warning: mail(): 1 in adhesion.php on line 901
			return FALSE;//$out.="<pre style='border: 1px dotted #666666;padding:10px;'><code>L'envoi du message n'a pas été réalisé en raison des limitations des serveurs de Free, merci de réessayer un peu plus tard.</code></pre>";
		} else {return TRUE;}
/*
		if(mail($to, $subject, $eBody, $headers)){#BASIQUE
			return TRUE;
		} else {
			return FALSE;
		}
*/
	}
#les.pages.perso.chez.free.fr/l-art-d-envoyer-des-mails-depuis-les-pp-de-free.io
	public function freeMail($to, $subject, $message, $additional_headers=null, $additional_parameters=null) {
  // if(isset($_SESSION['profil']) AND $_SESSION['profil'] < 1){var_dump($to, $subject, $message, $additional_headers);exit;}#DBG
		$start_time = time();
		$res = mail($to, $subject, $message, $additional_headers, $additional_parameters);
		$time = time() - $start_time;
		if(strpos('free.fr',$_SERVER["HTTP_HOST"])) $res = $res & ($time>1);//si free.fr
		return $res;
	}

	/**
	 * Méthode permettant de mettre en forme le mail de notification à l'administrateur
	 *
	 * @param $nom string nom de l'adhérent
	 * @param $prenom string prenom de l'adhérent
	 * @param $adresse1 string première partie de l'adhérent
	 * @param $adresse2 string deuxième partie de l'adhérent
	 * @param $cp numeric code postal de l'adhérent
	 * @param $ville string ville de l'adhérent
	 * @param $tel numeric téléphone de l'adhérent
	 * @param $mail string email de l'adhérent
	 * @param $choix string choix de l'adhérent quant à l'adhésion
	 * @param $mailing string choix de l'adhérent quant à l'envoi de mail par l'asso
	 * @param $pro array ensemble des caractéristiques professionnelles
	 *
	 * @return string
	 * @author Cyril MAGUIRE
 	 */
	public function notification($nom,$prenom,$adresse1,$adresse2,$cp,$ville,$tel,$mail,$choix,$mailing,$pro=array()) {
		return '
		<table style="border:none;">
			<thead>
				<tr>
					<th style="border:none;text-align:left;">'.$this->getLang('NAME').'&nbsp;: '.$nom.'</th>
				</tr>
				<tr>
					<th style="border:none;text-align:left;">'.$this->getLang('FIRST_NAME').'&nbsp;: '.$prenom.'</th>
				</tr>
			</thead>
			<tbody>
				'.($this->getParam('typeAnnuaire') == 'professionnel' ?
			'
				<tr>
					<td style="border:none;">'.$this->getLang('L_FORM_ACTIVITY').'&nbsp;: '.$pro['activite'].'</td>
				</tr>
				<tr>
					<td style="border:none;">'.$this->getLang('L_FORM_SOCIETY').'&nbsp;: '.$pro['etablissement'].'</td>
				</tr>
				<tr>
					<td style="border:none;">'.$this->getLang('L_FORM_SERVICE').'&nbsp;: '.$pro['service'].'</td>
				</tr>
			': '').'
				<tr>
					<td style="border:none;">'.$this->getLang('L_FORM_ADDRESS').'&nbsp;: '.$adresse1.'</td>
				</tr>
				<tr>
					<td style="border:none;">'.$adresse2.'</td>
				</tr>
				<tr>
					<td style="border:none;">'.$this->getLang('L_FORM_ZIP_CODE').'&nbsp;: '.$cp.'</td>
				</tr>
				<tr>
					<td style="border:none;">'.$this->getLang('L_FORM_CITY').'&nbsp;: '.$ville.'</td>
				</tr>
				<tr>
					<td style="border:none;">'.$this->getLang('L_FORM_TEL').'&nbsp;: '.$this->formatFrenchPhoneNumber($tel).'</td>
				</tr>
				'.($this->getParam('typeAnnuaire') == 'professionnel' ?
			'
				<tr>
					<td style="border:none;">'.$this->getLang('L_FORM_TEL_OFFICE').'&nbsp;: '.$this->formatFrenchPhoneNumber($pro['tel_office']).'</td>
				</tr>
			': '').'
				<tr>
					<td style="border:none;">&nbsp;</td>
				</tr>
				<tr>
					<td style="border:none;">'.$this->getLang('L_NOTI_MAIL').'&nbsp;: '.$mail.'</td>
				</tr>
				<tr>
					<td style="border:none;">'.(($choix == 'adhesion') ? $this->getLang('L_ADHESION_OK').' '.$this->getParam('nom_asso') : ($choix == 'renouveler') ? $this->getLang('L_ADHESION_RENEW') : $this->getLang('L_ADHESION_STOP')).'</td>
				</tr>
				'.($this->getParam('showAnnuaire') == 'on' ?
			'
				<tr>
					<td style="border:none;">'.(($pro['coordonnees'] == 'rec') ? $this->getLang('L_COORD_REC') : ($pro['coordonnees'] == 'public') ? $this->getLang('L_COORD_PUBLIC') : $this->getLang('L_COORD_NO')).'</td>
				</tr>
			': '').'
				<tr>
					<td style="border:none;">'.(($mailing == 'maillist') ? $this->getLang('L_NOTI_RULES_NEWS') : $this->getLang('L_NOTI_RULES_NEWS_NO')).' '.$this->getParam('nom_asso').'</td>
				</tr>
				<tr>
					<td style="border:none;">'.$this->getLang('L_NOTI_SENDING_DATE').' '.date($this->getLang('L_DATE_FORMAT')).'</td>
				</tr>
			</tbody>
		</table>
		';
	}

	/**
	 * Méthode qui affiche un message contenant les instructions à suivre pour régler son adhésion
	 *
	 * @return string
	 * @author Cyril MAGUIRE, Thomas Ingles
	 */
	public function adresse($spam=TRUE) {//add nom + prenom 
  if($this->getParam('annee') == 'illimite')
		//~ return sprintf($this->getLang('L_NOCOTIS_HTML'),$this->getParam('nom_asso'),$this->getParam('adresse_asso')).($spam?$this->getLang('L_SPAM_HTML'):'');
		return $this->getLang('L_NOCOTIS_HTML')/*,$this->getParam('nom_asso'),$this->getParam('adresse_asso')*/.($spam?$this->getLang('L_SPAM_HTML'):'');
  else
		return sprintf($this->getLang('L_ADRESS_HTML'),$this->getParam('nom_asso'),$this->getParam('adresse_asso')).($spam?$this->getLang('L_SPAM_HTML'):'');
	}
	///////////////////////////////////////////////////////////
	//
	// Méthodes permettant la modification des listes de Gutuma
	//
	//////////////////////////////////////////////////////////

	/**
	 * Checks the ending of the specified string
	 * @param string $haystack The string to check
	 * @param string $needle The ending to check for
	 * @return bool TRUE if the string ends with the given string, else FALSE
	 */
	public function strEnds($haystack, $needle) {
		$ending = substr($haystack, strlen($haystack) - strlen($needle));
		return $ending === $needle;
	}

	/**
	 * Loads all of the lists
	 * @param bool $load_addresses TRUE if lists addresses should be loaded (default is FALSE)
	 * @param bool $inc_private TRUE if private lists should included (default is TRUE)
	 * @return mixed Array of lists or FALSE if an error occured
	 */
	public function getAllGutumaLists($load_addresses = FALSE, $inc_private = TRUE){
		$lists = array();
		if (!$this->isGutumaActivated) return $lists;
		if ($dh = opendir(realpath(rtrim($this->GutumaListsDir,'/')))) {
			while (($file = readdir($dh)) !== FALSE) {
				if (!@is_dir($file) && $this->strEnds($file, '.php')) {//Warning: is_dir() [function.is-dir]: open_basedir restriction in effect. File(..) is not within the allowed path(s): (/)
					$list = $this->getGutumaList(substr($file, 0, strlen($file - 4)), $load_addresses);
					if ($inc_private || !$list->private)
						$lists[] = $list;
				}
			}
			closedir($dh);
		}
		return $lists;
	}

	/**
	 * Gets the list with the specified id
	 * @param int $id The list id
	 * @param bool $load_addresses TRUE is list addresses should be loaded (default FALSE)
	 * @return mixed The list or FALSE if an error occured
	 */
	public function getGutumaList($id, $load_addresses = FALSE){
		$time_start = microtime();
		$list = array();
		if (!$this->isGutumaActivated) return $list;
		// Open list file
		$lh = @fopen(realpath(rtrim($this->GutumaListsDir,'/').'/'.$id.'.php'), 'r');
		if ($lh == FALSE)
			return FALSE;
		// Read header from first line
		$header = explode("|", fgetss($lh));

		$list['id'] = $header[0];
		$list['name'] = $header[1];
		$list['private'] = (bool)$header[2];
		$list['size'] = (int)$header[3];

		// Read all address lines
		if ($load_addresses) {
			$addresses = array();
			while (!feof($lh)) {
				$address = trim(fgets($lh));
				if (strlen($address) > 0)
					$addresses[] = $address;
			}
			$list['addresses'] = $addresses;
		}

		fclose($lh);
		return $list;
	}

	/**
	 * Creates a new address list
	 * @param string $name The list name
	 * @param bool $private TRUE if the list should be private (default is FALSE)
	 * @param array $addresses
	 * @return mixed The new list if it was successfully created, else FALSE
	 */
	public function createGutumaList($name, $private = FALSE, $addresses = NULL){
		if (!$this->isGutumaActivated) return true;
		if ($name == '' || preg_match('[^a-zA-Z0-9 \-]', $name))
			return FALSE;
		// Demo mode check for number of addresses
		if (isset($addresses) && count($addresses) >= 100)
			return FALSE;
		// Check for duplicate name
		$all_lists = $this->listsDiff;
		foreach ($all_lists as $l) {
			if (strcasecmp($l->name, $name) == 0)
				return FALSE;
		}
		// Demo mode check for number of lists
		if (count($all_lists) >= 100)
			return FALSE;
		$this->id = time();
		$this->name = $name;
		$this->private = $private;
		$this->addresses = isset($addresses) ? $addresses : array();
		// Save the list
		if (!$this->updateGutumaList()){
			return FALSE;
		}
		return $this;
	}

	/**
	 * Adds the specified address to this list
	 * @param string $address The address to add
	 * @param bool $update TRUE if list should be updated, else FALSE
	 * @return bool TRUE if the address was successfully added
	 */
	public function addAdressInGutumaList($address, $update){

		if (!$this->isGutumaActivated) return true;
		if (in_array($address, $this->addresses))
			return FALSE;

		if (strlen($address) > 320)
			return FALSE;

		if (count($this->addresses) >= 100)
			return FALSE;

		// Add and then sort addresses alphabetically
		$this->addresses[] = $address;
		natcasesort($this->addresses);

		if ($update) {
			if (!$this->updateGutumaList())
				return FALSE;
		}
		return TRUE;
	}

	/**
	 * Updates this address list, i.e., saves any changes
	 * @return bool TRUE if operation was successful, else FALSE
	 */
	public function updateGutumaList() {
		if (!$this->isGutumaActivated) return true;
		$lh = @fopen(realpath(rtrim($this->GutumaListsDir,'/')).'/'.$this->id.'.php', 'w');
		if ($lh == FALSE)
			return FALSE;
		fwrite($lh, "<?php die(); ?>".$this->id.'|'.$this->name.'|'.($this->private ? '1' : '0').'|'.count($this->addresses)."\n");
		foreach ($this->addresses as $a)
			fwrite($lh, $a."\n");
		fclose($lh);
		$this->listDiff = $this->addresses;//improve
		return TRUE;
	}

	/**
	 * Removes the specified address from this list
	 * @param string $address The address to remove
	 * @param bool $update TRUE if list should be updated, else FALSE	
	 * @return bool TRUE if operation was successful, else FALSE
	 */
	public function removeAdressFromGutumaList($address, $update){
		if (!$this->isGutumaActivated) return true;
		// Create new address array minus the one being removed
		$found = FALSE;
		$newaddresses = array();

		foreach ($this->addresses as $a) {
			if ($address != $a)
				$newaddresses[] = $a;
			else
				$found = TRUE;
		}
		
		if (!$found)
			return FALSE;

		$this->addresses = $newaddresses;
		
		if ($update) {
			if (!$this->updateGutumaList())
				return FALSE;
		}
		return TRUE;
	}

	///////////////////////////////////////////////////////////
	//
	// Méthodes permettant la gestion des adhérents
	//
	//////////////////////////////////////////////////////////

	/**
	 * Méthode qui selon le paramètre tri retourne sort ou rsort (tri PHP)
	 *
	 * @param	tri	asc ou desc
	 * @return	string
	 * @author	Stéphane F.
	 **/
	protected function mapTri($tri) {
		if($tri=='desc')
			return 'rsort';
		elseif($tri=='asc')
			return 'sort';
		elseif($tri=='alpha')
			return 'alpha';
		else
			return 'rsort';
	}

	/**
	 * Méthode qui récupere la liste des adherents
	 *
	 * @param	publi	before, after ou all => on récupère tous les fichiers (date) ?
	 * @return	boolean	vrai si articles trouvés, sinon faux
	 * @author	Stéphane F
	 **/
	public function getAdherents($motif) {

		# On fait notre traitement sur notre tri
		$ordre = $this->mapTri('asc');
		# On recupere nos fichiers (tries) selon le motif, la pagination, la date de publication
		if($aFiles = $this->plxGlob_adherents->query($motif,'',$ordre,0,false,'all')) {
			# on mémorise le nombre total d'articles trouvés
			foreach($aFiles as $k=>$v) # On parcourt tous les fichiers
				$array[$k] = $this->parseAdherent(PLX_ROOT.$this->getParam('adherents').'adhesions/'.$v);
			# On stocke les enregistrements dans un objet plxRecord
			$this->plxRecord_adherents = new plxRecord($array);
			return true;
		}
		else return false;
	}


	/**
	 * Méthode qui récupère les infos enregistrées dans le fichier xml d'un adhérent
	 *
	 * @param $filename ressource le chemin vers le fichier de l'adhérent
	 * @return array
	 *
	 * @author Cyril MAGUIRE
	 */
	public function parseAdherent($filename) {

		if(!is_file($filename)) return;

		# Mise en place du parseur XML
		$data = implode('',file($filename));
		$parser = xml_parser_create(PLX_CHARSET);
		xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
		xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,0);
		xml_parse_into_struct($parser,$data,$values,$iTags);
		xml_parser_free($parser);
		if(isset($iTags['adherent']) AND isset($iTags['nom'])) {
			$nb = sizeof($iTags['nom']);
			$size=ceil(sizeof($iTags['adherent'])/$nb);
			for($i=0;$i<$nb;$i++) {
				$attributes = $values[$iTags['adherent'][$i*$size]]['attributes'];
				$adherent['id'] = $attributes['number'];
				# Recuperation du nom
				$adherent['nom']=plxUtils::getValue($values[$iTags['nom'][$i]]['value']);
				# Recuperation du prenom
				$adherent['prenom']=plxUtils::getValue($values[$iTags['prenom'][$i]]['value']);
				# Recuperation de l'adresse 1
				$adherent['adresse1']=plxUtils::getValue($values[$iTags['adresse1'][$i]]['value']);
				# Recuperation de l'adresse 2
				$adherent['adresse2']=plxUtils::getValue($values[$iTags['adresse2'][$i]]['value']);
				# Recuperation du code postal
				$adherent['cp']=plxUtils::getValue($values[$iTags['cp'][$i]]['value']);
				# Recuperation de la ville
				$adherent['ville']=plxUtils::getValue($values[$iTags['ville'][$i]]['value']);
				# Recuperation du téléphone
				$adherent['tel']=plxUtils::getValue($values[$iTags['tel'][$i]]['value']);
				# Recuperation du mail
				$adherent['mail']=plxUtils::getValue($values[$iTags['mail'][$i]]['value']);
				# Recuperation du choix quant à l'adhésion
				$adherent['choix']=plxUtils::getValue($values[$iTags['choix'][$i]]['value']);
				# Recuperation du choix pour le mailing
				$adherent['mailing']=plxUtils::getValue($values[$iTags['mailing'][$i]]['value']);
				# Recuperation du statut de l'adhérent
				$adherent['validation']=plxUtils::getValue($values[$iTags['validation'][$i]]['value']);
				# Recuperation de la date de première adhésion
				$adherent['firstDate']=plxUtils::getValue($values[$iTags['firstDate'][$i]]['value']);
				# Recuperation de la date de validation de l'adhésion
				$adherent['date']=plxUtils::getValue($values[$iTags['date'][$i]]['value']);
				# Recuperation de la chaine salt de l'adhérent
				$adherent['salt']=plxUtils::getValue($values[$iTags['salt'][$i]]['value']);
				# Recuperation du mot de passe cripté de l'adhérent
				$adherent['password']=plxUtils::getValue($values[$iTags['password'][$i]]['value']);
				# Recuperation de la clé
				$adherent['cle']=plxUtils::getValue($values[$iTags['cle'][$i]]['value']);
				# Recuperation des chaines aléatoires
				$adherent['rand1']=plxUtils::getValue($values[$iTags['rand1'][$i]]['value']);
				$adherent['rand2']=plxUtils::getValue($values[$iTags['rand2'][$i]]['value']);
				if ($this->getParam('typeAnnuaire') == 'professionnel') {
					# Recuperation de l'activité
					$adherent['activite']=isset($iTags['activite'][$i])?plxUtils::getValue($values[$iTags['activite'][$i]]['value']):'';
					# Recuperation de l'établissement
					$adherent['etablissement']=isset($iTags['etablissement'][$i])?plxUtils::getValue($values[$iTags['etablissement'][$i]]['value']):'';
					# Recuperation du service
					$adherent['service']=isset($iTags['service'][$i])?plxUtils::getValue($values[$iTags['service'][$i]]['value']):'';
					# Recuperation du poste
					$adherent['tel_office']=isset($iTags['tel_office'][$i])?plxUtils::getValue($values[$iTags['tel_office'][$i]]['value']):'';
				}
				if ($this->getParam('showAnnuaire') == 'on') {
					# Recuperation du choix sur le partage des coordonnées
					$adherent['coordonnees']=isset($iTags['coordonnees'][$i])?plxUtils::getValue($values[$iTags['coordonnees'][$i]]['value']):'refus';# rec public
				}
			}
		}
		return $adherent;
	}

	/**
	 * Méthode permettant de formater un numéro de téléphone au format français ou international
	 */
	public function formatFrenchPhoneNumber($phoneNumber, $international = FALSE){
		//Supprimer tous les caractères qui ne sont pas des chiffres
		$phoneNumber = preg_replace('/[^0-9]+/', '', $phoneNumber);
		//Garder les 9 derniers chiffres
		$phoneNumber = substr($phoneNumber, -9);
		//On ajoute +33 si la variable $international vaut TRUE et 0 dans tous les autres cas
		$motif = $international ? '+33 (\1) \2 \3 \4 \5' : '0\1 \2 \3 \4 \5';
		$phoneNumber = preg_replace('/(\d{1})(\d{2})(\d{2})(\d{2})(\d{2})/', $motif, $phoneNumber);

		return $phoneNumber;
	}

	/**
	 * Méthode qui retourne le prochain id d'un adhérent
	 *
	 * @return	string		id d'un nouvel adhérent sous la forme 0001
	 * @author	Stephane F.
	 **/
	public function nextIdAdherent() {
		# On récupère l'ensemble des adhérents
		//$this->adherentsList = $this->getAdherent(PLX_ROOT.$this->getParam('adherents').'plugin.adhesion.adherents.xml');
		# On récupère le dernier identifiant
		if($aKeys = array_keys($this->plxGlob_adherents->aFiles)) {
			rsort($aKeys);
			return str_pad($aKeys['0']+1,5, '0', STR_PAD_LEFT);
		} else {
			return '00001';
		}
	}

	/**
	 * Méthode permettant de supprimer les adhérents sélectionnés
	 *
	 * @param $content 	array tableau contenant les index des adhérents à supprimer
	 * @param $mail 	array tableau optionnel contenant les paramètres des mails à envoyer pour confirmation
	 *
	 * @return bool
	 * @author Cyril MAGUIRE, Thomas Ingles
	 */
	public function deleteAdherentsList(&$content,$mail=array(null)) {
		$action = FALSE;
		foreach($content['idAdherent'] as $k=>$id) {
			if(!preg_match('/^[0-9]{5}$/',$id)) return false;#Vérification de l'intégrité de l'identifiant
			$resDelAd = true;# Variable d'état
			if($globAd = $this->plxGlob_adherents->query('~^'.$id.'\.(.*).xml$~')) {# Suppression de l'adhérent
				unlink(PLX_ROOT.$this->getParam('adherents').'adhesions/'.$globAd['0']);
				$resDelAd = !file_exists(PLX_ROOT.$this->getParam('adherents').'adhesions/'.$globAd['0']);
				$_SESSION['info'] .= sprintf($this->getLang('L_ADMIN_REMOVE_ADH'),$content['nom_'.$id]).'<br/>';
				if(in_array($content['mail_'.$id],$this->listDiff)) {#On retire l'email de la liste de diffusion
					$this->removeAdressFromGutumaList($content['mail_'.$id],TRUE);
					$_SESSION['info'] .= sprintf($this->getLang('L_ADMIN_REMOVE_ADD'),$content['mail_'.$id],$content['nom_'.$id]).'<br/>' ;
				}
				eval($this->plxMotor->plxPlugins->callHook(__FUNCTION__.__CLASS__));#hook plugin
				foreach ($content as $d => $di){
					if(strstr($d,'_'.$id)) unset($content[$d]);# remove from content for non propagation in caller function
				}
			}
			$action = TRUE;
		}
		unset($content['idAdherent']);
		return $action;
	}

	/**
	 * Méthode permettant d'ajouter les mentions légales de la cnil aux mails envoyés avec le lien de suppression
	 *
	 * @param $id 		string index de l'adhérent
	 * @param $mail 	string mail de l'adhérent
	 * @param $text 	string mail au format texte ou html (html par défaut)
	 *
	 * @return string
	 * @author Cyril MAGUIRE, Thomas Ingles
	 *
	 */
	public function cnil($id=0,$mail='',$text=FALSE) {
		$this->plxMotor = plxMotor::getInstance();
		if ($id == 0 && $mail == '') {
			if ($text) {
				return $this->getLang('L_CNIL_NO_REPLY_TEXT')."\r\n\r\n" . $this->getParam('nom_asso') . ' : ' . $this->plxMotor->racine;
			} else {
				return '<p style="text-align:center;">'.str_replace(PHP_EOL.PHP_EOL,'<br/><hr/>',$this->getLang('L_CNIL_NO_REPLY_TEXT')).'</p><br/><hr/><br/><a href="' . $this->plxMotor->racine . '">' . $this->getParam('nom_asso') . '</a>';
			}
		} else {
			$urlParam = ($this->plxMotor->aConf['urlrewriting'])?'?':'&amp;';
			$cnillink = $this->plxMotor->urlRewrite('?adhesion').$urlParam.'q='.md5($id.'-'.$mail);
			if ($text) {
				return $this->getLang('L_CNIL_NO_REPLY_TEXT').PHP_EOL.$this->getLang('L_CNIL_TEXT').' : '.$cnillink."\r\n\r\n" . $this->getParam('nom_asso') . ' : ' . $this->plxMotor->racine;
			} else {
				return '<p style="text-align:center;">'.str_replace(PHP_EOL.PHP_EOL ,'<br/><hr/>',$this->getLang('L_CNIL_NO_REPLY_TEXT')).'<br/><a href="'.$cnillink.'">'.$this->getLang('L_CNIL_LINK').'</a></p><br/><hr/><br/><a href="' . $this->plxMotor->racine . '">' . $this->getParam('nom_asso') . '</a>';
			}
		}
	}

	/**
	 * Méthode permettant de comparer les données du lien cnil et le cas échéant de supprimer le courriel du compte associé des listes de diffusion
	 * todo : add salt & sha1?
	 * @param $value string 	$value = md5($id-$mail);
	 *
	 * @return bool
	 * @author Cyril MAGUIRE, Thomas Ingles
	 */
	public function compare($value) {
		$content['idAdherent'] = array();
		$mail = array();
		foreach ($this->plxRecord_adherents->result as $id => $array) {
			if ($value == md5($array['id'].'-'.$array['mail']) && $array['validation'] == '1') {
				$content['idAdherent'][] = $array['id'];
				foreach ($array as $key => $value) {
					$content[$key.'_'.$array['id']] = $value;
				}
				$content['mailing_'.$array['id']] = 'blacklist';
				$content['selection'][0] ='update';
				$this->editAdherentsList($content,$array['id'],TRUE);
				return TRUE;
			}
		}
		return FALSE;
	}

	/**
	 * Générateur de chaines de caractères aléatoire
	 *
	 * @param $nbLettres integer Nombre de caractères que la chaine contiendra
	 * @param $nbCaracteres integer Nombre de caracteres exotiques maximum que la chaine contiendra
	 * @param $caracteresSup array Caracteres exotiques que la chaine ne contiendra pas
	 * @param $voyellesSup array Voyelles que la chaine ne contiendra pas
	 * @param $consonnesSupp array Consonnes que la chaine ne contiendra pas
	 *
	 * @return string la chaine aléatoire
	 * @author Cyril MAGUIRE
	 */
	public function generateurMot($nbLettres = 8,$nbCaracteres = 4,$caracteresSup = array(),$nombresSup = array(),$voyellesSup = array(),$consonnesSupp = array()) {

		$mot = '';
		$choix = array('consonnes','voyelles','caracteres','nombres');
		$nombres = array('0','1','2','3','4','5','6','7','8','9');
		$voyelles = array('a','e','i','o','u','y');
		$consonnes = array('b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z');
		$caracteres = array('@','#','?','!','+','=','-','%','&','*');

		if (!empty($consonnesSupp)) {
			$consonnes = array_diff($consonnes,$consonnesSupp);
		}
		if (!empty($voyellesSup)) {
			$voyelles = array_diff($voyelles,$voyellesSup);
		}
		if (!empty($caracteresSup)) {
			$caracteres = array_diff($caracteres,$caracteresSup);
		}
		if (!empty($nombresSup)) {
			$nombres = array_diff($nombres,$nombresSup);
		}
		if (empty($consonnes)) {
			$consonnes = array('b');
		}
		if (empty($voyelles)) {
			$voyelles = $consonnes;
		}
		if (empty($nombres)) {
			$nombres = $consonnes;
		}
		if ($nbCaracteres == 0) {
			$caracteres = $consonnes;
		}

		$j = 0;
		for($i=0;$i<$nbLettres;$i++) {
			//choix aléatoire entre consonnes et voyelles
			$rand = array_rand($choix,1);
			if ($rand == 3) {
				$j++;
			}
			if ($nbCaracteres != 0 && $j == ($nbCaracteres-1)) {
				$caracteres = $consonnes;
			}
			$type = $choix[$rand];
			$tab = $$type;
			//on recherche l'index d'une lettre, au hasard dans le tableau choisi
			$lettre = array_rand($$type,1);
			//on recherche la dernière lettre du mot généré
			if (strlen($mot) > 0) {
				$derniereLettre = $mot[strlen($mot)-1];
			} else {
				$derniereLettre = '';
			}

			//si la lettre choisie est déjà à la fin du mot généré, on relance la boucle
			if ($tab[$lettre] == $derniereLettre || in_array($derniereLettre,$tab)) {
				$i--;
			} else {//sinon on l'ajoute au mot généré
				$maj = mt_rand(0,10);
				if ($maj<2) {
					$mot .= strtoupper($tab[$lettre]);	
				} else {
					$mot .= $tab[$lettre];	
				}
			}
		}
		return $mot;
	}

	public function generate_string(int $len = 38):string
 	{
	    if ($len < 5) $len = 38;
	    $letters = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-_'; 
	    srand((double) microtime() * 1000000); 
	    $string = '';
	    for ($i = 1; $i <= rand($len,64); $i++) { 
	       $q = rand(1,63);
	       $string = $string . $letters[$q]; 
	    }
	    return $string;
	}

	/**
	 * Méthode permettant de définir un mot de passe et une clé lors d'une nouvelle inscription validée
	 *
	 * @param $id 	string index de l'adhérent : type : 00001
	 *
	 * @return $password string mot de passe de l'adhérent en clair
	 * @author Cyril MAGUIRE, Thomas Ingles
	 */
	public function defPassword($id) {
		if (array_key_exists($id, $this->adherentsList)){
			$id = $this->adherentsList[$id];
		}else{
			$id = end($this->adherentsList) + 1;
		}
		if (empty($this->plxRecord_adherents->result[$id]['firstDate'])) {
			$this->plxRecord_adherents->result[$id]['firstDate'] = time();
		}
//crée
		$sel = $this->generate_string(10);
		$cle = $this->generate_string($this->getParam('cle'));
		//$max = strlen($this->plxRecord_adherents->result[$id]['mail']);

		// $at = explode('@',$this->plxRecord_adherents->result[$id]['mail']);
		// $max = strlen($at[0]);#only before @
		// $at = strlen($at[1])+1;

		// $min = $max > 10 ? 9 : 1;
		// $rand1 = mt_rand($min,$max) + $at;
		$rand1 = $this->generate_string(rand(6,10));
		$rand2 = $this->generate_string(6);
		// $txt = substr($this->plxRecord_adherents->result[$id]['mail'],0,-$rand1);
		// $txl = strlen($txt);
		// if($txl > 7){#text pass extract toolong
		// 	$rand1 += ($txl - mt_rand($txl-4, $txl-1));#adjust cut size
		// }
		// if($txl < 2){#text pass extract toolong
		// 	$rand1 += 3;#adjust cut size #3chars min
		// }

		// $rand2 = $this->generateurMot(3,1);
		// $password = $cle.'-'.substr($this->plxRecord_adherents->result[$id]['mail'],0,-$rand1).$rand2;//ici
//garde
		$password = 'TEMP_'.$cle.'-'.$rand1.$rand2;
		$this->plxRecord_adherents->result[$id]['salt'] = $sel;
		$this->plxRecord_adherents->result[$id]['password'] = $this->PW->create_hash($password);//hash
		$this->plxRecord_adherents->result[$id]['rand1'] = $rand1;
		$this->plxRecord_adherents->result[$id]['rand2'] = $rand2;
		$this->plxRecord_adherents->result[$id]['cle'] = $cle;
//donne le passe
		return $password;
	}

	/**
	 * Méthode qui retourne les informations $output en analysant
	 * le nom du fichier de l'adhérent $filename
	 *
	 * @param	filename	fichier de l'adhérent à traiter
	 * @return	array		information à récupérer
	 * @author	Stephane F
	 **/
	public function adInfoFromFilename($filename) {

		# On effectue notre capture d'informations
		if(preg_match('/([0-9]{5}).([a-z-]+).([a-z-]+).([0-9]{10}).xml$/',$filename,$capture)) {
			return array(
				'adId'		=> $capture[1],
				'nom'		=> $capture[2],
				'prenom'	=> $capture[3],
				'firstDate'	=> $capture[4]
			);
		}
	}

	/**
	 * Méthode permettant de mettre à jour un compte adhérent
	 *
	 * @param $content array tableau contenant les informations des comptes à mettre à jour
	 *
	 * @return $mail array tableau contenant les paramètres des mails à envoyer aux adhérents
	 * @author Cyril MAGUIRE, Thomas Ingles
	 */
	public function updateAdherentsList($content) {
  //var_dump(__CLASS__.'->'.__FUNCTION__.'()');$e = new \Exception;var_dump($e->getTraceAsString());
  //var_dump(__CLASS__.'->'.__FUNCTION__.'($content) ',$content);exit;
		$mail = array();
		foreach($content['idAdherent'] as $key => $id) {
			$ad = $this->adherentsList[$id];
			if (isset($content['choix_'.$id]) && $content['choix_'.$id] == 'stop') {//L'adhérent souhaite ne plus faire partie de l'association
				$this->plxRecord_adherents->result[$ad]['choix'] = $content['choix_'.$id];
				$mail[] = array(
					'name'=>$this->getParam('nom_asso'),
					'from'=>$this->getParam('email'),
					'to'=>$content['mail_'.$id],
					'subject'=>$this->getParam('devalidation_subject'),
					'body'=>'<p>'.$this->getLang('L_BJR_MSG').' '.$content['prenom_'.$id].' '.$content['nom_'.$id].'.<br/>'.$this->getParam('devalidation_msg').'</p>'.$this->cnil(),
					'contentType'=>'html',
					'cc'=>FALSE,
					'bcc'=>FALSE,
					'notification' => array(
						'adherent' => $this->plxRecord_adherents->result[$ad],
						'sujet' => $this->getLang('L_SUPPRESSION')
					)
				);
				$this->deleteAdherentsList($content,$mail);
				return FALSE;
			}
			elseif(isset($content['nom_'.$id]) && $content['nom_'.$id]!='') {

				if($this->isAlreadyExists($content,$id)){
					$_SESSION['error'] .= '<!-- '.__LINE__.' -->'.$this->getLang('L_ERR_USER_ALREADY_USED').'!<br/>'.$this->getLang('L_ERR_MAIL').',<br/>'.$this->getLang('L_ERR_NAME').' + '.$this->getLang('L_ERR_FIRST_NAME').' + '.$this->getLang('L_ERR_VILLE').'<br/>';
					return false;
				}

				if(isset($content['firstDate_'.$id])) {
					$this->plxRecord_adherents->result[$ad]['firstDate'] = $content['firstDate_'.$id];//date de la premiere cotis
				}
				if(isset($content['date_'.$id])) {
					$this->plxRecord_adherents->result[$ad]['date'] = $content['date_'.$id];//date de derniere cotis
				}

				$this->plxRecord_adherents->result[$ad]['nom'] = $content['nom_'.$id];
				$this->plxRecord_adherents->result[$ad]['prenom'] = $content['prenom_'.$id];
				$this->plxRecord_adherents->result[$ad]['adresse1'] = $content['adresse1_'.$id];
				$this->plxRecord_adherents->result[$ad]['adresse2'] = $content['adresse2_'.$id];
				$this->plxRecord_adherents->result[$ad]['cp'] = $content['cp_'.$id];
				$this->plxRecord_adherents->result[$ad]['ville'] = $content['ville_'.$id];
				$this->plxRecord_adherents->result[$ad]['tel'] = $this->formatFrenchPhoneNumber($content['tel_'.$id]);
/*
VAR_DUMP($id,$content,$this->plxRecord_adherents->result[$ad],$this->plxRecord_adherents->result[$ad]['mail'],$content['mail_'.$id],($this->plxRecord_adherents->result[$ad]['mail'] != $content['mail_'.$id]));
EXIT(__FILE__ . ' : ' . __LINE__);
*/
				if($content['validation_'.$id] == '0' AND in_array($content['mail_'.$id], $this->listDiff)){//remove mail IF in gutuma list unvalidateds
					if($this->removeAdressFromGutumaList($content['mail_'.$id],TRUE)) {
						$_SESSION['info'] .= '<!-- '.__LINE__.' -->'.sprintf($this->getLang('L_ADMIN_REMOVE_ADD'),$content['mail_'.$id],$content['nom_'.$id]).'<br/>';
					}
					$this->plxRecord_adherents->result[$ad]['mail'] = $content['mail_'.$id];
				}
				elseif($content['validation_'.$id] == '1'
					AND (
						( $newpass = ($this->plxRecord_adherents->result[$ad]['mail'] != $content['mail_'.$id]) )
							OR
						( $regenerepass = ($content['selection'][0]=='regenerepass') )//regenerepass
					)
				){# Si nouveau courriel | regen pass, si valide ;)
					if($newpass AND in_array($this->plxRecord_adherents->result[$ad]['mail'],$this->listDiff))#remove old mail IF in gutuma list validateds
						if($this->removeAdressFromGutumaList($this->plxRecord_adherents->result[$ad]['mail'],TRUE))
							$_SESSION['info'] .= '<!-- '.__LINE__.' -->'.sprintf($this->getLang('L_ADMIN_REMOVE_ADD'),$this->plxRecord_adherents->result[$ad]['mail'],$content['nom_'.$id]).'<br/>';
					$this->plxRecord_adherents->result[$ad]['mail'] = $content['mail_'.$id];# nouveau courriel pour
					$password = $this->defPassword($id);# nouveau mot de passe
					$this->plxRecord_adherents->result[$ad]['password'] = $this->PW->create_hash($password);# nouveau has
					$REG = isset($regenerepass) ? '_REG': '';#byAdmin
					#On envoi a l'adhérent
					$mail[] = array(
						'name'=>$this->getParam('nom_asso'),
						'from'=>$this->getParam('email'),
						'to'=>$this->plxRecord_adherents->result[$ad]['mail'],
						'subject'=>$this->getLang('L_NEW_MAIL_PASS_SUBJECT'.$REG),
						'body'=>'<p>'.$this->getLang('L_BJR_MSG').' '.$content['prenom_'.$id].' '.$content['nom_'.$id].'.<br/>'.$this->getLang('L_NEW_MAIL_PASS'.$REG).'&nbsp;:</p><p>&nbsp;</p><p><strong>'.$password.'</strong></p>'.$this->cnil($id,$content['mail_'.$id]),
						'contentType'=>'html',
						'cc'=>FALSE,
						'bcc'=>FALSE
					);
					$_SESSION['info'] .= '<!-- '.__LINE__.' -->'.sprintf($this->getLang('L_ADMIN_NEW_PASS'.$REG),'<i>'.$this->plxRecord_adherents->result[$ad]['mail'],$content['nom_'.$id]).'</i><br/>';
				}//else
				$this->plxRecord_adherents->result[$ad]['mail'] = $content['mail_'.$id];

				$this->plxRecord_adherents->result[$ad]['mailing'] = $content['mailing_'.$id];
				$this->plxRecord_adherents->result[$ad]['choix'] = $content['choix_'.$id];
				if ($this->getParam('typeAnnuaire') == 'professionnel') {
					if ($content['activite_'.$id] == 'autre') {
						$this->plxRecord_adherents->result[$ad]['activite'] = $content['activite_autre_'.$id]?$content['activite_autre_'.$id]:$this->getLang('L_ADMIN_NOT_DONE');
					} else {
						$this->plxRecord_adherents->result[$ad]['activite'] = $content['activite_'.$id];
					}
					$this->plxRecord_adherents->result[$ad]['etablissement'] = $content['etablissement_'.$id];
					$this->plxRecord_adherents->result[$ad]['service'] = $content['service_'.$id];
					$this->plxRecord_adherents->result[$ad]['tel_office'] = $this->formatFrenchPhoneNumber($content['tel_office_'.$id]);
				}
				if ($this->getParam('showAnnuaire') == 'on') {
					$this->plxRecord_adherents->result[$ad]['coordonnees'] = $content['coordonnees_'.$id];
				}

				//On retire l'email de la liste de diffusion si c'est le choix de l'adhérent et s'il est non validé
				if (in_array($content['mail_'.$id],$this->listDiff)
					AND ($content['mailing_'.$id] == 'blacklist' OR
						($content['validation_'.$id] == '0'
							AND $content['choix_'.$id] != 'renouveler'
							AND $content['selection'][0] == 'update'
						)
					)
				) {
					if($this->removeAdressFromGutumaList($content['mail_'.$id],TRUE))
						$_SESSION['info'] .= '<!-- '.__LINE__.' -->'.sprintf($this->getLang('L_ADMIN_REMOVE_ADD'),$content['mail_'.$id],$content['nom_'.$id]).'<br/>';//On notifie
				}
				elseif ($content['validation_'.$id] == '1'
					AND !in_array($content['mail_'.$id],$this->listDiff)
					AND $content['mailing_'.$id] != 'blacklist'
					AND ($content['selection'][0]=='update')
				) {
					if($this->addAdressInGutumaList($content['mail_'.$id],TRUE)) {
						$_SESSION['info'] .= '<!-- '.__LINE__.' -->'.sprintf($this->getLang('L_ADMIN_ADD_ADD'),$content['mail_'.$id],$content['nom_'.$id]).'<br/>';//On notifie
					}
				}

#				$_SESSION['info'] .= '#'.__CLASS__.' ('.@$content['selection'][0].') i'. $id .'  a'. $ad .' : '.$content['nom_'.$id].' '.$content['mail_'.$id].'<br/>';#debug
#				$this->plxRecord_adherents->result[$ad]['mailing'] = $content['mailing_'.$id];#Au K ou #old
#				$this->plxRecord_adherents->result[$ad]['mail'] = $content['mail_'.$id];#Au K ou #old

				if ($content['selection'][0]=='cotise' AND !$this->cotisationAJour($this->plxRecord_adherents->result[$ad]['date'],false)) {#a cotisé
					$this->plxRecord_adherents->result[$ad]['date'] = $content['date_'.$id];
					$mail[] = array(
						'name'=>$this->getParam('nom_asso'),
						'from'=>$this->getParam('email'),
						'to'=>$content['mail_'.$id],
						'subject'=>$this->getParam('cotis_subject'),
						'body'=>'<p>'.$this->getLang('L_BJR_MSG').' '.$content['prenom_'.$id].' '.$content['nom_'.$id].'.<br/>'.$this->getParam('cotis_msg').'</p>'.$this->cnil($id,$content['mail_'.$id]),
						'contentType'=>'html',
						'cc'=>FALSE,
						'bcc'=>FALSE,
						'notification' => array(
							'adherent' => $this->plxRecord_adherents->result[$ad],
							'sujet' => $this->getLang('L_MODIFICATION')
						)
					);
				}
				elseif ($content['selection'][0]=='regenerepass') {#v2.1.1 do nothing

				}
				elseif ($content['selection'][0]!='update') {
					if (defined('PLX_ADMIN') AND $this->plxRecord_adherents->result[$ad]['validation'] == '1' && $content['choix_'.$id] == 'renouveler') {
						$_SESSION['erase']['m'] = '<p id="password_error">'.$this->getLang('L_ERR_USER_ALREADY_VALID').'</p>';//On notifie (PUBLIC)
						$_SESSION['erase']['d'] = 'e';//display what (e = error)
						$_SESSION['info'] .= '#'.__CLASS__.' ('.@$content['selection'][0].') '. $this->getLang('L_ADMIN_ERR_USER_ALREADY_VALID').' : '.$content['nom_'.$id].' '.$content['mail_'.$id].'<br/>';//On notifie (ADMIN)
						//$this->plxRecord_adherents->result[$ad]['validation'] = 'adhesion';#auto set #todo to stop
						continue;
					}

					$this->plxRecord_adherents->result[$ad]['validation'] = $content['validation_'.$id];

					//Si l'inscription n'est pas validée
					//On supprime l'inscription de l'adhérent mais on conserve ses coordonnées
					if ($this->plxRecord_adherents->result[$ad]['validation'] == '0' && (/*$content['selection'][0] == 'devalidation' OR */$content['choix_'.$id] != 'renouveler')) {
						if (in_array($content['mail_'.$id],$this->listDiff)) {//On retire l'email de la liste de diffusion
							if($this->removeAdressFromGutumaList($content['mail_'.$id],TRUE))
								$_SESSION['info'] .= '<!-- '.__LINE__.' -->'.sprintf($this->getLang('L_ADMIN_REMOVE_ADD'),$content['mail_'.$id],$content['nom_'.$id]).'<br/>';
						}
						//On efface
						$this->plxRecord_adherents->result[$ad]['salt'] = '';//le contrôle de mot de passe
						$this->plxRecord_adherents->result[$ad]['password'] = '';//le mot de passe HASHÉ
						$this->plxRecord_adherents->result[$ad]['cle'] = '';//la clé
						$this->plxRecord_adherents->result[$ad]['rand1'] = '';//chaine aléatoire1
						$this->plxRecord_adherents->result[$ad]['rand2'] = '';//chaine aléatoire2
						$this->plxRecord_adherents->result[$ad]['date'] = time();//la date de la validation

						$mail[] = array(
							'name'=>$this->getParam('nom_asso'),
							'from'=>$this->getParam('email'),
							'to'=>$content['mail_'.$id],
							'subject'=>$this->getParam('devalidation_subject'),
							'body'=>'<p>'.$this->getLang('L_BJR_MSG').' '.$content['prenom_'.$id].' '.$content['nom_'.$id].'.<br/>'.$this->getParam('devalidation_msg').'</p>'.$this->cnil(),
							'contentType'=>'html',
							'cc'=>FALSE,
							'bcc'=>FALSE,
							'notification' => array(
								'adherent' => $this->plxRecord_adherents->result[$ad],
								'sujet' => $this->getLang('L_MODIFICATION')
							)
						);
						$_SESSION['info'] .= '<!-- '.__LINE__.' -->'.sprintf($this->getLang('L_ADMIN_DEVALIDATION_ADH'),$content['nom_'.$id]).'<br/>';//On notifie
					}

					//Si c'est une demande de renouvellement
					if ($this->plxRecord_adherents->result[$ad]['validation'] == '1' && $content['choix_'.$id] == 'renouveler') {
						$_SESSION['info'] .= '<!-- '.__LINE__.' -->'.sprintf($this->getLang('L_ADMIN_VALIDATION_ADH'),$content['nom_'.$id]).'<br/>';//On notifie
						//On efface
						$this->plxRecord_adherents->result[$ad]['salt'] = '';//le contrôle de mot de passe
						$this->plxRecord_adherents->result[$ad]['password'] = '';//le mot de passe
						$this->plxRecord_adherents->result[$ad]['cle'] = '';//la clé
						$this->plxRecord_adherents->result[$ad]['rand1'] = '';//chaine aléatoire1
						$this->plxRecord_adherents->result[$ad]['rand2'] = '';//chaine aléatoire2
						$this->plxRecord_adherents->result[$ad]['date'] = time();//la date de la validation

						$mail[] = array(
							'name'=>$this->getParam('nom_asso'),
							'from'=>$this->getParam('email'),
							'to'=>$content['mail_'.$id],
							'subject'=>$this->getParam('subject'),
							'body'=>'<p>'.$this->getLang('L_BJR_MSG').' '.$content['prenom_'.$id].' '.$content['nom_'.$id].'.<br/>'.$this->getParam('thankyou').'</p>'.$this->adresse().$this->cnil(),
							'contentType'=>'html',
							'cc'=>FALSE,
							'bcc'=>FALSE,
							'notification' => array(
								'adherent' => $this->plxRecord_adherents->result[$ad],
								'sujet' => $this->getLang('L_MODIFICATION')
							)
						);
						$_SESSION['info'] .= '<!-- '.__LINE__.' -->'.$this->getLang('L_FORM_NEW').'<br/>';//On notifie
					}

					//On valide l'inscription de l'adhérent
					if ($content['validation_'.$id] == '1') {
						if ($content['mailing_'.$id] == 'maillist') {//On ajoute l'email à l'ensemble des mails de la liste de diffusion
							if($this->addAdressInGutumaList($content['mail_'.$id],TRUE)) {
								$_SESSION['info'] .= '<!-- '.__LINE__.' -->'.sprintf($this->getLang('L_ADMIN_ADD_ADD'),$content['mail_'.$id],$content['nom_'.$id]).'<br/>';//On notifie
							}
						}

						$password = $this->defPassword($id);
						if (
						 empty($this->plxRecord_adherents->result[$id]['cle'])
						 || empty($this->plxRecord_adherents->result[$id]['mail'])
						 || empty($this->plxRecord_adherents->result[$id]['rand1'])
						 || empty($this->plxRecord_adherents->result[$id]['rand2'])
						) {
							$this->plxRecord_adherents->result[$ad]['password'] = $this->PW->create_hash($password);//Hash Pass
						}
						$this->plxRecord_adherents->result[$ad]['validation'] = strval($content['validation_'.$id]);
						$this->plxRecord_adherents->result[$ad]['date'] = time();//On ajoute la date de la validation
						if (empty($this->plxRecord_adherents->result[$ad]['firstDate'])) {
							$this->plxRecord_adherents->result[$ad]['firstDate'] = $this->plxRecord_adherents->result[$ad]['date'];
						}

						#on crée le mail d'accueil avec le mdp temporaire
						$logInBase = str_replace(array('-','_'),'',plxUtils::title2url(strtolower($content['nom_'.$id].$content['prenom_'.$id] )));
						$mail[] = array(
							'name'=>$this->getParam('nom_asso'),
							'from'=>$this->getParam('email'),
							'to'=>$content['mail_'.$id],
							'subject'=>$this->getParam('validation_subject'),
							'body'=>'<p>'.$this->getLang('L_BJR_MSG').' '.$content['prenom_'.$id].' '.$content['nom_'.$id].'.<br/>'.$this->getParam('validation_msg').'</p><p>'.$this->getLang('L_ADMIN_PASSWORD').'</p><p><strong>'.$password.'</strong></p>'.$this->cnil($id,$content['mail_'.$id]),
							'contentType'=>'html',
							'cc'=>FALSE,
							'bcc'=>FALSE,
							'notification' => array(
								'adherent' => $this->plxRecord_adherents->result[$ad],
								'sujet' => $this->getLang('L_VALIDATION')
							)
						);
						$_SESSION['info'] .= '<!-- '.__LINE__.' -->'.sprintf($this->getLang('L_ADMIN_VALIDATION_ADH'),$content['nom_'.$id]).'<br/>';//On notifie
					}#FI content['validation_'.$id] == '1'

				}#FI content['selection'][0]!='update'
				else {//update
					if (isset($this->plxRecord_adherents->result[$ad],$_SESSION['info']) && $this->plxRecord_adherents->result[$ad]['password'] != '') {
						$_SESSION['info'] .= '<!-- '.__LINE__.' -->'.sprintf($this->getLang('L_ADMIN_UPDATE_ADH'),$content['nom_'.$id]).'<br/>';//On notifie
						$this->plxRecord_adherents->result[$ad]['validation'] = $content['validation_'.$id];

						$mail = array(null);//On n'envoie pas de mail de confirmation
					} elseif (isset($content['validation_'.$id]) && $content['validation_'.$id] == '1') {
						$_SESSION['info'] .= '<!-- '.__LINE__.' -->'.sprintf($this->getLang('L_ADMIN_UPDATE_ADH'),$content['nom_'.$id]).' + '.$this->getLang('L_PASS_IS_SENDED').'<br/>';//On notifie
						$password = $this->defPassword($id);
						$this->plxRecord_adherents->result[$ad]['validation'] = $content['validation_'.$id];

						$mail[] = array(
							'name'=>$this->getParam('nom_asso'),
							'from'=>$this->getParam('email'),
							'to'=>$content['mail_'.$id],
							'subject'=>$this->getParam('validation_subject'),
							'body'=>'<p>'.$this->getLang('L_BJR_MSG').' '.$content['prenom_'.$id].' '.$content['nom_'.$id].'.<br/>'.$this->getParam('validation_msg').'</p><p>'.$this->getLang('L_YOUR PASS_IS').' :</p><p><strong>'.$password.'</strong></p>'.$this->cnil($id,$content['mail_'.$id]),
							'contentType'=>'html',
							'cc'=>FALSE,
							'bcc'=>FALSE,
							'notification' => array(
								'adherent' => $this->plxRecord_adherents->result[$ad],
								'sujet' => $this->getLang('L_MODIFICATION')
							)
						);
					}
				}#FI ELSE ($content['selection'][0]!='update') :: UPDATE
			}
		}#hcaerof
		return $mail;
	}

	/**
	 * Méthode qui insère un nouvel adhérent dans la liste des adhérents
	 *
	 * @param	content	tableau multidimensionnel du plugin adhesion
	 *
	 * @return	bool
	 * @author	MAGUIRE Cyril
	 **/
	public function insertNewAdherent($content) {
//var_dump(__CLASS__.'->'.__FUNCTION__.'($content)',$content);$e = new \Exception;var_dump($e->getTraceAsString());//EXIT;
		$mail = array();
		$id = $content['new'];
//~		 $this->getAdherents('/^[0-9]{5}.(.[a-z-]+){2}.[0-9]{10}.xml$/');
		$isAlreadyExists = $this->isAlreadyExists($content,$id);
		if ($isAlreadyExists) {#si dévalidé & déja ds la base
			if ($content['choix_'.$id] != 'adhesion') {
				foreach ($this->plxRecord_adherents->result as $key => $value) {
					if($value['mail'] == $content['mail_'.$id]) {
						$adherent = array(
							'nom_'.$key => $value['nom'],
							'prenom_'.$key => $value['prenom'],
							'adresse1_'.$key => $value['adresse1'],
							'adresse2_'.$key => $value['adresse2'],
							'cp_'.$key => $value['cp'],
							'ville_'.$key => $value['ville'],
							'tel_'.$key => $value['tel'],
							'mail_'.$key => $value['mail'],
							'choix_'.$key => $content['choix_'.$id],
							'mailing_'.$key => $content['mailing_'.$id],
							'validation_'.$key => $value['validation']
						);
						if ($this->getParam('typeAnnuaire') == 'professionnel') {
							if ($value['activite'] == 'autre') {
								$adherent['activite'] = $value['activite_autre'];
							} else {
								$adherent['activite'] = $value['activite'];
							}
							$adherent['etablissement'] = $value['etablissement'];
							$adherent['service'] = $value['service'];
							$adherent['tel_office'] = $value['tel_office'];
						}
						if ($this->getParam('showAnnuaire') == 'on') {
							$adherent['coordonnees'] = $content['coordonnees_'.$id];
						}
						$adherent['idAdherent'][] = $key;
					}
				}
				$mail = $this->updateAdherentslist($adherent);

				unset($content);//
			} else {
//    var_dump(__CLASS__.' '.__FUNCTION__.'',$this);exit(__LINE__);
				if ($this->plxMotor->mode == 'adhesion') {//static : fix mode
					$_SESSION['erase']['m'] = '<p id="password_error">'.$this->getLang('L_ERR_USER_ALREADY_USED').'</p>';
					$_SESSION['erase']['d'] = 'e';//display what
					header('Location:'.$this->plxMotor->urlRewrite('?'.$this->plxMotor->get));//adhesion.html
					exit;
				} else {
					$_SESSION['error'] = $this->getLang('L_ERR_USER_ALREADY_USED');
					header('Location:'.$this->plxMotor->urlRewrite('core/admin/plugin.php').'?p='.__CLASS__);
					exit;
				}

			}
		}#FI isAlreadyExists
		if(isset($content['nom_'.$id]) && $content['nom_'.$id] !='') {

			$ad = end($this->adherentsList)+1;

			$this->plxRecord_adherents->result[$ad]['nom'] = $content['nom_'.$id];
			$this->plxRecord_adherents->result[$ad]['prenom'] = $content['prenom_'.$id];
			$this->plxRecord_adherents->result[$ad]['adresse1'] = $content['adresse1_'.$id];
			$this->plxRecord_adherents->result[$ad]['adresse2'] = $content['adresse2_'.$id];
			$this->plxRecord_adherents->result[$ad]['cp'] = $content['cp_'.$id];
			$this->plxRecord_adherents->result[$ad]['ville'] = $content['ville_'.$id];
			$this->plxRecord_adherents->result[$ad]['tel'] = $this->formatFrenchPhoneNumber($content['tel_'.$id]);
			$this->plxRecord_adherents->result[$ad]['mail'] = $content['mail_'.$id];
			$this->plxRecord_adherents->result[$ad]['choix'] = $content['choix_'.$id];
			$this->plxRecord_adherents->result[$ad]['mailing'] = $content['mailing_'.$id];
			if ($this->getParam('typeAnnuaire') == 'professionnel') {
				if ($content['activite_'.$id] == 'autre') {
					$this->plxRecord_adherents->result[$ad]['activite'] = $content['activite_autre_'.$id];
				} else {
					$this->plxRecord_adherents->result[$ad]['activite'] = $content['activite_'.$id];
				}
				$this->plxRecord_adherents->result[$ad]['etablissement'] = $content['etablissement_'.$id];
				$this->plxRecord_adherents->result[$ad]['service'] = $content['service_'.$id];
				$this->plxRecord_adherents->result[$ad]['tel_office'] = $content['tel_office_'.$id];
			}
			if ($this->getParam('showAnnuaire') == 'on') {
				$this->plxRecord_adherents->result[$ad]['coordonnees'] = $content['coordonnees_'.$id];//fix:Fatal error: Unsupported operand types in adhesion/adhesion.php on line 1519 BUT provoc : Notice: Undefined index: coordonnees in adhesion/adhesion.php on line 1944
			}
			# Définition du mot de passe
			$password = $this->defPassword($id);
			$this->plxRecord_adherents->result[$ad]['password'] = $this->PW->create_hash($password);
			$this->plxRecord_adherents->result[$ad]['validation'] = '0';
			$this->plxRecord_adherents->result[$ad]['firstDate'] = '';
			$this->plxRecord_adherents->result[$ad]['date'] = '';
			$_SESSION['info'] .= sprintf($this->getLang('L_ADMIN_NEW_ADH'),$content['nom_'.$id]).'<br/>';
			$logInBase = str_replace(array('-','_'),'',plxUtils::title2url(strtolower($content['nom_'.$id].$content['prenom_'.$id] )));

			$mail[] = array(
				'name'=>$this->getParam('nom_asso'),
				'from'=>$this->getParam('email'),
				'to'=>$content['mail_'.$id],
				'subject'=>$this->getParam('subject'),
				'body'=>'<p>'.$this->getLang('L_BJR_MSG').' '.$content['prenom_'.$id].' '.$content['nom_'.$id].', '.$this->getLang('L_ADMIN_ID').'&nbsp;: '.$logInBase.'<br/><br/>'.$this->getParam('thankyou').'</p>'.$this->adresse().$this->cnil($id,$content['mail_'.$id]),
				'contentType'=>'html',
				'cc'=>FALSE,
				'bcc'=>FALSE
			);
		}
		return $mail;
	}

	/**
	 * Méthode qui écrit le fichier XML selon la liste des adhérents
	 * et qui notifie par mail l'adhérent (+ l'asso si choix stop)
	 *
	 * @param	$mail array tableau contenant les paramètres du mail à envoyer lors de la mise à jour du fichier
	 *
	 * @return	bool
	 * @author	MAGUIRE Cyril
	 **/
	public function recAdherentsList($mail,$id=false) {
//var_dump(__CLASS__.'->'.__FUNCTION__.'()');$e = new \Exception;var_dump($e->getTraceAsString());
//var_dump(__CLASS__.'->'.__FUNCTION__.'($mail,$id) ',$mail,$id);//exit;
			if ($id === false  OR !isset($this->adherentsList[$id])/**/) {
				$id = $this->nextIdAdherent();
				$ad = end($this->adherentsList)+1;
			} else {
				$ad = $this->adherentsList[$id];
			}
			$adherent = $this->plxRecord_adherents->result[$ad];
//var_dump(__CLASS__.'->'.__FUNCTION__.'('.__LINE__.')',$id,$ad,$adherent);//exit;
			# On génére le fichier XML
			$xml = "<?xml version=\"1.0\" encoding=\"".PLX_CHARSET."\"?>\n";
			$xml .= "<document>\n";
				$xml .= "\t<adherent number=\"".$id."\">\n\t\t";
				$xml .= "<nom><![CDATA[".plxUtils::cdataCheck($adherent['nom'])."]]></nom>\n\t\t";
				$xml .= "<prenom><![CDATA[".plxUtils::cdataCheck($adherent['prenom'])."]]></prenom>\n\t\t";
				$xml .= "<adresse1><![CDATA[".plxUtils::cdataCheck($adherent['adresse1'])."]]></adresse1>\n\t\t";
				$xml .= "<adresse2><![CDATA[".plxUtils::cdataCheck($adherent['adresse2'])."]]></adresse2>\n\t\t";
				$xml .= "<cp><![CDATA[".plxUtils::cdataCheck($adherent['cp'])."]]></cp>\n\t\t";
				$xml .= "<ville><![CDATA[".plxUtils::cdataCheck($adherent['ville'])."]]></ville>\n\t\t";
				$xml .= "<tel><![CDATA[".plxUtils::cdataCheck($adherent['tel'])."]]></tel>\n\t\t";
				$xml .= "<mail><![CDATA[".plxUtils::cdataCheck($adherent['mail'])."]]></mail>\n\t\t";

				$xml .= "<choix><![CDATA[".plxUtils::cdataCheck($adherent['choix'])."]]></choix>\n\t\t";
				$xml .= "<mailing><![CDATA[".plxUtils::cdataCheck($adherent['mailing'])."]]></mailing>\n\t\t";
				$xml .= "<salt><![CDATA[".plxUtils::cdataCheck($adherent['salt'])."]]></salt>\n\t\t";
				$xml .= "<password><![CDATA[".plxUtils::cdataCheck($adherent['password'])."]]></password>\n\t\t";
				$xml .= "<rand1><![CDATA[".plxUtils::cdataCheck($adherent['rand1'])."]]></rand1>\n\t\t";
				$xml .= "<rand2><![CDATA[".plxUtils::cdataCheck($adherent['rand2'])."]]></rand2>\n\t\t";
				$xml .= "<cle><![CDATA[".plxUtils::cdataCheck($adherent['cle'])."]]></cle>\n\t\t";
				$xml .=	"<validation>".plxUtils::cdataCheck($adherent['validation'])."</validation>\n\t\t";
				$xml .=	"<firstDate>".plxUtils::cdataCheck($adherent['firstDate'])."</firstDate>\n\t\t";
				$xml .=	"<date>".plxUtils::cdataCheck($adherent['date'])."</date>\n\t\t";
				if ($this->getParam('typeAnnuaire') == 'professionnel') {
					$xml .= "<activite><![CDATA[".plxUtils::cdataCheck($adherent['activite'])."]]></activite>\n\t\t";
					$xml .= "<etablissement><![CDATA[".plxUtils::cdataCheck($adherent['etablissement'])."]]></etablissement>\n\t\t";
					$xml .= "<service><![CDATA[".plxUtils::cdataCheck($adherent['service'])."]]></service>\n\t\t";
					$xml .= "<tel_office><![CDATA[".plxUtils::cdataCheck($adherent['tel_office'])."]]></tel_office>\n\t";
				}
				if ($this->getParam('showAnnuaire') == 'on') {
					$xml .= "\t<coordonnees><![CDATA[".plxUtils::cdataCheck($adherent['coordonnees'])."]]></coordonnees>\n\t";
				}
				$xml .= "</adherent>\n";
			$xml .= "</document>";

			$time = (empty($adherent['firstDate'])) ? time() : plxUtils::cdataCheck($adherent['firstDate']);

			if (isset($this->plxGlob_adherents->aFiles[$id]) && is_file(PLX_ROOT.$this->getParam('adherents').'adhesions/'.$this->plxGlob_adherents->aFiles[$id])) {
				$fileName = $this->plxGlob_adherents->aFiles[$id];
			} else {
				$fileName = $id.'.'.plxUtils::title2filename(plxUtils::cdataCheck($adherent['nom']).'.'.plxUtils::cdataCheck($adherent['prenom'])).'.'.time().'.xml';
			}
//var_dump(__CLASS__.'->'.__FUNCTION__.'('.__LINE__.')',$id,$ad,$fileName,$xml);exit;

			# On écrit le fichier et on notifie par mail l'adhérent (+ l'asso si stop)
			if(plxUtils::write($xml, PLX_ROOT.$this->getParam('adherents').'adhesions/'.$fileName)) {
				@chmod(PLX_ROOT.$this->getParam('adherents').'adhesions/'.$fileName.'.xml',0755);
				if ($mail != array(null) && $mail != NULL) {
					if (is_array($mail)) {
						$action = FALSE;
						foreach ($mail as $key => $m) {
							if (isset($m['notification'])) {#notif adherents
								if ($m['notification']['adherent']['validation'] == '0' || $m['notification']['adherent']['choix'] == 'stop') {
									$pro = array();
									if ($this->getParam('typeAnnuaire') == 'professionnel') {
										$pro['activite'] = $m['notification']['adherent']['activite'];
										$pro['etablissement'] = $m['notification']['adherent']['etablissement'];
										$pro['service'] = $m['notification']['adherent']['service'];
										$pro['tel_office'] = $m['notification']['adherent']['tel_office'];
										if ($this->getParam('showAnnuaire') == 'on') {
											$pro['coordonnees'] = $m['notification']['adherent']['coordonnees'];
										}
									}
									$body = $this->notification(
										$m['notification']['adherent']['nom'],
										$m['notification']['adherent']['prenom'],
										$m['notification']['adherent']['adresse1'],
										$m['notification']['adherent']['adresse2'],
										$m['notification']['adherent']['cp'],
										$m['notification']['adherent']['ville'],
										$m['notification']['adherent']['tel'],
										$m['notification']['adherent']['mail'],
										$m['notification']['adherent']['choix'],
										$m['notification']['adherent']['mailing'],
										$pro
									);
									$this->sendEmail($this->getParam('nom_asso'), $this->getParam('email'), $this->getParam('email'), $m['notification']['sujet'], $body, 'html');#to asso
								}#fi stop
							}#fi isset($m['notification'])
							if($this->sendEmail($m['name'], $m['from'], $m['to'], $m['subject'], $m['body'], $m['contentType'], $m['cc'], $m['bcc'])){#to adh
								$action = TRUE;
							}else{
								$action = FALSE;
							}
						}#hcaerof ($mail as $key => $m)
						return $action;
					} else {#$mail not array()
						if($this->sendEmail($mail['name'], $mail['from'], $mail['to'], $mail['subject'], $mail['body'], $mail['contentType'], $mail['cc'], $mail['bcc'])){#to adh
							return TRUE;
						}else{
							return FALSE;
						}
					}
				} else {#$mail == NULL
					return TRUE;
				}
			} else {//Impossible d'écrire le fichier
				return FALSE;
			}
	}
	/**
	 * Méthode qui édite le fichier XML du plugin adhesion selon le tableau $content
	 *
	 * @param	$content 	array	tableau multidimensionnel du plugin adhesion
	 * @param 	$id 		string 	index de l'adhérent
	 * @param	$action		bool 	permet de forcer la mise à jour du fichier
	 *
	 * @return	string
	 * @author	MAGUIRE Cyril
	 **/
	public function editAdherentslist($content, $id = 0, $action=FALSE) {
//var_dump(__CLASS__.'->'.__FUNCTION__.'($content, $id , $action)',$content, $id , $action);$e = new \Exception;var_dump($e->getTraceAsString());//EXIT;

		$error = FALSE;
		$success = FALSE;
		$_SESSION['info'] = '';
		$_SESSION['error'] = '';

		if ($id == 0 && empty($content['adherentNum'])) {
			$content['adherentNum'][] = $this->nextIdAdherent();
		}

		# On force la mise à jour
		if($action===TRUE){
			$mail = $this->updateAdherentsList($content);
			if (is_array($mail)) {
				$action = TRUE;
			} else {
				$action = FALSE;
			}
		}
		# suppression
		elseif(!empty($content['selection']) && $content['selection'][0]=='delete' && isset($content['idAdherent'])) {
			$this->deleteAdherentsList($content);
			$action = TRUE;
		}
		# mise à jour de la liste des adhérents
		elseif(isset($content['selection']) && isset($content['update']) && $content['update'] == true
			&& ($content['selection'][0]=='validation'
			|| $content['selection'][0]=='devalidation'
			|| $content['selection'][0]=='regenerepass'#2.1.1
			|| $content['selection'][0]=='cotise'
			|| $content['selection'][0]=='update'
			)
		){
			foreach ($content['idAdherent'] as $key => $value) {
				if ($content['selection'][0] == 'validation') {
					$content['validation_'.$value] = '1';
				}
				elseif ($content['selection'][0] == 'devalidation') {
					$content['validation_'.$value] = '0';
				}
				elseif ($content['selection'][0] == 'cotise') {
					$content['date_'.$value] = $content['date_'.$value] + (365*24*60*60);#adh date + 1 an
				}
			}
			$mail = $this->updateAdherentsList($content);
			if (is_array($mail)) {
				$action = TRUE;
			} else {
				$action = FALSE;
			}
		}
		# nouvel enregistrement dans la liste des adhérents depuis l'administration
		elseif( isset($content['new']) && $content['adherentNum'][0] == $content['new']
			&& ( isset($content['choix_'.$content['new']])
			&& $content['choix_'.$content['new']] == 'adhesion'
			)
		){
			$mail = $this->insertNewAdherent($content);
			if (is_array($mail)) {
				$action = TRUE;
				$content['idAdherent'][] = false;
			} else {
				$action = FALSE;
			}
		}
		# nouvel enregistrement dans la liste des adhérents depuis la partie publique
		elseif(isset($content['wall-e']) && empty($content['wall-e']) && $content['choix_'.$id] == 'adhesion'){
			$content['new'] = $id;
			$this->getAdherents('/^[0-9]{5}.(.[a-z-]+){2}.[0-9]{10}.xml$/');
			$mail = $this->insertNewAdherent($content);
			if (is_array($mail)) {
				$action = TRUE;
				$content['idAdherent'][] = false;
			} else {
				$action = FALSE;
			}
		}

		# sauvegarde
		if($action && isset($content['idAdherent'])) {
			foreach ($content['idAdherent'] as $key => $id) {
				$this->recAdherentsList($mail,$id);
			}
		}
		return $action;
	}

	/**
	 * Méthode qui vérifie si un nouvel enregistrement n'est pas déjà présent dans la liste des adhérents
	 *
	 * @param array $adherent tableau contenant les renseignements sur l'enregistrement qui doit être vérifié
	 * @param integer $id index de l'enregistrement en cours
	 *
	 * @return bool
	 * @author Cyril MAGUIRE, Thomas Ingles
	 */
	public function isAlreadyExists($adherent,$id) {
  //var_dump(__CLASS__.'->'.__FUNCTION__.'()');$e = new \Exception;var_dump($e->getTraceAsString());
//  var_dump(__CLASS__.'->'.__FUNCTION__.'($content) ',$this->plxRecord_adherents,$adherent,$id);exit;
		if (!$this->plxRecord_adherents) return FALSE;//Fix 0 adhérant (null) : 1ere inscription

//  var_dump(__CLASS__.'->'.__FUNCTION__.'($content) ',$this->plxRecord_adherents->result);exit;

		$verif = $this->plxRecord_adherents->result;

		if(count($verif) > 1){//warn invalid arg for foreach
			$search['nom'] = strtolower($adherent['nom_'.$id]);
			$search['prenom'] = strtolower($adherent['prenom_'.$id]);
			$search['ville'] = strtolower($adherent['ville_'.$id]);
			$search['mail'] = $adherent['mail_'.$id];
//  var_dump(__CLASS__.'->'.__FUNCTION__.'() ',$search);//exit;
			foreach ($verif as $index => $data) {
				if ($id == $data['id']) continue;
				if ($search['mail'] == $data['mail']) {

//  var_dump(__CLASS__.'->'.__FUNCTION__.'(MAIL) ',$search['mail'], $data['mail'],$search);exit;
					return TRUE;
				}
				if ($search['nom'] == strtolower($data['nom']) && $search['prenom'] == strtolower($data['prenom']) && $search['ville'] == strtolower($data['ville'])) {
//  var_dump(__CLASS__.'->'.__FUNCTION__.'(NAME) ',$search['nom'], $data['prenom'],$search);exit;
					return TRUE;
				}
			}
		}
//  var_dump(__CLASS__.'->'.__FUNCTION__.'(NO) ',$search);exit;
		return FALSE;
	}

	/**
	 * Méthode permettant d'envoyer le courriel de rappel de cotisation à un compte
	 *
	 * @param $email string  l'email du compte ou envoyer le rappel
	 * @param $num int  num du rappel
	 * @return booléen
	 * @author Thomas Ingles
	 */
	public function sendRappel($email,$num=1) {
		foreach ($this->plxRecord_adherents->result as $key => $compte) {
			if ($compte['mail'] == $email) {
				$m = array(
					'name'=>$this->getParam('nom_asso'),
					'from'=>$this->getParam('email'),
					'to'=>$email,
					'subject'=>$this->getParam('rappel_subject').' ('.$this->getLang('L_RAPPEL_'.$num).')',
					'body'=>'<p>'.$this->getParam('rappel_msg').'</p>'.$this->cnil($compte['id'],$email),
					'contentType'=>'html',
					'cc'=>FALSE,
					'bcc'=>FALSE
				);#print_r($m);exit();
				if($this->sendEmail($m['name'], $m['from'], $m['to'], $m['subject'], $m['body'], $m['contentType'], $m['cc'], $m['bcc'])){
					return TRUE;
				}else{
					return FALSE;
				}
			}
		}
		return FALSE;
	}

	/**
	 * Méthode permettant de retrouver le mot de passe associé à un compte
	 *
	 * @param $email string  l'email du compte dont il faut retrouver le mot de passe
	 *
	 * @return string la clé associée au compte
	 * @author Cyril MAGUIRE
	 */
	public function retrieveMyPass($email) {
		foreach ($this->plxRecord_adherents->result as $key => $compte) {
			if ($compte['mail'] == $email) {
				$id = $compte['id'];
				// $compte['password'] = $this->defPassword($compte['id']);
				$compte['regenpass'] = true;
				$this->editMyAccount($compte,$id);
				return true;
			}
		}
		return false;
	}

	/**
	 * Méthode permettant d'éditer les paramètres d'un compte adhérent
	 *
	 * @param $compte array tableau des paramètres du compte
	 * @param $id string index du compte dans la liste des adhérents
	 *
	 * @return bool
	 * @author Cyril MAGUIRE
	 */
	public function editMyAccount($compte,$ad) {
		$this->plxMotor = plxMotor::getInstance();
		$this->gutumaPlugin();

		$id = $this->adherentsList[$ad];

		$this->plxRecord_adherents->result[$id]['nom'] = $compte['nom'];
		$this->plxRecord_adherents->result[$id]['prenom'] = $compte['prenom'];
		$this->plxRecord_adherents->result[$id]['adresse1'] = $compte['adresse1'];
		$this->plxRecord_adherents->result[$id]['adresse2'] = $compte['adresse2'];
		$this->plxRecord_adherents->result[$id]['cp'] = $compte['cp'];
		$this->plxRecord_adherents->result[$id]['ville'] = $compte['ville'];
		$this->plxRecord_adherents->result[$id]['tel'] = $this->formatFrenchPhoneNumber($compte['tel']);
		if($this->plxRecord_adherents->result[$id]['mail'] != $compte['mail'] ) {//remove old mail in gutuma list
			#SEND MAIL with notification mail has changed
			$mail = array(
				'name'=>$this->getParam('nom_asso'),
				'from'=>$this->getParam('email'),
				'to'=>$compte['mail'], #FIX Notice: Undefined variable: content ::: OLD $content['mail_'.$id]
				'subject'=>$this->getLang('L_NEW_MAIL_SUBJECT'),
				'body'=>'<p>'.$this->getLang('L_NEW_MAIL').'&nbsp;:</p><p>&nbsp;</p>'.$this->cnil($id,$compte['mail']),
				'contentType'=>'html',
				'cc'=>FALSE,
				'bcc'=>FALSE
			);
			$this->sendEmail($mail['name'], $mail['from'], $mail['to'], $mail['subject'], $mail['body'], $mail['contentType'], $mail['cc'], $mail['bcc']);
			$this->removeAdressFromGutumaList($this->plxRecord_adherents->result[$id]['mail'],TRUE);
			$this->plxRecord_adherents->result[$id]['mail'] = $compte['mail'];
		} else {
			$this->plxRecord_adherents->result[$id]['mail'] = $compte['mail'];
		}
		if (isset($compte['regenpass']) && $compte['regenpass'] === true) {
			$rand = $this->generate_string($this->tokenLenght);
			$this->plxRecord_adherents->result[$id]['rand1'] = $rand;
			$linkToRegenPassword = $this->plxMotor->urlRewrite('changemypass').'?linkToken='.$rand;
			$message = $this->getLang('L_REGEN_PASS').'<br/><br/><a href="'.$linkToRegenPassword.'">'.$linkToRegenPassword.'</a>';
			#SEND MAIL with notification pass has changed
			$mail = array(
				'name'=>$this->getParam('nom_asso'),
				'from'=>$this->getParam('email'),
				'to'=>$compte['mail'], #FIX Notice: Undefined variable: content ::: OLD $content['mail_'.$id]
				'subject'=>$this->getLang('L_NEW_PASS_SUBJECT'),
				'body'=>'<p>'.$message.'</p><p>&nbsp;</p>'.$this->cnil($id,$compte['mail']),
				'contentType'=>'html',
				'cc'=>FALSE,
				'bcc'=>FALSE
			);
			$this->sendEmail($mail['name'], $mail['from'], $mail['to'], $mail['subject'], $mail['body'], $mail['contentType'], $mail['cc'], $mail['bcc']);

		}
		if ($compte['password'] != '' && !isset($compte['regenpass'])) {
			//vérification du mot de passe
			if (true === $this->PW->isPasswordStrong($compte['password'], $this->pwLen) ) {
				$password = $compte['password'];
				$compte['password'] = $this->PW->create_hash($password);
				$this->plxRecord_adherents->result[$id]['password'] = $compte['password'];
				$this->plxRecord_adherents->result[$id]['rand1'] = $compte['rand1'];
				$message = $this->getLang('L_NEW_PASS');
				#SEND MAIL with notification pass has changed
				$mail = array(
					'name'=>$this->getParam('nom_asso'),
					'from'=>$this->getParam('email'),
					'to'=>$compte['mail'], #FIX Notice: Undefined variable: content ::: OLD $content['mail_'.$id]
					'subject'=>$this->getLang('L_NEW_PASS_SUBJECT'),
					'body'=>'<p>'.$message.'</p><p>&nbsp;</p>'.$this->cnil($id,$compte['mail']),
					'contentType'=>'html',
					'cc'=>FALSE,
					'bcc'=>FALSE
				);
				$this->sendEmail($mail['name'], $mail['from'], $mail['to'], $mail['subject'], $mail['body'], $mail['contentType'], $mail['cc'], $mail['bcc']);
			} else {
				plxMsg::Error($this->getLang('L_PASSWORD_NOT_STRONG'));
				header('Location:'.$this->plxMotor->path_url);//.$this->plxMotor->racine.$this->plxMotor->get
				exit;
			}
		}
		$this->plxRecord_adherents->result[$id]['choix'] = $compte['choix'];
		$this->plxRecord_adherents->result[$id]['mailing'] = $compte['mailing'];
		if ($this->getParam('typeAnnuaire') == 'professionnel') {
			if ($compte['activite'] == 'autre') {
				$this->plxRecord_adherents->result[$id]['activite'] = $compte['activite_autre'];
			} else {
				$this->plxRecord_adherents->result[$id]['activite'] = $compte['activite'];
			}
			$this->plxRecord_adherents->result[$id]['etablissement'] = $compte['etablissement'];
			$this->plxRecord_adherents->result[$id]['service'] = $compte['service'];
			$this->plxRecord_adherents->result[$id]['tel_office'] = $compte['tel_office'];
		}
		if ($this->getParam('showAnnuaire') == 'on') {
			$this->plxRecord_adherents->result[$id]['coordonnees'] = $compte['coordonnees'];
		}
		#On ajoute l'email à l'ensemble des mails de la liste de diffusion
		if ($compte['mailing'] == 'maillist') {
			if($this->addAdressInGutumaList($compte['mail'],TRUE)){
				$_SESSION['erase']['m'] = sprintf($this->getLang('L_ADD_ADD'),$compte['mail']).'<br/>';//On notifie
				$_SESSION['erase']['d'] = 's';//success
			}
		}
		if ($compte['mailing'] == 'blacklist') {
			if($this->removeAdressFromGutumaList($compte['mail'],TRUE)){
				$_SESSION['erase']['m'] = sprintf($this->getLang('L_REMOVE_ADD'),$compte['mail']).'<br/>';//On notifie
				$_SESSION['erase']['d'] = 's';//success
			}
		}
		if (isset($compte['regenpass'])) {
			unset($compte['regenpass']);
		}
		if ($compte['choix'] == 'stop') {
			$content['idAdherent'] = array(0 => $ad);
			return $this->deleteAdherentsList($content);
		} else {
			return $this->recAdherentsList(false,$ad);
		}
	}
	///////////////////////////////////////////////////////////
	//
	// Méthodes permettant la gestion des connexions des adhérents
	//
	//////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////
	//
	// Pages dynamiques : catégories et articles
	//
	//////////////////////////////////////////////////////////

	/**
	 * Méthode permettant de récupérer et de vérifier le token d'une url
	 */
	public function getTokenLink()
	{
		if (isset($_SERVER['REQUEST_URI'])) {
			$token = explode('=',basename($_SERVER['REQUEST_URI']));
			if (isset($token[1]) ) {
				if (strlen($token[1]) == $this->tokenLenght) {
					return $token[1];
				}
			}
		}
		return '';
	}

	/**
	 * Méthode qui liste tous les hash des mots de passe des adhérents
	 *
	 * @return array
	 * @author Cyril MAGUIRE, Thomas Ingles
	 */
	public function getPasswords() {
		$pw = null;
		//print_r($this->adherentsList);
		if(empty($this->plxRecord_adherents)){
			$this->getAdherents('/^[0-9]{5}.(.[a-z-]+){2}.[0-9]{10}.xml$/');
		}

		foreach ($this->plxRecord_adherents->result as $key => $value) {
			$pw[] = array(
				'id' => $value['id'],#for hook plugin
				'nom' => $value['nom'],
				'prenom' => $value['prenom'],
				'cle' => $value['cle'],
				'mail' => $value['mail'],
				'salt' => $value['salt'],
				'pass' => $value['password'],
				'rand1' => $value['rand1'],
				'rand2' => $value['rand2']
			);
		}
		return $pw;
	}

	/**
	 * Méthode qui retourne si l'adhérent est actif (login)
	 *
	 * @return bool
	 * @author Thomas Ingles
	 */
	public function getValidation($login){
		if(!!$this->plxRecord_adherents)
			$this->getAdherents('/^[0-9]{5}.(.[a-z-]+){2}.[0-9]{10}.xml$/');
		foreach ($this->plxRecord_adherents->result as $k => $v)
			if($login == str_replace(array('-','_'),'',plxUtils::title2url(strtolower($v['nom'].$v['prenom'] ))))
				return $v['validation'];
	}

	/**
	 * Méthode qui vérifie si le mot de passe saisi par l'utilisateur est dans la liste des mots de passe
	 * @param $pw string Mot de passe saisi crypté en md5
	 * @param $login string identifiant, correspondant au nom collé au prénom en minuscules sans espace ni caractères accentués ou exotiques
	 *
	 * @return bool
	 * @author Cyril MAGUIRE
	 */
	public function verifPass($pw,$login){
//~		 if(isset($content['wall-e']) && empty($content['wall-e'])) {return FALSE;} && POST
		
		if(isset($_SESSION['maxtryAdh']) && $_SESSION['maxtryAdh'] >= 3) {
			$_SESSION['timeoutAdh'] = time() + (60*15);
		}
		$error = false;
		$listPass = $this->getPasswords();//print_r($listPass);

		foreach ($listPass as $k => $v) {
			//sha1($salt.md5($cle.'-'.substr($email,0,-$rand1).$rand2))
			$logInBase = str_replace(array('-','_'),'',plxUtils::title2url(strtolower($v['nom'].$v['prenom'] )));
			if (true === $this->PW->validate_password($pw,$v['pass']) && $login == $logInBase ) {
				$_SESSION['account'] = plxUtils::charAleatoire(5).md5($v['mail']).plxUtils::charAleatoire(3);
				$_SESSION['domainAd'] = $this->session_domain;
				$_SESSION['ValidAd'] = $this->getValidation($login);
				$_SESSION[__CLASS__] = $v['id'];//For hook plugins
				unset($_SESSION['maxtryAdh'],$_SESSION['timeoutAdh']);
				return true;
			}
			if ( (false === $this->PW->validate_password($pw,$v['pass']) && $login == $logInBase) || (true === $this->PW->validate_password($pw,$v['pass']) && $login != $logInBase) ) {
				$error = true;
			}
		}
		
		if ($error) {
			if(!isset($_SESSION['maxtryAdh'])) {
				$_SESSION['maxtryAdh'] = 1;
			} else{
				$_SESSION['maxtryAdh']++;
			}
		}
		if(!isset($_SESSION['maxtryAdh'])) {
			$_SESSION['maxtryAdh'] = 1;
		} else{
			$_SESSION['maxtryAdh']++;
		}
		return FALSE;
	}

	/**
	 * Méthode qui ajoute le champs 'mot de passe' dans les options des catégories
	 *
	 * @return	stdio
	 * @author	Rockyhorror
	 **/
	public function AdminCategory() {
		echo '<?php
				$password = plxUtils::strCheck($plxAdmin->aCats[$id]["password"]);
				$image = \'&nbsp;<img src="'.PLX_PLUGINS.__CLASS__.'/locker-cat.png" alt="" />\';
			?>';
		echo '<p id="'.__CLASS__.'_password"><label for="id_password">'.$this->getlang('L_CATEGORIE_PASSWORD_FIELD_LABEL').'&nbsp;: <?php if($password == "on") echo $image ?></label>';
		echo '<?php plxUtils::printSelect("password",array(""=>L_NO,"on"=>L_YES),$password); ?></p>';
	}
	public function plxAdminEditCategoriesUpdate(){
		echo '<?php $this->aCats[$cat_id]["password"]=(isset($this->aCats[$cat_id]["password"]) && $this->aCats[$cat_id]["password"] == "on" ? "on" :"") ?>';
	}
	public function plxAdminEditCategoriesNew() {#add new cat pass in article.php
		echo '<?php $this->aCats[$cat_id]["password"] = ""; ?>';
	}
	public function plxAdminEditCategoriesXml() {
		echo '<?php $xml .= "<password><![CDATA[".plxUtils::cdataCheck($cat["password"])."]]></password>"; ?>';
	}
	public function plxAdminEditCategorie() {
		echo '<?php $this->aCats[$content["id"]]["password"] = trim($content["password"]); ?>';
	}
	public function plxMotorGetCategories() {
		echo '<?php $this->aCats[$number]["password"] = isset($iTags["password"][$i]) ? plxUtils::getValue($values[$iTags["password"][$i]]["value"]) : ""; ?>';
	}

	/**
	 * Méthode qui permet de démarrer la bufferisation de sortie sur la page admin/categories.php
	 *
	 * @return	stdio
	 * @author	Stephane F
	 **/
	public function AdminCategoriesTop() {
		echo '<?php ob_start(); ?>';
	}

	/**
	 * Méthode qui affiche l'image du cadenas si la catégorie est protégée par un mot de passe
	 *
	 * @return	stdio
	 * @author	Stephane F, Thomas Ingles
	 **/
	public function AdminCategoriesFoot() {
		echo '<?php '; ?>
		$content=ob_get_clean();
		if(preg_match_all('#idCategory\[\]" value="([0-9]{3})"#', $content, $capture)) {//Multi
			$image = '<img src="<?php echo PLX_PLUGINS.__CLASS__ ?>/locker-cat.png" title="<?php $this->lang('L_CAT_PROTECTED') ?>" />';
			foreach($capture[1] as $idCat) {
				//~ $str = '<td>'.L_CATEGORY.' '.$idCat;// <= 5.3.1
				$str = '</td><td>'.$idCat;// >= 5.4
				if(isset($plxAdmin->aCats[$idCat]['password']) AND $plxAdmin->aCats[$idCat]['password'] == 'on') {
					$content = str_replace($str, $str.' '.$image, $content);
				}
			}
		}
		echo $content;
?><?php
	}

	/**
	 * Méthode qui permet de démarrer la bufferisation de sortie sur la page admin/article.php
	 *
	 * @return	stdio
	 * @author	Stephane F, Thomas Ingles
	 **/
	public function AdminArticleTop() {
		echo '<?php ob_start(); ?>';
	}

	/**
	 * Méthode qui affiche une icone aux catégories dédiées aux adhérents (sidebar)
	 *
	 * @return	stdio
	 * @author	Stephane F, Thomas Ingles
	 **/
	public function AdminArticleFoot() {
		echo '<?php '; ?>
		$content=ob_get_clean();
		if(preg_match_all('#label for="cat_([0-9]{3})"#', $content, $capture)) {
			$image = '&nbsp;<img src="<?php echo PLX_PLUGINS.__CLASS__ ?>/locker-cat.png" title="<?php $this->lang('L_CAT_PROTECTED') ?>" />';
			foreach($capture[1] as $idCat) {
//~ 				$str = '<label for="cat_'.$idCat.'">';
				$str = 'catId[]" value="'.$idCat.'" />';
				if(isset($plxAdmin->aCats[$idCat]['password']) AND $plxAdmin->aCats[$idCat]['password'] == 'on') {
					$content = str_replace($str, $str.$image, $content);
				}
			}
		}
		echo $content;
?><?php
	}

	/**
	 * Méthode qui ajoute le champ 'mot de passe' dans l'édition de l'article
	 *
	 * @return	stdio
	 * @author	Rockyhorror
	 **/
	public function AdminArticleSidebar(){
		echo '<?php $image = \'&nbsp;<img src="'.PLX_PLUGINS.__CLASS__ .'/locker.png" alt="" />\'; ?>';
		echo '<p id="'.__CLASS__.'_password"><label for="id_password">'.$this->getlang('L_ARTICLE_PASSWORD_FIELD_LABEL').'&nbsp;: <?php if($password == "on") echo $image ?></label>';
		echo '<?php plxUtils::printSelect("password",array(""=>L_NO,"on"=>L_YES),$password); ?></p>';
	}
	public function plxAdminEditArticleXml(){
		echo '<?php $xml .= "\t"."<password><![CDATA[".plxUtils::cdataCheck(trim($content["password"]))."]]></password>"."\n"; ?>';
	}
	public function plxMotorParseArticle(){
		echo '<?php $art["password"] = (isset($iTags["password"]) && isset($values[ $iTags["password"][0] ]) )?trim(plxUtils::getValue($values[ $iTags["password"][0] ]["value"]) ):""; ?>';
	}
	public function AdminArticlePostData(){//Nvelle cat article.php
		echo '<?php $password = $_POST["password"]; ?>';
	}
	public function AdminArticleContent(){//affiche l'icone si l'article est privé (cat ou lui même)
		echo '<?php '; ?>
			$selCatPass = '';
			$image = ($password?'<sup><img class="float-left" src="<?php echo PLX_PLUGINS.__CLASS__ ?>/locker.png" title="<?php $this->lang('L_ART_PROTECTED') ?>"><?php $this->lang('L_ART_PROTECTED') ?></sup>':'');
			$imagecat = '<sup><img class="float-left" src="<?php echo PLX_PLUGINS.__CLASS__ ?>/locker-cat.png" title="<?php $this->lang('L_CAT_PROTECTED') ?>"><?php $this->lang('L_CAT_PROTECTED')?></sup>';
			foreach($plxAdmin->aCats as $cat_id => $cat) {
				if($cat['password']){
					$selCatPass = (is_array($catId) AND in_array($cat_id, $catId)) ? ($password?'<br/>':'').$imagecat : '';
					if($selCatPass) break;//locked (4 no lost if pass next but unselected)
				}
			}
			if($password OR $selCatPass) echo '<div class="float-right alert green"><sup class="float-center"><?php $this->lang('L_ADHESIONS') ?> : <?php echo $this->getParam('mnuMembers') ?></sup><img class="float-none" src="<?php echo PLX_PLUGINS.__CLASS__ ?>/icon.png" title="<?php $this->lang('L_ADHESIONS') ?>"><br/>'.$image.$selCatPass.'</div>';
?><?php
	}
	public function AdminArticleInitData(){
		echo '<?php $password = ""; ?>';
	}
	public function AdminArticleParseData(){
		echo '<?php $password = $result["password"]; ?>';
	}
	/**
	 * Méthode qui affiche un cadenas si l'article a un mot de passe ::: est inutilisé
	 *
	 * @return	stdio
	 * @author	Rockyhorror
	 **/
	public function showIconIfLock() {
		$artPassword = $this->plxMotor->plxRecord_arts->f('password');
		if(!empty($artPassword)){
			$string = '<?php echo "<img src=\"'.PLX_PLUGINS.__CLASS__.'/locker.png\">"; ?>';
			echo $string;
		}
	}

	/**
	 * Méthode qui masque les commentaires
	 * Note : in hooks plx(Motor|Feed)PreChauffageEnd()
	 * @author Cyril MAGUIRE, Thomas Ingles
	 */
	private function hideComs() {
		// echo '<?php '; ? >
#strpos($this->mode,'_password') === FALSE en loupe if $this->mode != '###_password' ::: article commentaire & tag
		if(isset($_SESSION['lockArticles']))//si connecté : ds feed, tjrs actif :: $_SESSION['lockArticles'] non activé
			return;
#Coms All verif
			foreach($this->plxMotor->plxGlob_coms->aFiles as $key => $comFilename) {
				$fileInfo = $this->plxMotor->comInfoFromFilename($comFilename);
				$artInfo = $this->plxMotor->artInfoFromFilename($this->plxMotor->plxGlob_arts->aFiles[$fileInfo['artId']]);
				$cats = explode(',',$artInfo['catId']);
				foreach($cats AS $cat){
					if($cat=='draft' OR $cat=='000' OR $cat=='home')continue;// Fix: Notice: Undefined index : Catégorie virtuelles et brouillon (non gérable par le plugin)
					if(!empty($this->aCats[$cat]['password'])) {
						unset ($this->plxMotor->plxGlob_coms->aFiles[$key]);
						continue(1);
					}
				}
#Art solo verif
				$root = PLX_ROOT.$this->aConf["racine_articles"];//ok multilingue
				$art = $this->parseArticle($root.$this->plxMotor->plxGlob_arts->aFiles[$fileInfo['artId']]);
				if(isset($art['password']) AND !empty($art['password']))
					unset ($this->plxMotor->plxGlob_coms->aFiles[$key]);
			}
// ? ><?php
	}

	/**#unused! Improve & HideStat
	 * Méthode qui masque les articles d'une catégorie
	 *
	 * @author Cyril MAGUIRE
	 */
	private function hideArts($catId='') {#unused? OUI
		foreach($this->plxMotor->plxGlob_arts->aFiles as $key => $artFilename){
			$fileInfo = $this->plxMotor->artInfoFromFilename($artFilename);
			$catPassword = $this->plxMotor->aCats[$fileInfo['catId']]['password'];
			if(!empty($catPassword)) {
//				if(($fileInfo['catId'] != $catId) &&  !isset($_SESSION['lockArticles']['categorie'][$fileInfo['catId']])){
				if(($fileInfo['catId'] != $catId) &&  !isset($_SESSION['lockArticles']['categorie'])){
					unset ($this->plxMotor->plxGlob_arts->aFiles[$key]);
				}
//				elseif (!isset($_SESSION['lockArticles']['categorie'][$fileInfo['catId']])) {
				elseif (!isset($_SESSION['lockArticles']['categorie'])) {
					unset ($this->plxMotor->plxGlob_arts->aFiles[$key]);
				}
			}
		}
	}

	/**
	 * Méthode qui redefinit les modes de plxMotor
	 * @scope : art,home,cat,tag&arch
	 * @return	stdio
	 * @author	Cyril MAGUIRE, Thomas Ingles
	 **/
	public function plxMotorPreChauffageEnd() {
		$this->plxMotor = plxMotor::getInstance();
		$this->hideComs();
		if($this->plxMotor->mode=='article') {
#			ob_start();echo $this->plxMotor->plxRecord_arts->f('password');$password = ob_get_clean();#ok::Origin
#			$password = plxUtils::getValue($this->plxMotor->plxRecord_arts->f('password'));//Strict standards: Only variables should be passed by reference
			$password = $this->plxMotor->plxRecord_arts->f('password');#2.0.0 (intest)
			if(empty($password)) {#cat verif if artpass not
				$cat_id = explode(',',$this->plxMotor->plxRecord_arts->f('categorie'));
				foreach ($cat_id as $key => $value) {
					if (!empty($this->plxMotor->aCats[$value]['password'])) {
						$password = plxUtils::getValue($this->plxMotor->aCats[$value]['password']);
						break;
					}
				}
			}
			if(!empty($password)) {
				if(!isset($_SESSION['lockArticles']['articles'])) {
					$this->plxMotor->mode = 'article_password';
				}
			}
			else {
				$cat_id = $this->plxMotor->plxRecord_arts->f('categorie');
				if(!empty($this->plxMotor->aCats[$cat_id]['password'])) {
					if(!isset($_SESSION['lockArticles']['categorie'])) {
						$this->plxMotor->mode = 'categorie_password';
					}
				}
			}
		}
#		elseif($this->plxMotor->mode == 'categorie') {}
		elseif($this->plxMotor->mode == 'archives' OR $this->plxMotor->mode == 'tags') {
			$this->plxMotor->getArticles();
			if($this->plxMotor->plxRecord_arts){//have arts?
				foreach ($this->plxMotor->plxRecord_arts->result as $key => $art) {
					$cat_id = explode(',',$art['categorie']);
					foreach ($cat_id as $key => $value) {
						if(!empty($this->plxMotor->aCats[$value]['password']) && !isset($_SESSION['lockArticles']['categorie'])) {
#							$this->plxMotor->mode = 'home';#remove tag in link url feed
#							$this->plxMotor->mode = 'tags_password';#Fix link url feed
							$this->plxMotor->mode = $this->plxMotor->mode . '_password';#Fix link url feed
							return;#stop loop #Fix (tag|archives)_password_password
						}
					}
				}
			}
		}
	}

	/**
	 * Méthode qui permet la (dé)conexion en empechant de poster un commentaire (en mode article)
	 *# Hook plugins
	 * @return	stdio
	 * @author	Thomas Ingles
	 **/
	public function plxMotorDemarrageBegin() {
		if(isset($_POST['logout']) OR isset($_POST['login']) OR isset($_POST['password']))//(Dé)Connexion
			echo '<?php $this->aConf["allow_com"] = 0; ?>';//empeche de commenter dans le vide lors de cnx & décnx [$_post] v5.6
	}

	/**
	 * Méthode qui affiche le formulaire de saisie du mot de passe
	 *
	 * @return	stdio
	 * @author	Cyril MAGUIRE, Thomas Ingles
	 **/
	public function plxMotorDemarrageEnd() {

		if (isset($_SESSION['pw']) && $_SESSION['pw'] == true && $this->plxMotor->mode != 'myaccount') {
			$_SESSION['lockArticles']['error'] = $this->getlang('L_CHANGE_YOUR_PASS');
			$_SESSION['pw'] = true;
			$loca = $this->plxMotor->urlRewrite('?myaccount.html');#v2.2.0
			header('Location:'.$loca);
			exit;
		}
		if(empty($this->plxMotor))
			$this->plxMotor = plxMotor::getInstance();
		if(isset($_POST['forgetmypass'])//Mot de passe oublié, (retrieveMyPass)
		AND (isset($_POST['wall-e']) AND empty($_POST['wall-e']))) {//#wall-e == public + wall-e no-checked == humain 
			$error = '';
			if(!plxUtils::checkMail($_POST['email']))
				$error .= $this->getLang('L_ERR_MAIL').'! ';
			if (isset($this->plxMotor->plxPlugins->aPlugins['plxCapchaImage']))//si capchaImage
				$_SESSION['capcha']=sha1($_SESSION['capcha']);
			if($this->plxMotor->aConf['capcha'] AND $_SESSION['capcha'] != sha1($_POST['rep']))
				$error .= $this->getLang('L_ERR_ANTISPAM').'! ';
			$noerror = empty($error);
			if($noerror AND $this->retrieveMyPass(plxUtils::strCheck($_POST['email']))) {//si ok : on renvoie la clé si l'email correspond
				$_SESSION['retrievePass']['m'] = '<p id="password_success">'.$this->getLang('L_EMAIL_PASS_OK').'</p>';
				$_SESSION['retrievePass']['d'] = 's';
				header('Location:'.$this->plxMotor->urlRewrite() );//retour a l'accueil
				exit;
			} else {
				$_SESSION['retrievePass']['m'] = '<p id="password_error">'.(!$noerror?$error:$this->getLang('L_EMAIL_PASS_KO')).'</p>';
				$_SESSION['retrievePass']['d'] = 'e';
				header('Location:'.$this->plxMotor->urlRewrite('?forgetmypass.html') );
				exit;
			}
		}

		if(isset($_POST['logout'])) {//Déconnexion
			//On supprime les index de session
			unset($_SESSION['lockArticles'], $_SESSION['adhesion'], $_SESSION['account'], $_SESSION['domainAd'], $_SESSION['maxtryAdh'], $_SESSION['timeoutAdh']);
			$_SESSION['logout'] = $this->getlang('L_DECONNEXION_OK');
			header('Location: '.$this->plxMotor->racine.$this->plxMotor->path_url);
			exit;
		}

		$this->session_domain = str_replace(PLX_PLUGINS.__CLASS__,'',dirname(__FILE__));//Définition du domaine initial
		//Si le mode est protégé, on affiche le message de connexion, sinon on affiche la page normalement
		switch ($this->plxMotor->mode) {
			case 'static_password':
			case 'article_password':
			case 'categorie_password':
			//~ case 'categories_password':
			case 'annuaire':
				$showForm = TRUE;
			break;
			case 'home':
			case 'categorie':
			case 'tags_password':#exist? UTILE?
			case 'archives_password':
				$this->plxMotor->mode = str_replace('_password','',$this->plxMotor->mode);
				$protectedCats = array();
				foreach($this->plxMotor->plxGlob_arts->aFiles as $key => $artFilename){
					$fileInfo = $this->plxMotor->artInfoFromFilename($artFilename);
					$cats = explode(',', $fileInfo['catId']);
					$catPassword = '';
					foreach ($cats as $k => $value) {
						if ($value>0 && isset($this->plxMotor->aCats[$value]) && $this->plxMotor->aCats[$value]['password'] == 'on') {//$value>0  solve Notice: Undefined index: 000 (home exist in recent version?)
							$catPassword = $this->plxMotor->aCats[$value]['password'];
							if($catPassword == 'on') {
								$protectedCats[] = $fileInfo['catId'];
							}
							$catPassword = '';
						}
					}
				}

				if (!isset($_SESSION['lockArticles']['categorie'])) {
					$htm = '';
					$strCut = $this->getParam('strcutLocks');
					foreach ($this->plxMotor->plxRecord_arts->result as $k => $data) {
						if ( ($data['password'] == 'on' ) OR in_array($data['categorie'],$protectedCats) ) {//thumbnail
							if($strCut){#On tronque le html de l'article #tep
								$htm = $this->cutContent($this->plxMotor->plxRecord_arts->result[$k]['chapo'].PHP_EOL.$this->plxMotor->plxRecord_arts->result[$k]['content']);
							}
							$this->plxMotor->plxRecord_arts->result[$k]['chapo'] = '';
							$this->plxMotor->plxRecord_arts->result[$k]['nb_com'] = 0;
							$this->plxMotor->plxRecord_arts->result[$k]['allow_com'] = 0;
							$this->plxMotor->plxRecord_arts->result[$k]['thumbnail'] = '';
							$this->plxMotor->plxRecord_arts->result[$k]['content'] = $htm . '<p class="locked">'.sprintf($this->getLang('L_NEED_AUTH'),$this->plxMotor->urlRewrite('?adhesion.html'),$this->getParam('mnuAdhesion')).'</p>'.$this->finclude(PLX_PLUGINS.__CLASS__.'/form.login.inc.php',false,'aside');//categorie
						}
					}#end foreach : this->plxMotor->plxRecord_arts->result
					unset($htm);
				}
				$showForm = FALSE;
				break;
			default:
				$showForm = FALSE;
		}//FI SWITCH motor mode

		if(isset($_POST['lockArticles']) AND isset($_POST['password']) AND isset($_POST['login'])//Vérification de la connexion
		AND (isset($_POST['wall-e']) AND empty($_POST['wall-e']))) {//no wall-e
			if($this->verifPass($_POST['password'],plxUtils::strCheck($_POST['login']))) {
				$_SESSION['lockArticles']['articles'] = $_SESSION['lockArticles']['categorie'] = 'on';
				$_SESSION['lockArticles']['success'] = $this->getlang('L_CONNEXION_OK');

				$showForm = FALSE;
				$strCut = $this->getParam('strcutLocks');
				$url = $this->plxMotor->racine.$this->plxMotor->path_url;//$this->plxMotor->urlRewrite('?'.$this->plxMotor->mode);//

				if(isset($_SESSION['adhesion_redirect']) AND strpos($_SESSION['adhesion_redirect'], 'ttp') !== FALSE){#v2.2.1
					$redir = $_SESSION['adhesion_redirect'];
					plxUtils::checkSite($redir);#verif une pages préenregisté par un autre plugin
					unset($_SESSION['adhesion_redirect']);
					$url = !empty($redir)? $redir: $url;
				}
				if (substr($_POST['password'],0,5) == 'TEMP_') {
					$_SESSION['lockArticles']['success'] .= '. '.$this->getlang('L_CHANGE_YOUR_PASS');
					$_SESSION['pw'] = true;
					$loca = $this->plxMotor->urlRewrite('?myaccount.html');#v2.2.0
					header('Location:'.$loca);
					exit;
				}
				switch ($this->plxMotor->mode) {
#					case 'article_password':
#					case 'categorie_password':
#					case 'categories_password':
#					case 'static_password':
#					case 'tags_password':
					case 'annuaire':
					case 'adhesion':#subscribe form
					case 'login-page':#v2.2 redirect on logon #TODO #PARAM ?
					case __CLASS__:
						#on redirige (vers mon compte si l'annuaire est désactivé)
						$loca = $this->plxMotor->urlRewrite('?'.($this->getParam('showAnnuaire')!='on'?'myaccount':'annuaire').'.html');#v2.2.0
						if($redir){#v2.2.1
							$loca = $redir;
						}
						header('Location:'.$loca);
						exit;
					case 'categorie':
					case 'home':
#					case 'tags':
						$htm = '';
						foreach ($this->plxMotor->plxRecord_arts->result as $key => $value) {
							if(isset($value['password']) && $value['password'] == 'on' && !isset($_SESSION['lockArticles']['categorie']) && !isset($_SESSION['lockArticles']['articles']) ) {
								if($strCut){#On tronque le html de l'article #tep
									$htm = $this->cutContent($this->plxMotor->plxRecord_arts->result[$key]['chapo'].PHP_EOL.$this->plxMotor->plxRecord_arts->result[$key]['content']);
								}
								//On modifie le contenu de l'article
								$a = array();
								$a['chapo'] = '';
								$a['nb_com'] = 0;
								$a['allow_com'] = 0;
								$a['thumbnail'] = '';
								$a['content'] = $htm . '<p class="locked">'.sprintf($this->getLang('L_NEED_AUTH'),$this->plxMotor->urlRewrite('?adhesion.html'),$this->getParam('mnuAdhesion')).'</p>'.$this->finclude(PLX_PLUGINS.__CLASS__.'/form.login.inc.php',false,'aside');//categorie, home, article
								$this->plxMotor->plxRecord_arts->result[$key] = array_merge($this->plxMotor->plxRecord_arts->result[$key], $a);
							}
						}#end foreach : this->plxMotor->plxRecord_arts->result
						unset($htm);
				}//Fi switch plxMotor->mode
				header('Location:'.$url);//on redirige
				exit;
			}//FI LOGIN OK
			else {//BAD LOGIN
				if (isset($_SESSION['maxtryAdh']) && $_SESSION['maxtryAdh'] >= 3) {
					$_SESSION['timeoutAdh'] = time() + (60*15);
					$_SESSION['erase']['m'] = '<p id="password_error">'.$this->getlang('L_PLUGIN_MAXTRY').'&nbsp;'.date('H\hi',(60+$_SESSION['timeoutAdh'])).'</p>';
					$_SESSION['erase']['d'] = 'e';//display what
				} else {
					$_SESSION['lockArticles']['error'] = $this->getlang('L_PLUGIN_BAD_PASSWORD');
				}
			}//FI LOGIN OK & BAD LOGIN
		}//FI Vérification de la connexion

		if (isset($_SESSION['domainAd']) && $_SESSION['domainAd'] != $this->session_domain) {
			$showForm = TRUE;
			unset($_SESSION['domainAd']);
		}

		if($showForm) {

			$a = array();
			$a['content'] = '<p class="locked">'.sprintf($this->getLang('L_NEED_AUTH'),$this->plxMotor->urlRewrite('?adhesion.html'),$this->getParam('mnuAdhesion')).'</p>'.$this->finclude(PLX_PLUGINS.__CLASS__.'/form.login.inc.php',false,'aside');//article
			if(isset($_SESSION['lockArticles']['error'])) {
				$a['content'] .= '<p class="static_password_error">'.$_SESSION['lockArticles']['error'].'</p>';
				unset($_SESSION['lockArticles']['error']);
			}
			$a['nb_com'] = 0;
			$a['allow_com'] = 0;
			$a['chapo'] = '';
			$a['thumbnail'] = '';
			if($this->plxMotor->plxRecord_arts) {
				if($this->getParam('strcutLocks')){#On tronque le html de l'article #tep
					$a['content'] = $this->cutContent($this->plxMotor->plxRecord_arts->result[0]['chapo'].PHP_EOL.$this->plxMotor->plxRecord_arts->result[0]['content']) . $a['content'];
				}
				$this->plxMotor->plxRecord_arts->result[0] = array_merge($this->plxMotor->plxRecord_arts->result[0], $a);
				if ($this->plxMotor->mode != 'annuaire') {
					$this->plxMotor->template = $this->plxMotor->plxRecord_arts->f('template');
				}
			}
		}
	}

	/**
	 * Méthode qui coupe le html (selon le paramètre strcutLocks)
	 *
	 * @param	htm	HTML a couper
	 * @return	html tronqué ou rien
	 * @scope	home,categorie,article,tags,archives
	 * @author	Thomas Ingles
	 **/
	public function cutContent($htm) {
		$len = $this->getParam('strcutLocks');
		if($len){
			$str = trim(strip_tags($htm)); 
			#plxUtils::strCut($str='', $length=25, $type='', $add_text='...')
			return '<p class="trunked">'.plxUtils::strCut($str, $len, 'word', '...').'</p>'.PHP_EOL;
		}
		return '';#nothing
	}

	/**
	 * Méthode qui inclus le formulaire de connexion
	 * Voir L_NEED_AUTH ou lockarticles
	 *
	 * @return string if $echo true inc on called moment
	 * @author Thomas Ingles
	 */
	public function finclude($what,$echo=false,$class=false) {
		if (!is_file($what)) return;
		if(!$echo)ob_start();
		include($what);
		if(!$echo)return ob_get_clean();
}

	/**
	 * Méthode qui affiche le bouton de déconnexion et le formulaire de connexion
	 * Voir $this->ThemeEndBody pour l'affichage des messages
	 *
	 * @return string
	 * @author Cyril MAGUIRE
	 */
	public function loginLogout() {//TODO Wall-e + capcha? for login (OR add func in this class)

		if ((isset($_SESSION['domain']) AND $_SESSION['domain'] == $this->session_domain) || (isset($_SESSION['lockArticles']['categorie']) && $_SESSION['lockArticles']['categorie'] == 'on') || (isset($_SESSION['lockArticles']['articles']) && $_SESSION['lockArticles']['articles'] == 'on') ) {
			if (isset($_SESSION['timeoutAdh']) ) {
				unset($_SESSION['timeoutAdh']);
			}
		}//unset($_SESSION['maxtryAdh'],$_SESSION['timeoutAdh']);// to debug
		if (isset($_SESSION['timeoutAdh']) ) {
			unset($_SESSION['lockArticles']);
			if (time() < $_SESSION['timeoutAdh'] ) {
				$this->finclude(PLX_PLUGINS.__CLASS__.'/form.login.inc.php',true);#echo cnx form trop d'essais + time out notice + renvoi pass
			} else {
				unset($_SESSION['maxtryAdh']);
				unset($_SESSION['timeoutAdh']);
				header('Location:'.$this->plxMotor->racine.$this->plxMotor->path_url);//header('Location:'.$this->plxMotor->urlRewrite());
				exit;
			}
		} else {
			if ( (isset($_SESSION['domain']) AND $_SESSION['domain'] == $this->session_domain) || (isset($_SESSION['lockArticles']['categorie']) && $_SESSION['lockArticles']['categorie'] == 'on') || (isset($_SESSION['lockArticles']['articles']) && $_SESSION['lockArticles']['articles'] == 'on') ) {
				$this->finclude(PLX_PLUGINS.__CLASS__.'/form.logout.inc.php',true);#echo Space member form
			} else {
				$this->finclude(PLX_PLUGINS.__CLASS__.'/form.login.inc.php',true);#echo login | oops pass form
			}
		}
	}

	/**
	 * Méthode qui enclanche la bufferisation de sortie pour afficher les cadenas (voir AdminIndexFoot())
	 *
	 * @author Cyril MAGUIRE
	 */
	public function AdminIndexTop (){
		echo '<?php ob_start(); ?>';
	}

	/**
	 * Méthode qui affiche le cadenas au niveau de la page articles de l'administration si un article a un mot de passe
	 *
	 * @author Cyril MAGUIRE
	 */
	public function AdminIndexFoot () {
		echo '<?php '; ?>
			$content=ob_get_clean();
			if(preg_match_all("#value=\"([0-9]{4})\"#", $content, $capture)) {//origin
#			if(preg_match_all("#</td><td>([0-9]{4})</td><td>#", $content, $capture)) {//idée ok
				$image = '<img style="max-width: none;" src="<?php echo PLX_PLUGINS.get_class($this) ?>/locker.png" title="<?php $this->lang('L_ART_PROTECTED')?>">';
				$imagecat = '<img style="max-width: none;" src="<?php echo PLX_PLUGINS.get_class($this) ?>/locker-cat.png" title="<?php $this->lang('L_CAT_PROTECTED')?>">';
				$artTab = array();
				$artTabCat = array();
				while($plxAdmin->plxRecord_arts->loop()) {
					$art_id = $plxAdmin->plxRecord_arts->f('numero');
					$cat_id = explode(',',$plxAdmin->plxRecord_arts->f('categorie'));
					$artTab[$art_id] = $plxAdmin->plxRecord_arts->f('password');
					foreach ($cat_id as $key => $value) {
						if (!empty($plxAdmin->aCats[$value]['password'])) {
							$artTabCat[$art_id] = $plxAdmin->aCats[$value]['password'];
						}
					}
				}
				foreach($capture[1] as $ArtId) {
					$adImg = "";
					if(!empty($artTab[$ArtId]))// && empty($artTabCat[$ArtId])
						$adImg .= $image;
					if(!empty($artTabCat[$ArtId]))
						$adImg .= $imagecat;
#					$str = '<td><a href="article.php?a='.$ArtId.'" title="";//on links edit
#					$content = str_replace($str.L_ARTICLE_EDIT_TITLE.'>', $str, $content);//on links edit
#					$content = str_replace($str, $str.L_ARTICLE_EDIT_TITLE.'>'.$adImg.'&nbsp;', $content);//on links edit
					$str = '</td><td>'.$ArtId;
					$content = str_replace($str, $str.$adImg, $content);
				}
			}
			echo $content;
?><?php
	}

	/**
	 * Méthode permettant de masquer s'il y a un mot de passe
	 *
	 * @author Cyril MAGUIRE, Thomas Ingles
	 */
	public function plxFeedPreChauffageEnd() {
		$this->plxMotor = plxFeed::getInstance();
		if ($this->plxMotor->mode == 'commentaire') {
			$this->hideComs();
		}
		echo '<?php '; ?>
		switch($this->mode) {
			case 'tag':
			case 'article' : $this->mode = $this->mode.'_password';break;//$this->getRssArticles(); break;
			//case 'commentaire' : $this->getRssComments(); break;
			//case 'admin' : $this->getAdminComments(); break;
			default : break;
		}
?><?php
	}

	/**
	 * Méthode qui permet d'afficher un article dans le flux RSS s'il n'est pas protégé par un mot de passe
	 * Note : $_SESSION : only langs : $_SESSION['lockArticles'] unexist here
	 * @author Cyril MAGUIRE, Thomas Ingles
	 */
	public function plxFeedDemarrageBegin() {
		echo '<?php '; ?>
		if(strpos($this->mode,'_password') === FALSE)//if ($this->mode != '###_password' ::: article commentaire & tag
			return;
		$this->mode = str_replace('_password','',$this->mode);//goto ORIGIN mode (no trouble of PluXml Motor ;)
		# Flux de commentaires d'un article précis
		if($this->mode == 'commentaire' AND $this->cible) {
			if(!$this->getArticles()) { # Aucun article, on redirige
				$this->cible = $this->cible + 0;
				header('Location:'.$this->urlRewrite('?article'.$this->cible.'/'));
				exit;
			} else { # On récupère les commentaires
				$regex = '/^'.$this->cible.'.[0-9]{10}-[0-9]+.xml$/';
				$this->getCommentaires($regex,'rsort',0,$this->bypage);
			}
		}
		# Flux de commentaires global
		elseif($this->mode == 'commentaire') {
			$regex = '/^[0-9]{4}.[0-9]{10}-[0-9]+.xml$/';
			$this->getCommentaires($regex,'rsort',0,$this->bypage);
		}
		# Flux admin
		elseif($this->mode == 'admin') {
			if(empty($this->clef)) { # Clef non initialisée
				header('Content-Type: text/plain; charset='.PLX_CHARSET);
				echo L_FEED_NO_PRIVATE_URL;
				exit;
			}
			# On récupère les commentaires
			$this->getCommentaires('/^'.$this->cible.'[0-9]{4}.[0-9]{10}-[0-9]+.xml$/','rsort',0,$this->bypage,'all');
		}
		# Flux d'articles pour un tag
		elseif($this->mode == 'tag') {
			if(empty($this->motif)) {
				header('Location: '.$this->urlRewrite('?tag/'.$this->cible.'/'));
				exit;
			} else {
				$this->getArticles(); # Récupération des articles (on les parse)
			}
		}
		# Flux d'articles
		else {
			if($this->cible) {# Flux des articles d'une catégorie précise
				# On va tester la catégorie
				if(empty($this->aCats[$this->cible]) OR !$this->aCats[$this->cible]['active']) { # Pas de catégorie, on redirige
					$this->cible = $this->cible + 0;
					header('Location: '.$this->urlRewrite('?categorie'.$this->cible.'/'));
					exit;
				}
			}
			$this->getArticles(); # Récupération des articles (on les parse)
		}
		if($this->plxRecord_arts) {
			$strCut = intval('<?php echo $this->getParam('strcutLocks') ?>');
			$htm = '';
			while($this->plxRecord_arts->loop()) {
				$password = $this->plxRecord_arts->f('password');
				$category = $this->plxRecord_arts->result[$this->plxRecord_arts->i]['categorie'];
				$cats = explode(',',$category);
				foreach($cats AS $cat){
					if($cat=='draft' OR $cat=='000' OR $cat=='home')continue;//warn : Cat virtuelles et brouillon (non gérable par le plugin)
					$catPassword = $this->aCats[$cat]['password'];
					if(!empty($catPassword)) {
						$password = 'on';
						continue;
					}
				}
				if(!empty($password)) {#clear all fields
					if($strCut){#On tronque le html de l'article #tep
						$htm = $this->plxPlugins->aPlugins['<?php echo __CLASS__ ?>']->cutContent($this->plxRecord_arts->result[$this->plxRecord_arts->i]['chapo'].PHP_EOL.$this->plxRecord_arts->result[$this->plxRecord_arts->i]['content']);
					}
					$this->plxRecord_arts->result[$this->plxRecord_arts->i]['chapo'] = '';
					$this->plxRecord_arts->result[$this->plxRecord_arts->i]['nb_com'] = 0;
					$this->plxRecord_arts->result[$this->plxRecord_arts->i]['allow_com'] = 0;
					$this->plxRecord_arts->result[$this->plxRecord_arts->i]['thumbnail'] = '';
					$this->plxRecord_arts->result[$this->plxRecord_arts->i]['content'] = $htm . '<i><?php $this->lang('L_NEED_AUTH_FEED')?></i>';
				}
			}#end while : this->plxRecord_arts->loop
			unset($htm);
			$this->getRssArticles();#DISPLAY THIS
			return true;#STOP HERE! : getArticles() RELOAD arts with real data AFTER
		}
?><?php
	}

	///////////////////////////////////////////////////////////
	//
	// Pages statiques
	//
	//////////////////////////////////////////////////////////

	/**
	 * Méthode qui ajoute le champ de saisie du mot de passe dans la page d'édition de la page statique
	 *
	 * @return	stdio
	 * @author	Cyril MAGUIRE, Thomas Ingles
	 **/
	public function AdminStatic() {
		echo '<?php '; ?>
			if(!$id AND !empty($_GET['p'])) # On affiche le contenu de la page
				$id = plxUtils::strCheck(plxUtils::nullbyteRemove($_GET['p']));//fix IF plxMML LOADED BEFORE, HAVE IN HOOK $id = $ title = '';   ?
			$password = plxUtils::getValue($plxAdmin->aStats[$id]["password"]);
			$image = '&nbsp;<img src="<?php echo PLX_PLUGINS.__CLASS__ ?>/locker.png" alt="" />';
?>
			<fieldset>
				<p id="<?php echo __CLASS__ ?>_password"><label for="id_password"><?php echo $this->getLang('L_FORM_ADMIN_PASSWORD') ?>&nbsp;: <?php echo '<?php if($password == "on") echo $image ?>'; ?></label>
				<?php echo '<?php plxUtils::printSelect("password",array(""=>L_NO,"on"=>L_YES),$password); ?>'; ?></p>
			</fieldset>
<?php
	}

	/**
	 * Méthode qui ajoute la notification de mot de passe dans la chaine xml à sauvegarder dans statiques.xml
	 *
	 * @return	stdio
	 * @author	Cyril MAGUIRE, Thomas Ingles
	 **/
	public function plxAdminEditStatiquesXml() {
		echo '<?php '; ?>
		$xml .= '<password><![CDATA['.plxUtils::cdataCheck(@$static['password']).']]></password>';
?><?php
	}

	/**
	 * Méthode qui récupère la notification de mot de passe saisit lors de l'édition de la page statique
	 *
	 * @return	stdio
	 * @author	Cyril MAGUIRE, Thomas Ingles
	 **/
	public function plxAdminEditStatique() {
		echo '<?php '; ?>
		$this->aStats[$content['id']]['password'] = (!empty($content['password']) ? 'on' : '');//FALSE
?><?php
	}

	/**
	 * Méthode qui récupère la notification de mot de passe stockée dans le fichier xml statiques.xml
	 *
	 * @return	stdio
	 * @author	Cyril MAGUIRE, Thomas Ingles
	 **/
	public function plxMotorGetStatiques() {
		echo '<?php '; ?>
		$password = plxUtils::getValue($iTags['password'][$i]);
		$this->aStats[$number]['password']=plxUtils::getValue($values[$password]['value']);
?><?php
	}

	/**
	 * Méthode qui permet de démarrer la bufferisation de sortie sur la page admin/statiques.php
	 *
	 * @return	stdio
	 * @author	Cyril MAGUIRE
	 **/
	public function AdminStaticsTop() {
		echo '<?php ob_start(); ?>';
	}

	/**
	 * Méthode qui affiche l'image du cadenas si la page est protégée par un mot de passe
	 *
	 * @return	stdio
	 * @author	Cyril MAGUIRE, Thomas Ingles
	 **/
	public function AdminStaticsFoot() {
		echo '<?php '; ?>
		$content=ob_get_clean();
//		 if(preg_match_all("#<td>".L_PAGE." ([0-9]{3})</td>#", $content, $capture)) {// <= 5.3.1
//		 if(preg_match_all("#</td><td>([0-9]{3})</td><td>#", $content, $capture)) {// >= 5.4
		if(preg_match_all('#idStatic\[\]" value="([0-9]{3})"#', $content, $capture)) {//Multi
			$image = '<img src="<?php echo PLX_PLUGINS.get_class($this)?>/locker.png" title="<?php $this->lang('L_PGS_PROTECTED')?>" />';
			foreach($capture[1] as $idStat) {
//				 $str = "<td>".L_PAGE." ".$idStat;//5.2
				$str = "</td><td>".$idStat;
				if(isset($plxAdmin->aStats[$idStat]["password"]) AND $plxAdmin->aStats[$idStat]["password"] == 'on') {
					$content = str_replace($str, $str." ".$image, $content);
				}
			}
		}
		echo $content;
?><?php
	}

	/**
	 * Méthode qui protege les pages statiques si mode static_password : si page protégée url = static_password
	 *
	 * @return	stdio
	 * @author	Cyril MAGUIRE, Thomas Ingles
	 **/
	public function plxShowConstructStat() {
		# infos sur la page statique
		echo '<?php '; ?>
		if($this->plxMotor->mode=='static_password') {
			$array = array();
			$array[$this->plxMotor->cible] = array(
			'name'		=> $this->plxMotor->aStats[$this->plxMotor->idStat]['name'],
			'menu'		=> '',
			'url'		=> 'static_password',
			'readable'	=> 1,
			'active'	=> 1,
			'group'		=> ''
		);
			$this->plxMotor->aStats = array_merge($this->plxMotor->aStats, $array);
		}
?><?php
	}

	/**
	 * Méthode qui affiche le formulaire d'identification si un mot de passe est présent pour la page statique
	 *
	 * @return	stdio
	 * @author	Cyril MAGUIRE, Thomas Ingles
	 **/
	public function plxMotorPreChauffageEndStat() {
		$cible = (defined('PLX_MYMULTILINGUE')?'../':'').'../../'.PLX_PLUGINS.__CLASS__.'/form';
		echo '<?php '; ?>
		if($this->mode=='static') {
			$password = plxUtils::getValue($this->aStats[$this->cible]['password']);
			if($password=='on') {
				if(!isset($_SESSION['lockArticles']['categorie'])) {
					$this->idStat = $this->cible;
					$this->cible = '<?php echo $cible ?>';
					$this->mode = 'static_password';
					$this->template = 'static.php';
				}
			}
		}
?><?php
	}

	/**
	 * Méthode qui valide la connexion d'un adhérent
	 *
	 * @return	stdio
	 * @author	Cyril MAGUIRE
	 **/
	public function plxMotorDemarrageEndStat(){
		if($this->plxMotor->mode == 'static' || $this->plxMotor->mode == 'static_password') {
			if(isset($_POST['lockArticles']) AND isset($_POST['password']) AND isset($_POST['login'])) {
				if ($this->verifPass($_POST['password'],plxUtils::strCheck($_POST['login']))) {
					$_SESSION['lockArticles']['categorie'] = $_SESSION['lockArticles']['articles'] = 'on';
					$url = $this->plxMotor->urlRewrite('?'.$this->plxMotor->aCats[$this->plxMotor->cible]['url']);
					$_SESSION['lockArticles']['success'] = $this->getlang('L_CONNEXION_OK');
					header('Location:'.$url);
					exit;
				}
				else {
					$_SESSION['lockArticles']['error'] = $this->getlang('L_PLUGIN_BAD_PASSWORD');
				}
			}
		}
	}

	/**
	 * Méthode qui renseigne le titre de la page dans la balise html <title> des statiques avec Mot De Passe
	 *
	 * @return	stdio
	 * @author	Stephane F
	 **/
	public function plxShowPageTitleStat() {
		echo '<?php '; ?>
			if($this->plxMotor->mode == "static_password") {
				echo plxUtils::strCheck($this->plxMotor->aConf["title"])." - ".plxUtils::strCheck($this->plxMotor->aStats[$this->plxMotor->idStat]["name"]);
				return TRUE;
			}
?><?php
	}

	/**
	 * Méthode qui ajoute le fichier style-lock.css dans le fichier header.php du thème
	 *
	 * @return	stdio
	 * @author	Stephane F
	 **/
	public function ThemeEndHeadStat() {
		if(isset($_SESSION['lockArticles'])) return;
?>
	<link rel="stylesheet" type="text/css" href="<?php echo PLX_PLUGINS.__CLASS__ ?>/style-lock.css?v=<?php echo $this->v ?>" media="screen" />
<?php
	}

	/**
	 * Méthode qui affiche des messages succes/erreur
	 *
	 * @return	stdio
	 * @author	Cyril MAGUIRE
	 **/
	public function ThemeEndBody() {
		$js=0;
		if (isset($_SESSION['lockArticles']['success'])  ) {
			if (!isset($_SESSION['lockArticles']['log'])) {
				echo '<p id="password_success">'.$_SESSION['lockArticles']['success'].'</p>';
				$js='s';
			}
//			if($_SESSION['lockArticles']['success'] == $this->getlang('L_CONNEXION_OK')){
				unset($_SESSION['lockArticles']['success']);
//			}
		}
		if (isset($_SESSION['logout']) && $_SESSION['logout'] == $this->getlang('L_DECONNEXION_OK')) {
			echo '<p id="password_success">'.$this->getLang('L_DECONNEXION_OK').'</p>';
			$js='s';
			unset($_SESSION['logout']);
		}
		if (isset($_SESSION['lockArticles']['error']) ) {
			if (!isset($_SESSION['lockArticles']['log'])) {
				echo '<p id="password_error">'.$_SESSION['lockArticles']['error'].'</p>';
				$js='e';
			}
//			if($_SESSION['lockArticles']['error'] == $this->getlang('L_PLUGIN_BAD_PASSWORD')){
				unset($_SESSION['lockArticles']['error']);
//			}
		}
		if (isset($_SESSION['retrievePass'])) {
			echo $_SESSION['retrievePass']['m'];//message
			$js=$_SESSION['retrievePass']['d'];//display what
			unset($_SESSION['retrievePass']);
		}
		if (isset($_SESSION['erase'])) {
			echo $_SESSION['erase']['m'];//message
			$js=$_SESSION['erase']['d'];//display what
			unset($_SESSION['erase']);
		}
		if ($js) {
			echo '
			<script type="text/javascript">
			setTimeout(function(){
				'.(strstr($js,'s')?'document.getElementById("password_success").style.display="none";':'').'
				'.(strstr($js,'e')?'document.getElementById("password_error").style.display="none";':'').'
			},5000);
			</script>';
		}
	}
}
