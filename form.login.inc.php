<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 * $this IS adhesion : used with finclude L_NEED_AUTH (ART,TAG,CAT,PAGE... LOCKED)
 * @version	2.0.0
 * @date	23/10/2018
 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
 **/
#TODO capcha?
if(!$this->form_login_adherent) {
	$this->form_login_adherent = $this->plxMotor->mode;
} else {
	return;//Only one form!
}

?>
<div class="<?php echo $this->plxMotor->mode.$class?' '.$class:''; ?>" id="espace-adherents">
	<h3 class="widget-title icon-parents"><?php echo $this->getParam('mnuMembers');#echo' : '. $this->plxMotor->mode.' : '. $this->plxMotor->cible.' : '.$this->plxMotor->path_url.' : '.$this->form_login_adherent.' : '.time() ?> - <?php echo $this->getParam('mnuConnexion') ?></h3>
<?php
if (isset($_SESSION['timeoutAdh']) ) {
	//unset($_SESSION['maxtryAdh'],$_SESSION['timeoutAdh']);var_dump(time(),$_SESSION['timeoutAdh'],$_SESSION['maxtryAdh']);// to debug
	if (time() < $_SESSION['timeoutAdh'] ) {
?>
	<p style="text-align:center;"><?php $this->lang('L_PLUGIN_MAXTRY')?>&nbsp;<?php echo date('H\hi',(60+$_SESSION['timeoutAdh']))?></p>
	<p id="forgetmypass"><a href="<?php echo $this->plxMotor->urlRewrite('?forgetmypass.html');?>"><?php $this->lang('L_FORGET_PASS')?></a></p>
</div>
<?php return;
	}
}
?>
	<form action="" method="post">
		<fieldset>
			<p>
				<label><?php $this->lang('L_ID') ?>&nbsp;:</label><br class="hide" />
				<input type="text" name="login" maxlength="150" value="" id="id" required />
			</p><p>
				<label><?php $this->lang('L_FORM_PASSWORD') ?>&nbsp;:</label><br class="hide" />
				<input type="password" name="password" size="25" maxlength="90" value="" id="pass" required />
				<input type="hidden" name="lockArticles" />
				<input type="hidden" name="adMotorId" value="<?php echo $this->plxMotor->path_url; ?>" />
			</p>
			<p><input type="submit" value="<?php $this->lang('L_FORM_OK') ?>" /></p>
		</fieldset>
		<p id="wall-e">
			<label for="walle"><?php $this->lang('L_FORM_WALLE') ?></label>
			<input id="walle" name="wall-e" type="text" size="50" value="<?php echo plxUtils::strCheck(@$_POST['wall-e']) ?>" maxlength="50" />
		</p>
	</form>
	<p id="new-adherent"><a href="<?php echo $this->plxMotor->urlRewrite('?adhesion.html');?>"><?php echo $this->getParam('mnuAdhesion')?></a></p>
	<p id="forgetmypass"><a href="<?php echo $this->plxMotor->urlRewrite('?forgetmypass.html');?>"><?php echo $this->getParam('mnuForgetMyPass') ?></a></p>
</div>
<script>function doFocus(i){window.document.getElementById(i).focus();}</script>
