<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 * $this IS plxShow
 * @version	2.0.0
 * @date	24/05/2018
 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
 **/
$useCapcha = TRUE;
include('form.init.inc.php');#init plug & capcha
?><form action="" method="post">
	<fieldset>
		<p><label><b><?php $plxPlugin->lang('L_FORM_MAIL') ?>&nbsp;:</b></label><br /></p>
		<input type="email" name="email" size="50" value="" required />
<?php if($this->plxMotor->aConf['capcha']): #$this->lang('ANTISPAM_WARNING')?>
		</fieldset>
		<fieldset>
		<p><label for="id_rep"><strong><?php $plxPlugin->lang('L_FORM_ANTISPAM') ?>&nbsp;:</strong></label></p>
		<?php $this->capchaQ(); ?>
		<input id="id_rep" name="rep" type="text" size="2" maxlength="1" autocomplete="off" style="width: auto; display: inline;" required />
<?php endif; ?>
		<p><input type="submit" value="<?php $plxPlugin->lang('L_FORM_BTN_SEND') ?>" /></p>
		<input type="hidden" name="forgetmypass">
		<p id="wall-e">
			<label for="walle"><?php $plxPlugin->lang('L_FORM_WALLE') ?></label>
			<input id="walle" name="wall-e" type="text" size="50" value="<?php #echo plxUtils::strCheck($wall_e) ?>" maxlength="50" />
		</p>
	</fieldset>
</form>