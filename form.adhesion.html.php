<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 * $this IS plxShow
 * @version	2.1.1
 * @date	17/04/2019
 * @author	Stephane F, Cyril MAGUIRE, Thomas Ingles
 **/
$useCapcha = TRUE;
include('form.init.inc.php');#init plug & capcha
#init vars
$id = $plxPlugin->nextIdAdherent();
$pro = array();
$error = array();
$success = false;
$wall_e = '';
if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
 $aA = explode(',',$plxPlugin->getParam('tabActivites'));
 $aK = array_map('strtolower', $aA);
 $aActivites = array_combine($aK, $aA);
 $firstActivityId = key($aActivites);#php 7 : array_key_first
}
if(!empty($_POST) && !empty($_POST['wall-e'])) {
	$wall_e = $_POST['wall-e'];
}
if (isset($_GET['q'])) {
	$erase = $plxPlugin->compare($_GET['q']);
	if ($erase) {
		$_SESSION['erase']['m'] = '<p id="password_success">'.$plxPlugin->getLang('L_FORM_ERASE_FORM_LIST_OK').'</p>';
		$_SESSION['erase']['d'] = 's';//display what
		header('location:'.$this->plxMotor->urlRewrite());
		unset($_GET['q']);
		exit();
	}
}
if(!empty($_POST) && empty($_POST['wall-e']) && isset($_POST['nom_'.$id])) {
	$nom=strtolower(trim(plxUtils::strCheck($_POST['nom_'.$id])));
	$prenom=strtolower(trim(plxUtils::strCheck($_POST['prenom_'.$id])));

	if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
		$activite=isset($_POST['activite_'.$id])?trim(plxUtils::strCheck($_POST['activite_'.$id])):'';
		$activite_autre=trim(plxUtils::strCheck($_POST['activite_autre_'.$id]));
		$etablissement=trim(plxUtils::strCheck($_POST['etablissement_'.$id]));
		$service=trim(plxUtils::strCheck($_POST['service_'.$id]));
		$tel_office=str_replace(array('.','-',' ','_','+','(',')',',',':',';','/'),'',plxUtils::strCheck($_POST['tel_office_'.$id]));
		if($activite =='')
			$error[] = $plxPlugin->getLang('L_ERR_ACTIVITE');
		if($activite =='autre' && trim($activite_autre) == '')
			$error[] = $plxPlugin->getLang('L_ERR_AUTRE_ACTIVITE');
		if(trim($etablissement) == '')
			$error[] = $plxPlugin->getLang('L_ERR_ETABLISSEMENT');
		if(trim($service) == '')
			$error[] = $plxPlugin->getLang('L_ERR_SERVICE');
		if(trim($tel_office) != '' && !preg_match('!^[0-9]{9,13}[0-9]$!',$tel_office))
			$error[] = $plxPlugin->getLang('L_ERR_TEL_OFFICE');
		$pro = array(
			'activite'=>$activite,
			'activite_autre'=>$activite_autre,
			'etablissement'=>$etablissement,
			'service'=>$service,
			'tel_office'=>$tel_office,
			'coordonnees'=>''
		);
	}

	if($plxPlugin->getParam('showAnnuaire') == 'on') {
		$coordonnees = isset($_POST['coordonnees_'.$id])?trim(plxUtils::strCheck($_POST['coordonnees_'.$id])):'';
		if(trim($coordonnees) == '')
			$error[] = $plxPlugin->getLang('L_ERR_COORDONNEES');
		else
			$pro['coordonnees'] = $coordonnees;
	}

	$adresse1=trim(plxUtils::strCheck($_POST['adresse1_'.$id]));
	$adresse2=trim(plxUtils::strCheck($_POST['adresse2_'.$id]));
	$cp=$_POST['cp_'.$id];
	$ville=trim(plxUtils::strCheck($_POST['ville_'.$id]));
	$tel=str_replace(array('.','-',' ','_','+','(',')',',',':',';','/'),'',$_POST['tel_'.$id]);
	$mail=trim(str_replace('&#64;', '@', plxUtils::strCheck($_POST['mail_'.$id])));
	$choix=isset($_POST['choix_'.$id])?plxUtils::strCheck($_POST['choix_'.$id]):'';
	$mailing=isset($_POST['mailing_'.$id])?plxUtils::strCheck($_POST['mailing_'.$id]):'';
	if(trim($nom)=='')
		$error[] = $plxPlugin->getLang('L_ERR_NAME');
	if(trim($prenom)=='')
		$error[] = $plxPlugin->getLang('L_ERR_FIRST_NAME');
	if(trim($adresse1) == '')
		$error[] = $plxPlugin->getLang('L_ERR_ADRESSE');
	if(trim($cp) == '' || strlen($cp) != 5 || !is_numeric($cp))
		$error[] = $plxPlugin->getLang('L_ERR_CP');
	if(trim($ville) == '')
		$error[] = $plxPlugin->getLang('L_ERR_VILLE');
	if(trim($tel) == '' || !preg_match('!^[0-9]{9,13}[0-9]$!',$tel))
		$error[] = $plxPlugin->getLang('L_ERR_TEL');
	if(trim($choix) == '')
		$error[] = $plxPlugin->getLang('L_ERR_CHOIX');
	if(trim($mailing) == '')
		$error[] = $plxPlugin->getLang('L_ERR_MAILING');
	if(!plxUtils::checkMail($mail))
		$error[] = $plxPlugin->getLang('L_ERR_MAIL');
	if (isset($this->plxMotor->plxPlugins->aPlugins["plxCapchaImage"]))//si capchaImage
		$_SESSION["capcha"]=sha1(@$_SESSION["capcha"]);
	if($this->plxMotor->aConf['capcha'] AND $_SESSION['capcha'] != sha1($_POST['rep']))
		$error[] = $plxPlugin->getLang('L_ERR_ANTISPAM');
	foreach ($plxPlugin->adherentsList as $adherent) {
		if ($nom.' '.$prenom == $adherent['nom'].' '.$adherent['prenom'] && $mail == $adherent['mail'] && $adherent['validation'] == '1' && $choix == 'adhesion') {
			$error['extra'] = $plxPlugin->getLang('L_ERR_USER_ALREADY_USED');
		}
	}
	if(empty($error)) {
		$content = $plxPlugin->notification($nom,$prenom,$adresse1,$adresse2,$cp,$ville,$tel,$mail,$choix,$mailing,$pro);#RETOUR HTML ?
		# On édite la liste des adhérents
		$edit = $plxPlugin->editAdherentslist($_POST,$id);

		if ($choix != 'stop') {
			$logInBase = str_replace(array('-','_'),'',plxUtils::title2url(strtolower($nom.$prenom)));#login
			#Si pas d'erreur, envoie du mail à l'admin contenant les informations de l'adhérent
			if($plxPlugin->sendEmail($plxPlugin->getParam('nom_asso'),$plxPlugin->getParam('email'),$plxPlugin->getParam('email'),$plxPlugin->getParam('subject'),$content,'html')){
				if ($choix == 'renouveler') {
					#Envoie du mail à l'adhérent
					if($mail = $plxPlugin->sendEmail($plxPlugin->getParam('nom_asso'),$plxPlugin->getParam('email'),$mail,$plxPlugin->getParam('subject'),'<p>'.$plxPlugin->getLang('L_BJR_MSG').' '.$prenom.' '.$nom.'.<br/>'.$plxPlugin->getParam('thankyou').'</p>'.$plxPlugin->adresse(),'html')) {
						if (empty($error)){
							$success = $plxPlugin->getParam('thankyou');#RETOUR HTML
						}
					}
				} elseif (empty($error)){
					$success = $plxPlugin->getParam('thankyou');#RETOUR HTML
				}
			}
			if(!$mail)#on informe si une erreur d'envoi, affiches les infos, sans le "verifier votre spam box" ;)
				$info = '<b class="contact_error">'.$plxPlugin->getLang('L_ERR_SENDEMAIL').'!</b><br/><br/>'.$plxPlugin->getLang('L_BJR_MSG').$prenom.' '.$nom.'.<br/><br/>'.$plxPlugin->getParam('thankyou').'<br/><br/>'.$plxPlugin->getLang('L_ADMIN_ID').'&nbsp;: <b><u>'.$logInBase.'</u></b><br/><br/>'.$plxPlugin->adresse(FALSE);
		} elseif($choix == 'stop') {
			$info = $plxPlugin->getLang('L_FORM_ERASE');
		}
	}
}
else {
	$nom = '';
	$prenom = '';
	$adresse1 = '';
	$adresse2 = '';
	$cp = '';
	$ville = '';
	$tel = '';
	$mail = '';
	$choix = '';# renouveler stop
	$mailing = '';# blacklist
	if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
		$activite = $firstActivityId;
		$activite_autre = '';
		$etablissement = '';
		$service = '';
		$tel_office = '';
	}
	if($plxPlugin->getParam('showAnnuaire') == 'on') {
		$coordonnees='';# public refus
	}
}
?>
<div id="form_adherer">
<?php
$_POST = '';
if(!empty($info)) {
?>
	<p class="contact_success"><?php echo $info; ?></p>
<?php
	$nom = '';
	$prenom = '';
	$adresse1 = '';
	$adresse2 = '';
	$cp = '';
	$ville = '';
	$tel = '';
	$mail = '';
	$choix = '';# renouveler stop
	$mailing = '';# blacklist
	if ($plxPlugin->getParam('typeAnnuaire') == 'professionnel') {
		$activite = $firstActivityId;
		$activite_autre = '';
		$etablissement = '';
		$service = '';
		$tel_office = '';
	}
	if($plxPlugin->getParam('showAnnuaire') == 'on') {
		$coordonnees='';# public refus
	}
}
else {
	if(!empty($error))://0 ?>
		<div class="contact_error">
<?php if(isset($error['extra'])) :
				echo '<p>'.$error['extra'].'</p>';
				unset($error['extra']);
				endif;//$error['extra']
				if(!empty($error))://1
?>
				<h3><?php $plxPlugin->lang('L_FORM_FIELDS_MISSING') ?></h3>
				<ul>
<?php foreach ($error as $e) {
					echo PHP_EOL.'						<li>'.$e.'</li>';
				}
?>
				</ul>
<?php endif;//!empty($error) 1 ?>
		</div>
<?php endif;//!empty($error) 0 ?>
<?php unset($_POST);
		if($success):
			$_POST = '';
?>
		<p id="showpass"><?php echo $plxPlugin->getLang('L_ADMIN_ID'); ?>&nbsp;: <b><u><?php echo $logInBase ; ?></u></b><br/>

			<?php //echo $plxPlugin->getLang('L_ADMIN_PASSWORD');?>&nbsp;<?php //echo $l['cle'].'-'.substr($l['mail'],0,-$l['rand1']).$l['rand2']; ?>
		</p>
<?php
			//On affiche les instructions pour finaliser l'adhésion
			echo $plxPlugin->adresse();
		else:
?>
	<p id="all_required"><?php echo sprintf($plxPlugin->getLang('L_FORM_ALL_REQUIRED'),'<exp class="mandatory">*</exp>');?></p>
	<form action="#form" method="post" name="monadhesion">
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_IDENTITY');?>&nbsp;:</h2></legend>
		<p>
			<label for="nom"><?php $plxPlugin->lang('L_FORM_NAME') ?>&nbsp;:</label>
			<input id="nom" name="nom_<?php echo $id?>" type="text" size="30" pattern="[^0-9]+" value="<?php echo plxUtils::strCheck($nom) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
		</p>
		<p>
			<label for="prenom"><?php $plxPlugin->lang('L_FORM_FIRST_NAME') ?>&nbsp;:</label>
			<input id="prenom" name="prenom_<?php echo $id?>" type="text" size="30" pattern="[^0-9]+" value="<?php echo plxUtils::strCheck($prenom) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
		</p>
		</fieldset>
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') :
#
 $tot=count($aActivites);
 $ia=0;
?>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_ACTIVITY');?>&nbsp;:</h2></legend>
<?php foreach($aActivites AS $iActi => $acti): echo ++$ia==$tot?'<br/>':''; ?>
		<p class="act">
			<input id="<?php echo $iActi ?>" name="activite_<?php echo $id?>" type="radio" value="<?php echo $iActi ?>" <?php echo plxUtils::strCheck($activite) == $iActi? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="<?php echo $iActi ?>"><?php echo $acti ?></label>
		</p>
<?php endforeach; ?>
		<p>
			<label class="mask" for="activite_autre"><?php echo $plxPlugin->lang('L_FORM_DETAIL');?>&nbsp;:</label>
			<input class="mask" id="activite_autre" name="activite_autre_<?php echo $id?>" type="text" value="<?php echo plxUtils::strCheck($activite_autre);?>" />
		</p>
		</fieldset>
<?php endif; ?>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_AGENDA');?>&nbsp;:</h2></legend>
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') : ?>
		<p>
			<label for="etablissement"><?php $plxPlugin->lang('L_FORM_SOCIETY') ?>&nbsp;:</label>
			<input id="etablissement" name="etablissement_<?php echo $id?>" type="text" size="50" value="<?php echo plxUtils::strCheck($etablissement) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
		</p>
		<p>
			<label for="service"><?php $plxPlugin->lang('L_FORM_SERVICE') ?>&nbsp;:</label>
			<input id="service" name="service_<?php echo $id?>" type="text" size="50" value="<?php echo plxUtils::strCheck($service) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
		</p>
<?php endif; ?>
		<p>
			<label for="adresse1"><?php $plxPlugin->lang('L_FORM_ADDRESS') ?>&nbsp;:</label>
			<input class="xl" id="adresse1" name="adresse1_<?php echo $id?>" type="text" size="50" value="<?php echo plxUtils::strCheck($adresse1) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
			<input class="xl" id="adresse2" name="adresse2_<?php echo $id?>" type="text" size="50" value="<?php echo plxUtils::strCheck($adresse2) ?>" maxlength="50" />
		</p>
		<p>
			<label for="cp"><?php $plxPlugin->lang('L_FORM_ZIP_CODE') ?>&nbsp;:</label>
			<input id="cp" name="cp_<?php echo $id?>" type="text" size="16" value="<?php echo plxUtils::strCheck($cp) ?>" maxlength="5" required /><exp class="mandatory">*</exp>
		</p>
		<p>
			<label for="ville"><?php $plxPlugin->lang('L_FORM_CITY') ?>&nbsp;:</label>
			<input id="ville" name="ville_<?php echo $id?>" type="text" size="50" value="<?php echo plxUtils::strCheck($ville) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
		</p>
		<p>
			<label for="telnum"><?php $plxPlugin->lang('L_FORM_TEL') ?>&nbsp;:</label>
			<input id="telnum" name="tel_<?php echo $id?>" type="text" size="50" value="<?php echo plxUtils::strCheck($tel) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
		</p>
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') : ?>
		<p>
			<label for="tel_office"><?php $plxPlugin->lang('L_FORM_TEL_OFFICE') ?>&nbsp;:</label>
			<input id="tel_office" name="tel_office_<?php echo $id?>" type="text" size="50" value="<?php echo plxUtils::strCheck($tel_office) ?>" maxlength="50" required /><exp class="mandatory">*</exp>
		</p>
<?php endif; ?>
		<p>
			<label for="courriel"><?php $plxPlugin->lang('L_FORM_MAIL') ?>&nbsp;:</label>
			<input id="courriel" name="mail_<?php echo $id?>" type="email" size="50" value="<?php echo ($mail != '')? str_replace('@','&#64;',$mail):''; ?>" required /><exp class="mandatory">*</exp>
		</p>
		</fieldset>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_CHOICE');?>&nbsp;:</h2></legend>
		<p>
			<input id="adhesion" name="choix_<?php echo $id?>" type="radio" value="adhesion" <?php echo plxUtils::strCheck($choix) == 'adhesion'? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="adhesion"><?php $plxPlugin->lang('L_FORM_RULES') ?> <?php echo $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
<?php if($plxPlugin->getParam('annee') != 'illimite'): ?>
		<p>
			<input id="renouveler" name="choix_<?php echo $id?>" type="radio" value="renouveler" <?php echo plxUtils::strCheck($choix) == 'renouveler'? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="renouveler"><?php $plxPlugin->lang('L_FORM_RULES_RE') ?> <?php echo $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		<p>
			<input id="stop" name="choix_<?php echo $id?>" type="radio" value="stop" <?php echo plxUtils::strCheck($choix) == 'stop'? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="stop"><?php $plxPlugin->lang('L_FORM_RULES_NO') ?> <?php echo $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
<?php endif;#$plxPlugin->getParam('annee') != 'illimite') ?>
		</fieldset>
<?php if($plxPlugin->getParam('showAnnuaire') == 'on') : ?>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_SHARING');?>&nbsp;:</h2></legend>
<?php if($plxPlugin->getParam('publicAnnuaire') == 'on') : ?>
		<p>
			<input id="public" name="coordonnees_<?php echo $id?>" type="radio" value="public" <?php echo plxUtils::strCheck($coordonnees) == 'public' ? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="public"><?php $plxPlugin->lang('L_FORM_RULES_SHARE_PUBLIC') ?> <?php echo $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
<?php endif;#($plxPlugin->getParam('publicAnnuaire') == 'on') ?>
		<p>
			<input id="rec" name="coordonnees_<?php echo $id?>" type="radio" value="rec" <?php echo plxUtils::strCheck($coordonnees) == 'rec' ? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="rec"><?php $plxPlugin->lang('L_FORM_RULES_SHARE_MEMBER') ?> <?php echo $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		<p>
			<input id="refus" name="coordonnees_<?php echo $id?>" type="radio" value="refus" <?php echo plxUtils::strCheck($coordonnees) == 'refus' ? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="refus"><?php $plxPlugin->lang('L_FORM_RULES_SHARE_NO') ?> <?php echo $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		</fieldset>
<?php else:#fi ($plxPlugin->getParam('showAnnuaire') == 'on') ?>
			<input id="annuoff" name="coordonnees" type="hidden" value="rec" />
<?php endif; ?>
		<fieldset><legend><h2><?php $plxPlugin->lang('L_FORM_MAILING');?>&nbsp;:</h2></legend>
		<p>
			<input id="maillist" name="mailing_<?php echo $id?>" type="radio" value="maillist" <?php echo plxUtils::strCheck($mailing) == 'maillist' ? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="maillist"><?php $plxPlugin->lang('L_FORM_RULES_NEWS') ?> <?php echo $plxPlugin->getParam('nom_asso'); ?></label>
		</p>
		<p>
			<input id="blacklist" name="mailing_<?php echo $id?>" type="radio" value="blacklist" <?php echo plxUtils::strCheck($mailing) == 'blacklist' ? 'checked="checked"' : ''; ?> required /><exp class="mandatory">*</exp>
			<label for="blacklist"><?php $plxPlugin->lang('L_FORM_RULES_NEWS_NO') ?></label>
		</p>
		<p id="wall-e">
			<label for="walle"><?php $plxPlugin->lang('L_FORM_WALLE') ?>&nbsp;:</label>
			<input id="walle" name="wall-e" type="text" size="50" value="<?php echo plxUtils::strCheck($wall_e) ?>" maxlength="50" />
		</p>
		</fieldset>
		<fieldset>
<?php if($this->plxMotor->aConf['capcha']): #$this->lang('ANTISPAM_WARNING')?>
			<p>
				<label for="id_rep"><strong><?php $plxPlugin->lang('L_FORM_ANTISPAM') ?>&nbsp;:</strong></label>
			</p>
			<?php $this->capchaQ(); ?>
			<input id="id_rep" name="rep" type="text" size="2" maxlength="1" autocomplete="off" style="width: auto; display: inline;" required /><exp class="mandatory">*</exp>
<?php endif; ?>
		<p class="text-right">
			<input type="submit" name="submit" value="<?php $plxPlugin->lang('L_FORM_BTN_SEND') ?>" />
		</p>
		</fieldset>
	</form>
<?php if($plxPlugin->getParam('typeAnnuaire') == 'professionnel') : ?>
	<script type="text/javascript">
/* <![CDATA[ */
		var rad = document.monadhesion.activite_<?php echo $id?>;
		var othr = document.getElementsByClassName('mask');
		for(var m = 0;m < othr.length; m++)
			othr[m].style.display='none';
		for(var i = 0; i < rad.length; i++){
			rad[i].onclick = function() {
				if(this.value == 'autre'){//on affiche
					for(var m = 0;m < othr.length; m++)
						othr[m].style.display='';
					document.getElementById('activite_autre').required = true;
					document.getElementById('activite_autre').focus();
				}else{//on cache
					for(var m = 0;m < othr.length; m++)
						othr[m].style.display='none';
					document.getElementById('activite_autre').required = false;
					document.getElementById('activite_autre').value = '';
				}
			};
		}
/* ]]> */
	</script>
<?php endif; ?>

<?php endif;
} ?>
</div>