<?php if(!defined('PLX_ROOT')) exit;
/**
 * Plugin adhesion
 * $this IS plxShow
 * @version	2.0.0
 * @date	24/05/2018
 * @author	Stephane F, Cyril MAGUIRE, Thomas ingles
 **/
$useCapcha = !1;
include('form.init.inc.php');#init plug & !capcha
if($plxPlugin->getParam('showAnnuaire') != 'on') {
	header('Location:'.$this->plxMotor->urlRewrite('?erreur'));
	exit();
}
$locked = (//zone $locked?'public':'member'
   ( !isset($_SESSION['lockArticles']['articles']) && !isset($_SESSION['lockArticles']['categorie']) )
|| ( $_SESSION['lockArticles']['articles'] != 'on' && $_SESSION['lockArticles']['categorie'] != 'on')
);
if(isset($_SESSION['profil']) && $_SESSION['profil'] < PROFIL_MANAGER)//si connecté en admin ou manager on autorise
 $locked = FALSE;
//Si l'utilisateur n'est pas connecté, on affiche le message pour demander la connexion
if($plxPlugin->getParam('publicAnnuaire') == 'no' AND $locked):
	echo '<p class="locked">'.sprintf( $plxPlugin->getLang('L_NEED_AUTH'), $plxPlugin->plxMotor->urlRewrite('?adhesion.html'), $plxPlugin->getParam('mnuAdhesion') ).'</p>';
 $plxPlugin->form_login_adherent = false;
 $plxPlugin->finclude(PLX_PLUGINS.get_class($plxPlugin).'/form.login.inc.php',true,'aside');
else :
$r = $plxPlugin->getAdherents('/^[0-9]{5}.(.[a-z-]+){2}.[0-9]{10}.xml$/');
$t = $plxPlugin->getParam('typeAnnuaire') != 'generaliste';
?>
<div class="scrollable-table">
	<table class="table full-width" id="annuaire" summary="membres">
	<thead>
		<tr class="new">

<?php eval($this->plxMotor->plxPlugins->callHook('annuaireTH'.get_class($plxPlugin)));# Hook Plugins ?>

			<th><?php $plxPlugin->lang('L_ADMIN_LIST_NAME');echo ($locked?'</th><th>':'<br/>');#@modifié &nbsp;;;;) ?>
<?php 				$plxPlugin->lang('L_ADMIN_LIST_FIRST_NAME') ?>
<?php echo $t?'<br/>'.$plxPlugin->getLang('L_ADMIN_LIST_ACTIVITY'):'' ?></th>
<?php 				if($t){ ?><th><?php $plxPlugin->lang('L_ADMIN_LIST_STRUCTURE') ?><br/>
<?php 				$plxPlugin->lang('L_ADMIN_LIST_DPT') ?></th><?php } ?>
			<th><?php echo ($locked?'':$plxPlugin->getLang('L_ADMIN_LIST_ADRESSE').'<br/>'.$plxPlugin->getLang('L_ADMIN_LIST_ZIP_CODE').'&nbsp;') ?>
<?php 				$plxPlugin->lang('L_ADMIN_LIST_CITY') ?></th>
<?php echo ($locked?'':'			<th style="width:12%;">'.$plxPlugin->getLang('L_ADMIN_LIST_TEL').'<br/>'.($t?$plxPlugin->getLang('L_ADMIN_LIST_TEL_OFFICE'):'&nbsp;').'</th>') ?>
			<th><?php $plxPlugin->lang('L_ADMIN_LIST_MAIL') ?></th>
		</tr>
	</thead>
	<tbody>
<?php
		$num = 0;
		if ($r) {
			foreach($plxPlugin->plxRecord_adherents->result as $k=>$v) {
				$accord = ($locked?($v['coordonnees'] == 'public'):($v['coordonnees'] == 'rec' OR $v['coordonnees'] == 'public'));
				if ($v['validation'] == 1 && $accord) {
					$ordre = ++$num;
					echo '<tr class="line-'.($num%2).'">';
					# Hook Plugins
					eval($this->plxMotor->plxPlugins->callHook('annuaireTR'.get_class($plxPlugin)));
					echo '<td>'.$v['nom'].($locked?'</td><td>':'<br/>').$v['prenom'].'<br/>'.($t?@$v['activite']:'');
					if($t){
						echo '</td><td>';
						echo @$v['etablissement'].'<br/>'.@$v['service'];
					}
					echo '</td><td>';
					echo ($locked?'':$v['adresse1'].'<br/>'.$v['adresse2'].'<br/>'.$v['cp'].'&nbsp;').$v['ville'];
					echo ($locked?'':'</td><td class="dial">'.str_replace(' ','&nbsp;',$v['tel'].''.($t?@'<br/>'.$v['tel_office']:'')));
					echo '</td><td>';
					echo $plxPlugin->badEmail(plxUtils::strCheck($v['mail']));
					echo '</td></tr>';
				}
			}
		}
		else {
			echo '<tr><td colspan="8" style="text-align:center;"><strong>'.$plxPlugin->getLang('L_ADMIN_NO_VALIDATION').'</strong></td></tr>';
			$a[1] = 0;
		}
?>
	</tbody>
	</table>

<?php eval($this->plxMotor->plxPlugins->callHook('annuaireEnd'.get_class($plxPlugin)));# Hook Plugins ?>

</div>
<script style="display:none" type="text/javascript">
//stackoverflow.com/a/36108449 : onload chained
if(window.onload != null){var ola = window.onload;}
window.onload = function(){
 /* table_load();*/
 window.setTimeout(function() {
  gotable('annuaire');
 }, 333);
 if(ola!=null){ola();}//chained onload
};
/*console.log(navigator);*/
isDial = true;
try{
 navigator.registerProtocolHandler("tel", "tel:%s", "tel protocol");
}
catch(e){
 isDial = false;
 console.log('isDial report: '+e);
}
/* isDial = true;//debug */
function table_load(){
 var liame = window.document.getElementsByClassName('baddirection');
 for(e=0;e<liame.length;e++){
//console.log('search href :',liame[e].textContent.search('@'));
  if(liame[e].textContent.search('@')<0){/*si pas fait*/
   var s = liame[e].textContent;/* || liame[e].innerText;*/
   s = s.replace(/_\[TA\]_/,'@');
   s = s.replace(/_\[TOD\]_/gi,'.');
   s = rtl(s);
   liame[e].textContent = s;
   /* liame[e].innerText = s;*/
   /* liame[e].innerHTML = s;*/
   /* liame[e].setAttribute('data-content', s);*/
   /* liame[e].data = s;*/
   liame[e].className += ' nobot';
  }
 }
/*phone dialer*/
 var dial = window.document.getElementsByClassName('dial');
 for(e=0;e<dial.length;e++){
  console.log('search href :',dial[e].innerHTML.search('href'));
  if(dial[e].innerHTML.search('href')<0){/*si pas fait*/
   var s = dial[e].innerHTML;
   dial[e].setAttribute('data-content',dial[e].textContent);/*innerText*/
   s = s.split('<br>');
   for(f=0;f<s.length;f++)/*console.log(s[f])*/
    s[f] = (!!s[f]&&isDial?'<a href="tel:'+s[f].replace(/&nbsp;/gi,'')+'">'+s[f]+'</a>':s[f]);

   s = s.join('<br/>');
   dial[e].innerHTML = s;
  }
 }
/*dataTable.refresh();*/
}
</script>
<script style="display:none" type="text/javascript">
/* var myTable = document.querySelector("#annuaire");//4 modern browser */ 
/* var dataTable = new DataTable(myTable);*/
function gotable(id){
	dataTable = new DataTable('#'+id, {
        perPageSelect: [5, 10, 15, 20, 25, 50, 100],
        perPage: <?php echo ($plxPlugin->getParam('perPage') == '' ? 50 : $plxPlugin->getParam('perPage')); ?>,
/*		searchable: false,//true is default*/
/*		fixedHeight: false,//true is default*/
/*		sortable: false,//true is default (load ok but duplicate all td's content on sort seeing vdt-1.2.2 && chromium-31)*/
/* Customise the display text*/
		labels: {
			placeholder: "<?php $plxPlugin->lang('L_LABEL_JSDTABLE_PLACEHOLDE') ?>", /* The search input placeholder*/
			perPage: "<?php $plxPlugin->lang('L_LABEL_JSDTABLE_PERPGS') ?>", /* per-page dropdown label*/
			noRows: "<?php $plxPlugin->lang('L_LABEL_JSDTABLE_NODATA') ?>", /* Message shown when there are no search results*/
			info: "<?php $plxPlugin->lang('L_LABEL_JSDTABLE_INFO') ?>", 
		},
	});

	events = ['init','sort','page','perpage','search'];/*,'refresh'*//*,'update'//github.com/Mobius1/Vanilla-DataTables/wiki/Events*/
	for(vnt=0;vnt < events.length; vnt++){
		dataTable.on('datatable.'+events[vnt], function(args) {
   console.log('datatable.events: ' ,arguments.length,arguments);
			/*table_load();// Do something when datatable.XXXX fires*/
		});
	}
}
/* var windowObjectReference;*/
/* var strWindowFeatures = "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes";*/
/* function joindre(str){
 return windowObjectReference = window.open("mailto:"+rtl(str), "Contact membre", strWindowFeatures);
 }*/
function joindre(str){return location.href = "mailto:"+str;}
function rtl(str) {/*reverseString*/
	return (str === '') ? '' : rtl(str.substr(1)) + str.charAt(0);
}
table_load();
/*gotable('annuaire');*/
</script>
<?php
endif;