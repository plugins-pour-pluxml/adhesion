<?php

$LANG                              = array(

'L_ADMIN_MENU_NAME'                => 'Adhérents',#Nom du menu admin dans PluXml : admin & thème default
'L_ADMIN_TITLE_MENU'               => 'Administrer les données des adhérents',

'L_PAGE_TITLE'                     => 'Devenir Adhérent',//Devenir membre, Adhérer
'L_ID'                             => 'Identifiant',
'L_ADHESIONS'                      => 'Adhésions',
'L_DATE_FORMAT'                    => 'd/m/Y',
'L_LANG_ISO'                       => 'fr_FR',#pickadate trad
'L_FORMAT_SUBMIT'                  => 'dd/mm/yyyy',#pickadate yyyy/mm/dd
'L_FORMAT_TO_TXT'                  => 'dd/MM/yyyy',#pickadate yyyy/mm/dd
'L_ERROR_FILE_COPY'                => 'La copie du fichier <i>%</i> vers <i>%</i> a échoué...',
#print.php
'L_NO'                             => 'Non',
'L_YES'                            => 'Oui',
'L_FILE_NAME'                      => 'Liste_des_adherents_association_',
'L_WRONG FILE'                     => 'Mauvais Fichier tpl.',//Wrong file.
'L_FILE UNEXIST'                   => 'Fichier inexistant.',//File does not exist.
'L_PRINT_TITLE'                    => 'Liste des adhérents de l’association',
'L_DEACTIVATED'                    => 'Désactivées',//Annuaire
'L_INSCRIT'                        => 'Inscription validée',
# config.php
'L_MAIL_AVAILABLE'                 => 'Fonction d’envoi de courriel disponible',
'L_MAIL_NOT_AVAILABLE'             => 'Fonction d’envoi de courriel non disponible',
'L_ART_PROTECTED'                  => 'Article dédié aux adhérents',//protégé par mot de passe
'L_CAT_PROTECTED'                  => 'Catégorie dédiée aux adhérents',//protégée par mot de passe
'L_PGS_PROTECTED'                  => 'Page dédiée aux adhérents',//protégée par mot de passe
'L_CONFIG_ROOT_PATH'               => 'Emplacement du fichier des adherents (fichier xml)',
'L_CONFIG_ROOT_PATH_HELP'          => 'Ne pas oublier le slash (/) à la fin.',
'L_MENU_DISPLAY'                   => 'Afficher le menu du plugin adhésion',
'L_MENU_STRCUTLOCKS'               => 'Tronquer les articles à partir de combiens de mots',
'L_MENU_STRCUTLOCKS_HELP'          => 'Permet d’afficher les N premiers mots des articles réservés aux adhérents. Laisser vide ou 0 pour désactiver.',
'L_MENU_TITLE'                     => 'Titre du menu principal',
'L_MENU_ADHERER_TITLE'             => 'Titre du menu des avantages de l’adhésion',
'L_MENU_ADHESION_TITLE'            => 'Titre du menu du formulaire d’adhésion',
'L_MENU_MEMBERS_TITLE'             => 'Titre du menu et des pages Espace adhérent',
'L_MENU_CONNEXION_TITLE'           => 'Titre du menu et des pages de connexion',
'L_MENU_DECONNEXION_TITLE'         => 'Titre du menu des liens de déconnexion',
'L_MENU_DESC'                      => 'Description de l’adhésion',
'L_MENU_FORGET_MY_PASS'            => 'Titre de la page de récupération du mot de passe',
'L_MENU_MY_ACCOUNT'                => 'Titre de la page du compte adhérent',
'L_MENU_ACCES_ANNUAIRE'            => 'Afficher l’annuaire public',
'L_SHOW_MENU_ANNUAIRE'             => 'Afficher l’annuaire',
'L_MENU_ANNUAIRE'                  => 'Titre de la page annuaire',
'L_ANNUAIRE_GEN'                   => 'Généraliste',
'L_ANNUAIRE_PRO'                   => 'Professionel',
'L_TYPE_ANNUAIRE'                  => 'Type d’annuaire',
'L_MENU_POS'                       => 'Position du menu',
'L_EMAIL'                          => 'Courriel',
'L_EMAIL_SUBJECT'                  => 'Objet du courriel',
'L_PASSWD_SUBJECT'                 => 'Objet du courriel de récupération du mot de passe',
'L_PASSWD_MESSAGE'                 => 'Message de récupération du mot de passe',
'L_THANKYOU_MESSAGE'               => 'Message de remerciement',
'L_TEMPLATE'                       => 'Thème',
'L_SAVE'                           => 'Enregistrer',
'L_ADRESSE_ASSO'                   => 'Adresse de l’association',
'L_CLE'                            => 'Longueur de la clé du mot de passe',
'L_ANNEE'                          => 'Type d’année pour la durée de l’adhésion',
'L_ANNEE_CIVILE'                   => 'Année civile',
'L_ANNEE_ENTIERE'                  => 'Année entière',
'L_ANNEE_ILLIMITE'                 => 'Années illimitées',
'L_ANNEE_SS_COTIS'                 => 'Nombre d’années autorisés sans cotiser',
'L_ANNEE_SS_COTIS_HELP'            => 'Nombre d’années autorisés avant que l’adhérent(e) soit dévalidé(e) par le système. Inutilisé si le type d’Années est illimitées.',
'L_RAPPEL_SUBJECT'                 => 'Objet du courriel de rappel',
'L_RAPPEL_MESSAGE'                 => 'Message du courriel de rappel',
'L_COTIS_SUBJECT'                  => 'Objet du courriel de cotisation',
'L_COTIS_MESSAGE'                  => 'Message du courriel de cotisation',
'L_VALIDATION_SUBJECT'             => 'Objet du courriel de validation',
'L_VALIDATION_MESSAGE'             => 'Message du courriel de validation',
'L_DEVALIDATION_SUBJECT'           => 'Objet du courriel de dévalidation',
'L_DEVALIDATION_MESSAGE'           => 'Message du courriel de dévalidation',
'L_NOM_ASSO'                       => 'Nom de l’association',
'L_DOMAINE_ASSO'                   => 'Nom de domaine du site (style&nbsp;asso.com)',
'L_DISPLAY'                        => 'Afficher',
'L_HIDE'                           => 'Masquer',
'L_ANNUAIRE'                       => 'Annuaire',

'L_DEFAULT_MENU_NAME'              => 'Adhérer',
'L_DEFAULT_MENU_ADHERER'           => 'Pourquoi Adhérer?',
'L_DEFAULT_MENU_ADHESION'          => 'Adhérer à l’association',
'L_DEFAULT_MENU_MEMBERS'           => 'Espace adhérents',
'L_DEFAULT_MENU_CONNEXION'         => 'Connexion',
'L_DEFAULT_MENU_DECONNEXION'       => 'Déconnexion',
'L_DEFAULT_DESC'                   => 'Placer ici ce que l’association peut apporter aux adhérents',
'L_DEFAULT_MENU_PASS'              => 'Récupération du mot de passe',
'L_DEFAULT_MENU_MY_ACCOUNT'        => 'Mon compte',
'L_DEFAULT_MENU_ANNUAIRE'          => 'Annuaire des adhèrents',
'L_DEFAULT_SUBJECT'                => '## NOM ASSOCIATON ## Nouvelle demande d’adhésion',
'L_DEFAULT_SUBJECT_PASS'           => '## NOM ASSOCIATON ## Récupération de votre mot de passe',
'L_DEFAULT_MSG_PASS'               => 'Vous avez demandé à récupérer votre mot de passe temporaire. Il se trouve ci-dessous&nbsp;',
'L_DEFAULT_THANKYOU'               => 'Merci pour votre demande d’adhésion. Elle ne sera validée que lors de la réception de votre paiement (xxxx euros) par le trésorier de l’association.',
'L_DEFAULT_VAL_MSG'                => 'Merci pour votre adhésion. Nous venons de recevoir votre paiement et nous vous informons que vous êtes officiellement inscrit.',
'L_DEFAULT_VALIDATION_SUBJECT'     => '## NOM ASSOCIATON ## Demande d’adhésion validée',
'L_DEFAULT_COTIS_MSG'              => 'Merci pour votre ré-adhésion. Nous venons de recevoir votre paiement et nous vous informons que vous êtes officiellement ré-inscrit.',
'L_DEFAULT_COTIS_SUBJECT'          => '## NOM ASSOCIATON ## Cotisation annuelle reçu',
'L_DEFAULT_RAPPEL_MSG'             => 'Nous vous rappelons que nous n’avons pas encore reçu votre cotisation annuelle. Si vous souhaitez continuer votre adhésion, merci d’adresser votre règlement au trésorier de l’association.',
'L_DEFAULT_RAPPEL_SUBJECT'         => '## NOM ASSOCIATON ## Rappel de cotisation',
'L_DEFAULT_DEVAL_MSG'              => 'Nous vous informons que nous n’avons pas encore reçu votre cotisation annuelle. Si vous souhaitez continuer votre adhésion, merci d’adresser votre règlement au trésorier de l’association.',
'L_DEFAULT_DEVALIDATION_SUBJECT'   => '## NOM ASSOCIATON ## Demande de désinscription validée',
'L_WARNPRM_SUBJECT'                => 'Pensez à modifier l’objet du message d’adhésion',
'L_WARNPRM_DEVALIDATION_SUBJECT'   => 'Pensez à modifier l’objet du message de dévalidation',
'L_WARNPRM_VALIDATION_SUBJECT'     => 'Pensez à modifier l’objet du message de validation de l’adhésion',
'L_WARNPRM_COTIS_SUBJECT'          => 'Pensez à modifier l’objet du message de cotisation reçu',
'L_WARNPRM_RAPPEL_SUBJECT'         => 'Pensez à modifier l’objet du message de rappel de cotisation',
'L_WARNPRM_SUBJECT_PASSWORD'       => 'Pensez à modifier l’objet du message de récupération du mot de passe',
'L_WARNPRM_THANKYOU'               => 'Pensez à modifier le message de remerciement',
'L_WARNPRM_NOM_ASSO'               => 'Veuillez renseigner un nom d’association',
'L_WARNPRM_ADRESSE_ASSO'           => 'Veuillez renseigner une adresse pour l’association',
'L_WARNPRM_DESC_ADHESION'          => 'Veuillez expliquer ce que l’association apporte aux adherents',
'L_WARNPRM_NO_DIR_ADHES'           => 'Le dossier des adhésions n’a pas été créé. Vérifiez les droits en écriture et créez le manuellement pour que le plugin fonctionne.',

# bulletin adhesion
'L_FORM_ALL_REQUIRED'              => 'Tous les champs marqués par %s sont requis',
'L_FORM_IDENTITY'                  => 'Identité',
'L_FORM_NAME'                      => 'Nom',
'L_FORM_FIRST_NAME'                => 'Prénom',
'L_FORM_MAIL'                      => 'Courriel (important)',
'L_NOTI_MAIL'                      => 'Courriel',
'L_FORM_OTHER'                     => 'Autre',
'L_FORM_DETAIL'                    => 'Précisez',
'L_FORM_CHOICE'                    => 'Votre choix',
'L_FORM_SHARING'                   => 'Partage de vos données',
'L_FORM_MAILING'                   => 'Des nouvelles de l’association',
'L_FORM_ACTIVITY'                  => 'Activité',
'L_FORM_AGENDA'                    => 'Coordonnées',
'L_FORM_SOCIETY'                   => 'Etablissement',
'L_FORM_SERVICE'                   => 'Service',
'L_FORM_ADDRESS'                   => 'Adresse',
'L_FORM_ZIP_CODE'                  => 'Code Postal',
'L_FORM_CITY'                      => 'Ville',
'L_FORM_TEL'                       => 'Téléphone',
'L_FORM_TEL_OFFICE'                => 'Téléphone pro ou poste',
'L_FORM_BTN_SEND'                  => 'Envoyer',
'L_FORM_ANTISPAM'                  => 'Vérification anti-spam',
'L_FORM_ERASE'                     => 'Votre demande de désinscription a bien été prise en compte. N’hésitez pas à nous rejoindre si vous changez d’avis',
'L_FORM_ERASE_OK'                  => 'Désinscription enregistrée.',
'L_FORM_ERASE_FORM_LIST_OK'        => 'Désinscription des listes de diffusion enregistrée.',
'L_ALREADY_REC'                    => 'Les modifications ont bien été prises en compte.',
'L_FORM_OK'                        => 'OK',
'L_FORGET_PASS'                    => 'Mot de passe oublié?',
'L_MY_ACCOUNT'                     => 'Paramétrer mon compte',
'L_EMAIL_PASS_OK'                  => 'Un courriel vient de vous être envoyé pour modifier votre mot de passe.',
'L_EMAIL_PASS_KO'                  => 'Une erreur est survenue. Merci de verifier le courriel ou utiliser le formulaire de contact.',
'L_EDIT_OK'                        => 'Les modifications ont été enregistrées',
'L_DEVAL_OK'                       => 'Vous n’êtes plus membre de l’association',
'L_FORM_CONFIRM'                   => 'Êtes-vous sûr de ne plus vouloir être membre de l’association ? Cette action est irréversible.',
'L_NEW_MAIL'                       => 'Vous venez de changer votre courriel.',
'L_NEW_MAIL_SUBJECT'               => 'Nouvelle adresse',
'L_NEW_PASS'                       => 'Vous venez de changer votre mot de passe.',
'L_REGEN_PASS'                     => 'La procédure de renouvellement de votre mot de passe a été initiée. Si vous n\'avez rien demandé, merci de ne pas tenir compte de ce mail. Sinon, merci de suivre le lien : ',
'L_NEW_PASS_SUBJECT'               => 'Nouveau mot de passe',
'L_NEW_MAIL_PASS_REG'              => 'Un administrateur a généré un nouveau mot de passe. Vous devrez le changer lors de la prochaine connexion.',
'L_NEW_MAIL_PASS_SUBJECT_REG'      => 'Nouveau mot de passe',
'L_CHANGE_YOUR_PASS'               => 'Vous devez modifier votre mot de passe',
'L_PASSWORD'                       => 'Mot de passe', 
'L_EDIT_PASSWORD'                  => 'Modifier le mot de passe', 
'L_SECURITY_RULES'                 => '(%d caractères ou plus : minuscules, majuscules, chifres et caractères spéciaux)', 
'L_FORM_SECURITY'                  => 'Sécurité',

# erreurs
'L_WARNPRM_EMAIL'                  => 'Veuillez saisir une adresse courriel valide',
'L_WARNPRM_ADHERENTS'              => 'Le chemin vers le fichier des adhérents doit être complété.',
'L_WARNPRM_NO_GULIST'              => 'Liste de diffusion &quot;adherents&quot; indisponible. Veuillez vérifier sa présence dans le <a href="../../plugins/gutuma/news/lists.php">Plugin Gutuma</a>.',
'L_ERR_REC'                        => 'Les donnés n’ont pas été correctement enregistrées.',
'L_ERR_NAME'                       => 'Le nom',
'L_ERR_FIRST_NAME'                 => 'Le prénom',
'L_ERR_ACTIVITE'                   => 'L’activité',
'L_ERR_AUTRE_ACTIVITE'             => 'L’intitulé de votre activité',
'L_ERR_ETABLISSEMENT'              => 'L’établissement',
'L_ERR_SERVICE'                    => 'Le service',
'L_ERR_ADRESSE'                    => 'L’adresse',
'L_ERR_CP'                         => 'Le code postal',
'L_ERR_VILLE'                      => 'La ville',
'L_ERR_TEL'                        => 'Le téléphone',
'L_ERR_TEL_OFFICE'                 => 'Le téléphone pro',
'L_ERR_CHOIX'                      => 'Votre choix',
'L_ERR_COORDONNEES'                => 'Le traitement de vos données',
'L_ERR_MAILING'                    => 'Recevoir des nouvelles de l’association',//AARCH
'L_ERR_MAIL'                       => 'Le courriel',
'L_ERR_ANTISPAM'                   => 'La vérification anti-spam',
'L_FORM_THANKYOU'                  => 'Merci pour votre demande.',
'L_MSG_WELCOME'                    => 'Merci de remplir le formulaire ci-dessous',
'L_ERR_ANTISPAM'                   => 'La vérification anti-spam a échoué',
'L_ERR_SENDMAIL'                   => 'Une erreur est survenue pendant l’envoi de votre message',
'L_ERR_USER_ALREADY_USED'          => 'Cet adhérent existe déjà',
'L_ERR_USER_ALREADY_VALID'         => 'Votre compte est encore valide',
'L_ADMIN_ERR_USER_ALREADY_VALID'   => 'Ce compte est encore valide',#
'L_ERR_SENDEMAIL'                  => 'Echec de l’envoi du courriel de confirmation',
'L_ERR_PASS_SENT'                  => 'Echec d’envoi du mot de passe par courriel a l’adhérent',
'L_ERR_RAPPEL_SENT'                => 'Echec de l’envoi du rappel à cotisation par courriel a l’adhérent',
'L_PASSWORD_NOT_STRONG'            => 'Le mot de passe n\'est pas assez solide',
'L_ERR_DURING_RECORD'              => 'Une erreur est survenue durant l\'enregistrement',

'L_ERR_NO_GUTUMA'                  => 'Le plugin adhésion nécessite le plugin <a href=\"http://www.ecyseo.net/\" title=\"Voir page téléchargements\" onclick=\"window.open(this.href);return false;\">Gutuma</a> pour fonctionner. Ce dernier doit être activé.',
'L_INTERNAL_ERR'                   => 'Échec de l’enregistrement&nbsp;: erreur interne',

'L_FORM_WALLE'                     => 'Si vous souhaitez que votre demande ne soit jamais prise en compte, remplissez ce champ ^_^',
'L_FORM_RULES'                     => 'Je souhaite devenir adhérent de l’Association',
'L_FORM_RULES_RE'                  => 'Je souhaite renouveler mon adhésion',
'L_FORM_RULES_NO'                  => 'Je souhaite me désinscrire de l’Association',
'L_FORM_RULES_SHARE_PUBLIC'        => 'J’accepte que mes coordonnées figurent sur l’annuaire public de l’Association',
'L_FORM_RULES_SHARE_MEMBER'        => 'J’accepte que mes coordonnées figurent sur l’annuaire de l’espace adhérents de l’Association',
'L_FORM_RULES_SHARE_NO'            => 'Je refuse que mes coordonnées figurent sur l’annuaire de l’Association',//Je refuse que mes coordonnées figurent sur le site de l’Association
'L_FORM_RULES_NEWS'                => 'J’accepte de recevoir par courriel toute information concernant le site de l’Association',
'L_FORM_RULES_NEWS_NO'             => 'Je refuse de recevoir sur ma messagerie des informations concernant le site de l’Association',
'L_NOTI_RULES_NEWS'                => 'Accepte de recevoir par messagerie toute information concernant le site de l’Association',
'L_NOTI_RULES_NEWS_NO'             => 'Refuse de recevoir sur ma messagerie des informations concernant le site de l’Association',
'L_NOTI_SENDING_DATE'              => 'Envoyé à l’association le',
'L_FORM_FIELDS_MISSING'            => 'Un ou plusieurs champs n’ont pas été convenablement complétés&nbsp;:',

'L_ADD_ADD'                        => 'Votre adresse (%s) est ajoutée à la liste de diffusion',//function editMyAccount
'L_REMOVE_ADD'                     => 'Votre Adresse (%s) est retirée de la liste de diffusion',//function editMyAccount

'L_COORD_PUBLIC'                   => 'Diffuse ses coordonnées au public',
'L_COORD_REC'                      => 'Diffuse ses coordonnées aux adhérents',
'L_COORD_NO'                       => 'Refuse de diffuser ses coordonnées',
'L_NEWS_OK'                        => 'Accepte les courriels',
'L_NEWS_NO'                        => 'Refuse les courriels',
'L_ADHESION_OK'                    => 'Souhaite devenir adhérent',
'L_ADHESION_STOP'                  => 'Souhaite arréter son adhésion',
'L_ADHESION_RENEW'                 => 'Souhaite renouveler son adhésion',
# admin
'L_TITLE_CONFIG'                   => 'Gérer les adhérents',
'L_ADMIN_LIST_MEMBER_NEW'          => 'Inscrire un nouvel adhérent (À valider)',//
'L_ADMIN_LIST_MEMBERS_TO_VALIDATE' => 'À valider',//Liste des adhérents
'L_ADMIN_LIST_MEMBERS_VALIDATED'   => 'Validés',//Liste des adhérents
'L_ADMIN_LIST_MEMBERS_VALIDE'      => 'Valides',//Liste des adhérents
'L_COTIS'                          => 'Cotisation',
'L_COTIS_ZERO'                     => 'N/A',//in funk cotisationAJour()
'L_COTIS_OK'                       => 'Valide pour',//in funk cotisationAJour()
'L_COTIS_NO'                       => 'En retard de',//in funk cotisationAJour()
'L_DAYS'                           => 'jour(s)',//in funk cotisationAJour()

'L_ONLY_LETTERS'                   => 'Seule les lettres sont admises',
'L_TIME'                           => 'Heure',
'L_NEW_TIME'                       => 'Nouvelle heure',
'L_DATE'                           => 'Date',
'L_NEW_DATE'                       => 'Nouvelle date',
'L_CHANGE_DATE_BTN'                => 'Changer la date',
'L_CHANGE_DATE'                    => 'de l’adhésion',
'L_CHANGE_FIRST_DATE'              => 'de la première adhésion',

'L_ADMIN_EXPORT_LIST'              => 'Exporter',
'L_ADMIN_EXPORT_FORMAT'            => 'Exporter les données au format',
'L_ADMIN_LIST_ID'                  => 'ID',
'L_ADMIN_FIRST_ASK'                => 'Inscrit <br/>depuis le ',
'L_ADMIN_FIRST_SEL'                => 'Inscrit depuis le ',//IN SELECT OPTION VALUE : NO BR INSIDE
'L_ADMIN_DATE_VAL'                 => 'Validé le',
'L_ADMIN_LIST_NAME'                => 'Nom',
'L_ADMIN_LIST_FIRST_NAME'          => 'Prénom',
'L_ADMIN_LIST_ACTIVITY'            => 'Activité',
'L_ADMIN_LIST_STRUCTURE'           => 'Etablissement',
'L_ADMIN_LIST_DPT'                 => 'Service',
'L_ADMIN_LIST_ADRESSE'             => 'Adresse',
'L_ADMIN_LIST_ZIP_CODE'            => 'Code Postal',
'L_ADMIN_LIST_CITY'                => 'Ville',
'L_ADMIN_LIST_TEL'                 => 'Téléphone',
'L_ADMIN_LIST_TEL_PRO'             => 'Téléphone pro',//fessionel
'L_ADMIN_LIST_TEL_OFFICE'          => 'Poste',
'L_ADMIN_LIST_MAIL'                => 'Courriel',
'L_ADMIN_LIST_CHOICE'              => 'Choix',
'L_ADMIN_LIST_COORDONNEES'         => 'Annuaire',//Partage
'L_ADMIN_LIST_MAILING'             => 'Infolettres',
'L_ADMIN_LIST_VALIDATION'          => 'Valider',
'L_ADMIN_LIST_UPDATE'              => 'Mettre à jour',
'L_ADMIN_LIST_COTISE'              => 'A cotisé(e)',
'L_ADMIN_LIST_DEVALIDATION'        => 'Dévalider',
'L_ADMIN_LIST_REGENEREPASS'        => 'Regénérer le MDP',
'L_ADMIN_LIST_NEW'                 => 'Nouvelle adhésion',
'L_ADMIN_APPLY_BUTTON'             => 'Enregistrer',
'L_ADMIN_SELECTION'                => 'Pour la sélection',
'L_ADMIN_DELETE'                   => 'Effacer',
'L_ADMIN_VALIDATION_PENDING'       => 'Aucune demande d’adhésion en attente de validation',
'L_ADMIN_NO_VALIDATION'            => 'Aucune adhésion validée',
'L_ADMIN_ADD_ADD'                  => 'Liste de diffusion, ajout de l’adresse (%s) de %s.',
'L_ADMIN_NEW_PASS'                 => 'Nouveau mot de passe envoyé, l’adresse (%s) de %s changée.',
'L_ADMIN_NEW_PASS_REG'             => 'Mot de passe renénéré et envoyé à l’adresse (%s) de %s.',
'L_ADMIN_REMOVE_ADD'               => 'Liste de diffusion, rejet de l’adresse (%s) de %s.',
'L_ADMIN_REMOVE_ADH'               => 'Fiche de l’adhérent %s supprimée',
'L_ADMIN_UPDATE_ADH'               => 'Fiche de l’adhérent %s modifiée',
'L_ADMIN_NEW_ADH'                  => 'Fiche de l’adhérent %s ajoutée',
'L_ADMIN_VALIDATION_ADH'           => 'Fiche de l’adhérent %s validée',
'L_ADMIN_DEVALIDATION_ADH'         => 'Fiche de l’adhérent %s désactivée',
'L_ADMIN_SEND_RAPPEL'              => 'Envoyer le courriel de rappel à cotisation',
'L_ADMIN_SEND_RAPPEL2'             => 'Envoyer le deuxième courriel de rappel à cotisation',
'L_FORM_NEW'                       => 'Nouvelle<br/>demande',
'L_FORM_OLD'                       => 'Ancien(ne)<br/>adhérent(e)',
'L_ADMIN_DATE_DEL'                 => 'Dévalidé le',
'L_ADMIN_LIST_CLE'                 => 'Clef d’activation',
'L_ADMIN_PASSWORD'                 => 'Votre mot de passe temporaire est&nbsp;',
'L_ADMIN_ID'                       => 'Votre identifiant est',// votre nom relié à votre prénom, le tout en minuscules, sans espace, sans caractère accentué ou exotique comme les cédilles, les tirets, etc. (exemple&nbsp; dupondjeanfrancois).',####SIMPLIFY : YOUR ID IS : name... be carefull lang is edited (ol sel66)######
'L_ADMIN_CONFIRM'                  => 'Êtes-vous sûr de vouloir supprimer cet adhérent? Cette action est irréversible!',
//'L_ADMIN_CHOICE_CONFIRM'           => 'Êtes-vous sûr de vouloir changer le statut de cet adhérent ?',
'L_ADMIN_NOT_DONE'                 => 'Non renseignée',
'L_TAB_ACTIVITIES'                 => 'Quelles sont les activités possible',
'L_TAB_ACTIVITIES_HELP'            => 'Pour l’annuaire pro. Le dernier index doit être &laquo;Autre&raquo;.',
'L_LABEL_FIND'                     => 'Rechercher adhérent',
'L_LABEL_DELETE_FILTER'            => 'Supprimer le filtre',
'L_PASS_SENT'                      => 'Mot de passe envoyé',
'L_YOUR PASS_IS'                   => 'Votre mot de passe temporaire (que vous devrez changer à la première connexion) temporaire est',
'L_ADMIN_SEND_PASS'                => 'Renvoyer son mot de passe',
'L_PASS_IS_SENDED'                 => 'Mot de passe créé et envoyé',

'L_RAPPEL_1'                       => 'Premier',
'L_RAPPEL_2'                       => 'Deuxième',
'L_RAPPEL_SENT'                    => 'Courriel de rappel à cotisation envoyé',

'L_ADMIN_ACTION'                   => 'Action',
'L_VALIDATION'                     => 'Validation',
'L_MODIFICATION'                   => 'Modification',
'L_SUPPRESSION'                    => 'Suppression',

# LookArticles
#'L_HIDE_LOCKED_ARTICLES'           => 'Masquer de la page d’accueil les articles dédiés aux adhérents',//#unused
#'L_HIDE_LOCKED_CATEGORIES'         => 'Masquer de la page d’accueil les articles des catégories dédiées aux adhérents',#unused
'L_SAVE'                           => 'Sauvegarder',
'L_ARTICLE_PASSWORD_FIELD_LABEL'   => 'Cet article est dédié aux adhérents',
'L_PLUGIN_BAD_PASSWORD'            => 'Mauvais identifiant et/ou mot de passe.',
'L_PLUGIN_MAXTRY'                  => 'Nombre de tentatives dépassées. Vous pourrez tenter de vous reconnecter à partir de',
#'L_PLUGIN_EXPIRED_PASSWORD'        => 'Mot de passe expiré',
'L_CATEGORIE_PASSWORD_FIELD_LABEL' => 'Cette catégorie est dédiée aux adhérents',
'L_FORM_PASSWORD'                  => 'Mot de passe',
'L_FORM_ADMIN_PASSWORD'            => 'Cette page statique est dédiée aux adhérents',
'L_FORM_OK'                        => 'Entrer',
'L_CONNEXION_OK'                   => 'Vous êtes connecté(e)',
'L_DECONNEXION_OK'                 => 'Vous êtes déconnecté(e)',
'L_NEED_AUTH'                      => 'Réservé aux adhèrent(e)s. <a href="javascript:" onclick="doFocus(\'id\');">Connectez vous</a> ou <a href="%s">%s</a> pour y accéder</a>',//Vous devez d'abord vous connecter ou vous enregistrer pour annoncer un événement.
'L_NEED_AUTH_FEED'                 => 'Réservé aux adhérent(e)s',//Vous devez d'abord vous connecter ou vous enregistrer pour annoncer un événement.
#ADRESS /!\ never % for font-size or other style : sprintf()

'L_BJR_MSG'                         => 'Bonjour',# prenom nom.
'L_NOCOTIS_HTML'                    => "\r\n".'		<p>Merci de patienter le temps que votre inscription soit validée.</p>'."\r\n".'<p>Vous recevrez alors un courriel avec votre mot de passe temporaire (que vous devrez changer à la première connexion).</p>'."\r\n"/*.'<p>Vous pouvez nous envoyer un courrier à l’adresse postale suivante&nbsp;: </p>'."\r\n".'<p>Association <strong>%s</strong></p>'."\r\n".'		<div style="padding-left:50px;">%s</div>'."\r\n".'		<p>Ou avec le formulaire de contact du site.</p>'*/,
'L_ADRESS_HTML'                    => "\r\n".'		<p>Merci d’établir votre règlement à l’ordre de&nbsp;: Association <strong>%s</strong><br />et de le retourner à l’adresse postale suivante&nbsp;:</p>'."\r\n".'		<div style="padding-left:50px;">%s</div>'."\r\n".'		<p>Dès que votre inscription est enregistrée par le secrétariat de l’Association, vous recevrez alors la confirmation d’inscription par courriel avec votre mot de passe.</p>',
'L_SPAM_HTML'                      => "\r\n".'		<p style="font-size:13px"><em>Si vous ne <strong>recevez aucun courriel</strong>, pensez à vérifier régulièrement votre dossier <strong> Pourriels / Indésirrables / spams </strong> de votre messagerie.</em></p>',

#CNIL
'L_CNIL_TEXT'                      => 'Pour vous désinscrire de la liste de diffusion, cliquez sur ou recopiez le lien ci-après dans le champ d’adresse de votre navigateur internet',
'L_CNIL_LINK'                      => 'Se désinscrire de la liste de diffusion',
'L_CNIL_NO_REPLY_TEXT'             => 'Merci de ne pas répondre à ce courriel. Celui-ci ayant été généré automatiquement, personne ne va traiter votre réponse.'."\r\n"."\r\n".'Conformément à la Loi Informatique et Libertés du 06/01/1978 et à nos mentions légales vous disposez d’un droit d’accès et de rectification sur les données vous concernant.',

#annuaire + admin (vanilla JS DataTable)
'L_LABEL_TABLE_PERPGS'             => 'Nombre d’adhérent(e)s à afficher par page',
'L_LABEL_JSDTABLE_PLACEHOLDE'      => 'Chercher...',//Search...
'L_LABEL_JSDTABLE_PERPGS'          => '{select} adhérent(e)s par page',//{select} entries per page
'L_LABEL_JSDTABLE_NODATA'          => 'Aucun(e) adhérent(e) trouvé(e)',//No entries found
'L_LABEL_JSDTABLE_INFO'            => 'Affichage de {start} à {end} sur {rows} adhérent(e)s',//Showing {start} to {end} of {rows} entries
'L_LIBERAPAY'                      => 'Dons Réguliers simplifiés avec LiberaPay',
);
