<?php  if (!defined('PLX_ROOT')) exit; ?>
 <p class="in-action-bar">Aide du plugin Adhésion</p>
<?php if($_SESSION['profil']==PROFIL_ADMIN) { ?>
<h2>Espaces dédiés</h2>
<h5>Pour afficher le formulaire d'identification (espace adhérent(e)s)</h5>
<h5>Veuillez dupliquer le code ci-dessous dans votre fichier de thème <b title="de préférence">sidebar.php</b>.</h5>
<code class="alert green">&lt;?php eval($plxShow->callHook('loginLogout')); ?&gt;</code>
<h5>Ou tout autre endroit ou vous souhaitez le faire apparaitre.</h5>
<h2>Aux Utilisateurs du plugin gutuma.</h2>
<h5>Pour rendre disponible la fonction d'infolettre a adhésion (liste de diffusion),<br />
vous devez créer une liste nommé: <b class="green">adherents</b> dans gutuma ;)</h5>
<?php  if(defined('PLX_MYMULTILINGUE')) {# Si plugin plxMyMultilingue présent ?>
<div class="alert red">
<h2>/!\ Attention aux Utilisateur de plxMyMultilingue (0.8.1) /!\</h2>
<h3>Dans l'ordre des plugin: placer Adhésion <b>avant</b> Multilingue. Voir en premier.<br /></h3>
<h4>Cela empêche le bon fonctionnement des espaces destinés aux adhérents coté admin.<br />
Perte de $id dans le hook AdminStatic() lorsque adhesion est chargé après.</h4>
<h4 class="alert green">Comme cela il est compatible Multilingue, et vos adhérents contents.</h4>
</div>
<?php  }#FI plxMyMultilingue présent ?>
<?php }#FI PROFIL_ADMIN ?>
